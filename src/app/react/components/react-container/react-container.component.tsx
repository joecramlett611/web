// import * as React from 'react';
// import { FunctionComponent, useEffect, useState } from 'react';
// import './react-container.component.scss';
// // import {ConnectButton, getDefaultWallets, useWallet, WalletProvider} from '@suiet/wallet-kit';
// // import '@suiet/wallet-kit/style.css';
// // import { WalletContextState } from '@suiet/wallet-kit/dist/hooks/useWallet';
//
// export interface IMyComponentProps {
// 	onConnectRequest?: () => void;
// 	onConnectionChange?: (connection: SuietWalletConnection) => void;
// }
//
// export type SuietWalletConnection =
// 	Pick<any, 'wallet' | 'signAndExecuteTransaction'> & {address: string, isConnected: boolean}; // WalletContextState
//
// export interface SuiWalletComponentProps {
// 	onConnect: (connection: SuietWalletConnection) => void;
// }
//
// export const App: FunctionComponent<SuiWalletComponentProps> = (props: SuiWalletComponentProps) => {
// 	const {onConnect} = props;
//
// 	const {
// 		wallet,
// 		connected,
// 		account,
// 		signAndExecuteTransaction,
// 		disconnect,
// 		status,
// 	} = useWallet();
//
// 	const [isConnectable, setIsConnectable] = useState<boolean>(true);
//
// 	useEffect(() => {
// 		if (!isConnectable || status !== 'connected' || !wallet || !account?.address) {
// 			return;
// 		}
//
// 		if (!connected) {
// 			setIsConnectable(true);
// 			onConnect({
// 				address: undefined,
// 				isConnected: false,
// 				wallet: undefined,
// 				signAndExecuteTransaction,
// 			});
//
// 			return;
// 		}
// 		(async () => {
// 			setIsConnectable(false);
//
// 			onConnect({
// 				address: account.address,
// 				isConnected: true,
// 				wallet,
// 				signAndExecuteTransaction,
// 			});
// 		})();
// 	}, [connected]);
//
// 	return (
// 		!connected ? <ConnectButton label='' className='btn-wallet-sui'/> : <span></span>
// 	);
// };
//
// export const ReactContainerComponent: FunctionComponent<IMyComponentProps> = (props: IMyComponentProps) => {
// 	const {onConnectRequest, onConnectionChange} = props;
//
// 	const handleConnectionChange = (connection: SuietWalletConnection) => {
// 		if (onConnectionChange) {
// 			onConnectionChange(connection);
// 		}
// 	};
//
// 	return (
// 		<WalletProvider supportedWallets={getDefaultWallets()}>
// 			<App onConnect={connection => handleConnectionChange(connection)}/>
// 		</WalletProvider>
// 	);
// };
