import {
	AfterViewInit,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	Output,
	SimpleChanges,
	ViewChild,
	ViewEncapsulation
} from '@angular/core';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
window.React = React;
// import { ReactContainerComponent, SuietWalletConnection } from './react-container.component';

const containerElementName = 'myReactComponentContainer';

@Component({
	selector: 'app-react-container-wrapper-component',
	template: `<span #${containerElementName}></span>`,
	styleUrls: ['./react-container.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ReactContainerWrapperComponent implements OnChanges, OnDestroy, AfterViewInit {
	@ViewChild(containerElementName, {static: false}) containerRef: ElementRef;

	@Input() public counter = 10;
	@Output() public handleConnect = new EventEmitter<any>();

	constructor() {
		this.handleConnection = this.handleConnection.bind(this);
	}

	public handleConnection(connection: any): void {
		if (this.handleConnect) {
			this.handleConnect.emit(connection);
			this.render();
		}
	}

	public ngOnChanges(changes: SimpleChanges): void {
		this.render();
	}

	public ngAfterViewInit(): void {
		this.render();
	}

	public ngOnDestroy(): void {
		ReactDOM.unmountComponentAtNode(this.containerRef.nativeElement);
	}

	private render(): void {
		// ReactDOM.render(<ReactContainerComponent onConnectionChange={this.handleConnection}/>, this.containerRef.nativeElement);
	}
}
