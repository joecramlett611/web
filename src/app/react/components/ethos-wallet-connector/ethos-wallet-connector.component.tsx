import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import './ethos-wallet-connector.component.scss';
// import { EthosConnectProvider, ethos } from 'ethos-connect';
import { SuiEthosWalletConnection } from '../../../modules/sui/interfaces/sui-ethos-wallet-connection.interface';

export interface IMyComponentProps {
	onConnectRequest?: () => void;
	onConnectionChange?: (connection: SuiEthosWalletConnection) => void;
}

export interface SuiWalletComponentProps {
	onConnect: (connection: SuiEthosWalletConnection) => void;
}

export const App: FunctionComponent<SuiWalletComponentProps> = (props: SuiWalletComponentProps) => {
// 	const {onConnect} = props;
// 	const { status, wallet } = ethos.useWallet();
// 	const [isConnecting, setIsConnecting] = useState<boolean>(false);
// 	const handleEthosConnect = (connection: SuiEthosWalletConnection) => {
// 		onConnect(connection);
// 	};
// 	const connect = (): void => {
// 		setIsConnecting(true);
// 		ethos.showSignInModal();
// 	};
//
// 	useEffect(() => {
// 		if (!isConnecting || status !== 'connected' || !wallet) {
// 			return;
// 		}
//
// 		(async () => {
// 			setIsConnecting(false);
// 			handleEthosConnect({
// 				address: await wallet.getAddress(),
// 				wallet,
// 				isConnected: status === 'connected',
// 			});
// 			ethos.hideSignInModal();
// 		})();
// 	}, [wallet]);
//
// 	return (
// 		<span onClick={connect} className={'sui-ether-wallet'}></span>
// 	);
// };
//
// export const EthosWalletConnectorComponent: FunctionComponent<IMyComponentProps> = (props: IMyComponentProps) => {
// 	const {onConnectRequest, onConnectionChange} = props;
//
// 	const handleConnectionChange = (connection: SuiEthosWalletConnection) => {
// 		if (onConnectionChange) {
// 			onConnectionChange(connection);
// 		}
// 	};
//
// 	return (
// 		// @ts-ignore
// 		<EthosConnectProvider hideEmailSignIn={true}>
// 			<App onConnect={connection => handleConnectionChange(connection)}/>
// 		</EthosConnectProvider>
// 	);
};
