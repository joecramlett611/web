import {
	AfterViewInit,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	Output,
	SimpleChanges,
	ViewChild,
	ViewEncapsulation
} from '@angular/core';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { EthosWalletConnectorComponent } from './ethos-wallet-connector.component';
import { SuiEthosWalletConnection } from '../../../modules/sui/interfaces/sui-ethos-wallet-connection.interface';
window.React = React;

const containerElementName = 'ethosWalletConnectorContainer';

@Component({
	selector: 'app-ethos-wallet-connector-wrapper-component',
	template: `<span #${containerElementName}></span>`,
	styleUrls: ['./ethos-wallet-connector.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class EthosWalletConnectorWrapperComponent implements OnChanges, OnDestroy, AfterViewInit {
	@ViewChild(containerElementName, {static: false}) containerRef: ElementRef;

	@Input() public counter = 10;
	@Output() public handleConnect = new EventEmitter<SuiEthosWalletConnection>();

	constructor() {
		this.handleConnection = this.handleConnection.bind(this);
	}

	public handleConnection(connection: SuiEthosWalletConnection): void {
		if (!connection) {
			return;
		}

		this.handleConnect.emit(connection);
	}

	public ngOnChanges(changes: SimpleChanges): void {
		this.render();
	}

	public ngAfterViewInit(): void {
		this.render();
	}

	public ngOnDestroy(): void {
		ReactDOM.unmountComponentAtNode(this.containerRef.nativeElement);
	}

	private render(): void {
		ReactDOM.render(<EthosWalletConnectorComponent onConnectionChange={this.handleConnection}/>, this.containerRef.nativeElement);
	}
}
