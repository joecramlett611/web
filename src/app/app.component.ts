import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	public ngOnInit(): void {
		const headerLogo = document.querySelector('#navLogo');
		const header = document.querySelector('#headerNav');
		let top: number = header.getBoundingClientRect().top + window.scrollY;

		if (top > 50) {
			header.classList.add('backdrop-blur');
			header.classList.remove('no-blur');
			// headerLogo.src = '/assets/img/color_logo_no_text.svg';
			// headerLogo.style.maxHeight = '44px';
		} else {
			header.classList.remove('backdrop-blur');
			header.classList.add('no-blur');
			// headerLogo.style.maxHeight = 'unset';
			// headerLogo.src = '/assets/img/Color%20logo%20-%20no%20background.svg';
		}
		/* Scroll Function */
		window.addEventListener('scroll', () => {
			top = header.getBoundingClientRect().top + window.scrollY;
			/* Fixed Navigation */
			if (top > 50) {
				header.classList.add('backdrop-blur');
				header.classList.remove('no-blur');
				// headerLogo.src = '/assets/img/color_logo_no_text.svg';
				// headerLogo.style.maxHeight = '44px';
			} else {
				header.classList.remove('backdrop-blur');
				header.classList.add('no-blur');
				// headerLogo.style.maxHeight = 'unset';
				// headerLogo.src = '/assets/img/Color%20logo%20-%20no%20background.svg';
			}
		});

		document.body.addEventListener('mouseover', () => {
			const heroAbstractImage = document.querySelector('.picture-container-full img');

			if (heroAbstractImage) {
				heroAbstractImage.classList.add('active');
			}
		});

		document.body.addEventListener('mouseleave', () => {
			const heroAbstractImage = document.querySelector('.picture-container-full img');

			if (heroAbstractImage) {
				heroAbstractImage.classList.remove('active');
			}
		});
	}
}
