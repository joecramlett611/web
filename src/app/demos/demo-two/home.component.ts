import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Params, Router } from '@angular/router';
import 'swiper/css';
import { ICollectionDetails } from '../../modules/web3/interfaces/collection-details.interface';
import FirebaseStorageService from '../../modules/firebase/services/firebase-storage.service';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment/moment';
import { SupportedAssetEnum } from '../../modules/web3/enum/supported-asset.enum';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit, OnDestroy {
	public collections: Array<Pick<ICollectionDetails, 'collectionName' | 'id' | 'dropFrom' | 'dropTo' | 'totalSupply' | 'totalMinted' | 'imageURL' | 'mintPrice' | 'assetName' | 'creatorProfile' | 'squareImageURI'>>;
	private heroTextInterval: NodeJS.Timer;

	constructor(
		private readonly router: Router,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly titleService: Title,
	) {
	}

	public ngOnInit(): void {
		this.collections = [
			{
				id: 9166366397319,
				collectionName: 'In the Clouds',
				creatorProfile: {
					name: 'sorryvrerror',
					lowercaseName: 'sorryvrerror',
					avatar: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/avatar_0x6fF5723435b7dfC2371B57Fb5cB4c373E5995C78?alt=media&token=a0056c36-713b-40ec-bc2a-f02bb54a70f4',
				},
				totalSupply: 12,
				totalMinted: 12,
				mintPrice: 0,
				assetName: SupportedAssetEnum.USDC,
				imageURL: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/QmQjVdcuLxGRJ1WNCvkiqYfxobXeqbHxKh42JmydjcwrRD_9166366397319?alt=media&token=4dac7a80-6ac9-41c5-b383-ac7da08de8d2',
				squareImageURI: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/QmSpVfW9Y4im7phMkkH3bh8pvxyzUFZ14aqXJrexCKcJ65_9166366397319?alt=media&token=15e5b915-ca4d-418a-b3cb-cb0874d96c27',
				dropFrom: 0,
				dropTo: 0,
			},
			{
				id: 9166578142926,
				collectionName: 'Low Effort',
				creatorProfile: {
					name: 'Abstractooor',
					lowercaseName: 'abstractooor',
					avatar: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/avatar_0x1108228e3C27BC176BbFdC33f59c3AB78BedbD6C?alt=media&token=4492c00c-2618-42c9-a061-8639e024d469',
				},
				totalSupply: 25,
				totalMinted: 0,
				mintPrice: 1000,
				assetName: SupportedAssetEnum.OSEA,
				imageURL: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/avatar_0x1108228e3C27BC176BbFdC33f59c3AB78BedbD6C?alt=media&token=4492c00c-2618-42c9-a061-8639e024d469',
				squareImageURI: 'https://omnisea.infura-ipfs.io/ipfs/QmXPtnaszmPaNsW3f5XcE2H13XWnNKYDQzRL1YiMMcyJ5J',
				dropFrom: 0,
				dropTo: 1666289304,
			},
			{
				id: 9166326054249,
				collectionName: 'Dot.Bots',
				creatorProfile: {
					name: 'Devv',
					lowercaseName: 'devv',
					avatar: 'avatar_0x46523F4E792E30A266d9b29469076a1695f17081',
				},
				totalSupply: 222,
				totalMinted: 10,
				mintPrice: 5,
				assetName: SupportedAssetEnum.USDC,
				imageURL: 'https://firebasestorage.googleapis.com/v0/b/omnisea-7a88d.appspot.com/o/QmXnCZpgwZApiwFmHfoxv8g2PU2zUCYSNQbubmBp6zvt2Z_9166326054249?alt=media&token=4a8872b4-91c3-46f2-829f-f8199e7c9746',
				squareImageURI: 'https://omnisea.infura-ipfs.io/ipfs/QmZEDbizoAWCNyKPWJ1xiYELDGxuB6Mp1LN5APsatYg8sX',
				dropFrom: 0,
				dropTo: 0,
			},
		];
		//
		// for (const collection of this.featuredCollections) {
		// 	this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
		// 		collection.imageURL = url;
		// 	});
		//
		// 	this.firebaseStorageService.getIpfsFileURL(collection.creatorProfile.avatar).then((url) => {
		// 		collection.creatorProfile.avatar = url;
		// 		collection.creatorProfile.avatarLoaded = true;
		// 	});
		// }
		//
		// const swiper = new Swiper('.coverflow-slider', {
		// 	modules: [Navigation, Lazy, EffectCoverflow],
		// 	effect: 'coverflow',
		// 	speed: 400,
		// 	loop: true,
		// 	rewind: true,
		// 	spaceBetween: 20,
		// 	centeredSlides: false,
		// 	slidesPerView: 1,
		// 	slidesPerGroup: 1,
		// 	coverflowEffect: {
		// 		rotate: 50,
		// 		stretch: 0,
		// 		depth: 100,
		// 		modifier: 0.5,
		// 		slideShadows: true
		// 	},
		// 	breakpoints: {
		// 		560: {
		// 			slidesPerView: 2
		// 		},
		// 		768: {
		// 			slidesPerView: 3
		// 		},
		// 		1024: {
		// 			slidesPerView: 5
		// 		}
		// 	},
		// 	preloadImages: false,
		// 	lazy: true,
		// 	navigation: {
		// 		nextEl: '.swiper-button-next-4',
		// 		prevEl: '.swiper-button-prev-4',
		// 	},
		// });

		let animatedTextIndex: number = 1;

		this.heroTextInterval = setInterval(() => {
			document.querySelector(`.text-animated-${animatedTextIndex}`).classList.add('hidden');
			animatedTextIndex = animatedTextIndex === 4 ? 1 : (animatedTextIndex + 1);
			document.querySelector(`.text-animated-${animatedTextIndex}`).classList.remove('hidden');
		}, 6000);

		this.titleService.setTitle('Omnisea - Ultimate NFT Creator');
	}

	public ngOnDestroy(): void {
		if (this.heroTextInterval) {
			clearInterval(this.heroTextInterval);
		}
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);
	}

	public goToGateway(): void {
		this.router.navigate(['/onft/gateway']);
	}

	public goToLearn(): void {
		this.router.navigate(['/help-center']);
	}

	public goToCollection(
		collection: Pick<ICollectionDetails, 'collectionName' | 'id' | 'dropFrom' | 'dropTo' | 'totalSupply' | 'totalMinted' | 'imageURL' | 'mintPrice' | 'assetName' | 'creatorProfile' | 'squareImageURI'>,
	): void {
		const queryParams: Params = {
			id: collection.id,
		};

		this.router.navigate(
			['/drop'],
			{queryParams},
		);
	}

	public goToCreator(userAddress: string): void {
		this.router.navigate(['/user'], {queryParams: {userAddress}});
	}

	public isMintActive(collection: Pick<ICollectionDetails, 'collectionName' | 'id' | 'dropFrom' | 'dropTo' | 'totalSupply' | 'totalMinted' | 'imageURL' | 'mintPrice' | 'assetName' | 'creatorProfile' | 'squareImageURI'>): boolean {
		if (collection.totalSupply > 0 && collection.totalSupply === collection.totalMinted) {
			return false;
		}
		const now: number = moment().unix();
		const from: number = collection.dropFrom;
		const to: number = collection.dropTo;

		if (!from) {
			return to > 0 ? now <= to : true;
		}

		return to > 0 ? (now >= from && now <= to) : now >= from;
	}

	public isEnded(collection: Pick<ICollectionDetails, 'collectionName' | 'id' | 'dropFrom' | 'dropTo' | 'totalSupply' | 'totalMinted' | 'imageURL' | 'mintPrice' | 'assetName' | 'creatorProfile' | 'squareImageURI'>): boolean {
		if (collection.totalSupply > 0 && collection.totalSupply === collection.totalMinted) {
			return true;
		}
		const now: number = moment().unix();
		const to: number = collection.dropTo;

		return to > 0 && now > to;
	}

	public getHumanizedStartCounter(unix: number): string {
		if (!unix) {
			return undefined;
		}
		const now: number = moment().unix();
		const secondsDiff: number = moment(unix).diff(now);

		return moment.duration(secondsDiff, 'seconds').humanize(true);
	}
}
