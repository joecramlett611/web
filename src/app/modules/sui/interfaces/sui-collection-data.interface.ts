import { IAllowlist } from '../../aptos/interfaces/allowlist.interface';

export interface ISuiCollectionData {
	allowlist?: IAllowlist;
	totalSupply: number;
	maxSupply: number;
}
