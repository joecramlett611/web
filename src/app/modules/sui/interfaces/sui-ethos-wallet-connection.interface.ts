// import { Wallet } from 'ethos-connect/dist/types/Wallet';

export type SuiEthosWalletConnection = {
	wallet?: any,
	address: string,
	isConnected: boolean,
};
