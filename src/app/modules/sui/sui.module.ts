import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import SuiSuietWalletService from './services/sui-suiet-wallet.service';
// import SuiEthosWalletService from './services/sui-ethos-wallet.service';
import SuiNetworkService from './services/sui-network.service';

@NgModule({
	providers: [
		// SuiSuietWalletService,
		// SuiEthosWalletService, // TODO: Why it has to be on the root to work (behaviour subject forgot)
		SuiNetworkService,
	],
	imports: [
		CommonModule,
	],
})
export class SuiModule {
}
