import { Injectable } from '@angular/core';
// import { GetObjectDataResponse, JsonRpcProvider } from '@mysten/sui.js';
import { environment } from '../../../../environments/environment';
import { ISuiCollectionData } from '../interfaces/sui-collection-data.interface';
import { IAllowlist } from '../../aptos/interfaces/allowlist.interface';

@Injectable({
	providedIn: 'root',
})
export default class SuiNetworkService {
	// public async getCollectionMintedCount(creator: string, name: string): Promise<ISuiCollectionData> {
	// 	const provider: JsonRpcProvider = this.getProvider();
	// 	const launchpadMetadata: GetObjectDataResponse = await provider.getObject(
	// 		environment.suiSpecific.objects.launchpadMetadata,
	// 	);
	// 	// @ts-ignore
	// 	const createdByObject = launchpadMetadata.details.data.fields.created_by.fields.contents;
	// 	const createdByCreatorObject = createdByObject.find((obj) => obj.fields.key === creator).fields.value.fields.contents;
	// 	const collectionObject = createdByCreatorObject.find((obj) => obj.fields.key === name).fields.value.fields;
	// 	const allowlistObject = collectionObject.allowlist.fields;
	// 	const allowlist: IAllowlist = allowlistObject.is_enabled ? {
	// 		isEnabled: true,
	// 		addresses: allowlistObject.addresses,
	// 		publicMaxPerAddress: allowlistObject.public_max_per_address,
	// 		maxPerAddress: allowlistObject.max_per_address,
	// 		publicFrom: allowlistObject.public_from,
	// 	} : {
	// 		isEnabled: false,
	// 		addresses: allowlistObject.addresses,
	// 		publicMaxPerAddress: allowlistObject.public_max_per_address,
	// 		maxPerAddress: allowlistObject.max_per_address,
	// 		publicFrom: allowlistObject.public_from,
	// 	};
	//
	// 	return {
	// 		allowlist,
	// 		maxSupply: collectionObject.maximum_supply,
	// 		totalSupply: collectionObject.total_supply,
	// 	};
	// }
	//
	// public async getSuiCoinAndBalance(address: string): Promise<{
	// 	balance: number,
	// 	coinObjectId: string,
	// 	biggestCoinBalance: number,
	// }> {
	// 	const provider: JsonRpcProvider = this.getProvider();
	// 	const balances: GetObjectDataResponse[] = await provider.getCoinBalancesOwnedByAddress(address, '0x2::sui::SUI');
	// 	let balance: number = 0;
	// 	let coinObjectId: string = '';
	// 	let biggestCoinBalance: number = 0;
	//
	// 	for (const balanceObj of balances) {
	// 		// @ts-ignore
	// 		const coinBalance: number = balanceObj.details?.data?.fields?.balance || 0;
	// 		// @ts-ignore
	// 		const coinId: string = balanceObj.details?.data?.fields?.id?.id;
	//
	// 		if (coinBalance > biggestCoinBalance && coinId) {
	// 			biggestCoinBalance = coinBalance;
	// 			coinObjectId = coinId;
	// 		}
	//
	// 		balance += coinBalance;
	// 	}
	//
	// 	return {
	// 		balance,
	// 		coinObjectId,
	// 		biggestCoinBalance,
	// 	};
	// }
	//
	// private getProvider(): JsonRpcProvider {
	// 	return new JsonRpcProvider(environment.suiSpecific.api.uri);
	// }
}
