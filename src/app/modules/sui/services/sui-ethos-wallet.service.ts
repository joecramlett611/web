import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SuiEthosWalletConnection } from '../interfaces/sui-ethos-wallet-connection.interface';
import { environment } from '../../../../environments/environment';
// import { SuiTransactionResponse } from '@mysten/sui.js/dist/types/transactions';
// import { ObjectId } from '@mysten/sui.js/src/types';
// import { GetObjectDataResponse, JsonRpcProvider } from '@mysten/sui.js';

@Injectable({
	providedIn: 'root',
})
export default class SuiEthosWalletService {
	// @ts-ignore
	// public connection$: BehaviorSubject<SuiEthosWalletConnection> = new BehaviorSubject<SuiEthosWalletConnection>({});
	//
	// public isConnected(): boolean {
	// 	const connection: SuiEthosWalletConnection = this.connection$.getValue();
	//
	// 	return connection?.isConnected && connection.wallet !== undefined;
	// }
	//
	// public async sendTransaction<T = Array<string & number & boolean>>(functionName: string, payload: T): Promise<SuiTransactionResponse> {
	// 	const wallet = this.connection$.getValue()?.wallet;
	//
	// 	if (!wallet) {
	// 		throw new Error('Not connected with Ethos wallet');
	// 	}
	//
	// 	try {
	// 		const transaction = {
	// 			kind: 'moveCall' as const,
	// 			data: {
	// 				packageObjectId: environment.suiSpecific.package,
	// 				module: environment.suiSpecific.module,
	// 				function: functionName,
	// 				typeArguments: [],
	// 				arguments: payload,
	// 				gasBudget: 10000,
	// 			},
	// 		};
	//
	// 		// @ts-ignore
	// 		return await wallet.signAndExecuteTransaction(transaction);
	// 	} catch (error) {
	// 		console.error(error);
	// 	}
	// }
	//
	// public async mergeSuiCoins(primaryCoin: ObjectId): Promise<SuiTransactionResponse> {
	// 	const wallet = this.connection$.getValue()?.wallet;
	//
	// 	if (!wallet) {
	// 		throw new Error('Not connected with Ethos wallet');
	// 	}
	//
	// 	try {
	// 		const coinToMerge: string = await this.getSuiCoinIdToMerge(this.connection$.getValue().address, primaryCoin);
	// 		const transaction = {
	// 			kind: 'mergeCoin' as const,
	// 			data: {
	// 				primaryCoin,
	// 				coinToMerge,
	// 				gasPayment: await this.getSuiCoinIdToPayGas(this.connection$.getValue().address, primaryCoin, coinToMerge),
	// 				gasBudget: 30000,
	// 			},
	// 		};
	//
	// 		// @ts-ignore
	// 		return await wallet.signAndExecuteTransaction(transaction);
	// 	} catch (error) {
	// 		console.error(error);
	// 	}
	// }
	//
	// private async getSuiCoinIdToMerge(ownerAccout: string, primaryCoin: string): Promise<string> {
	// 	const provider: JsonRpcProvider = new JsonRpcProvider(environment.suiSpecific.api.uri);
	// 	const balances: GetObjectDataResponse[] = await provider.getCoinBalancesOwnedByAddress(ownerAccout, '0x2::sui::SUI');
	// 	let coinToMergeObjectId: string = '';
	// 	let biggestCoinBalance: number = 0;
	//
	// 	for (const balanceObj of balances) {
	// 		// @ts-ignore
	// 		const coinBalance: number = balanceObj.details?.data?.fields?.balance || 0;
	// 		// @ts-ignore
	// 		const coinId: string = balanceObj.details?.data?.fields?.id?.id;
	//
	// 		if (coinId === primaryCoin) {
	// 			continue;
	// 		}
	//
	// 		if (coinBalance > biggestCoinBalance && coinId) {
	// 			biggestCoinBalance = coinBalance;
	// 			coinToMergeObjectId = coinId;
	// 		}
	// 	}
	//
	// 	return coinToMergeObjectId;
	// }
	//
	// private async getSuiCoinIdToPayGas(ownerAccout: string, primaryCoin: string, mergedCoin: string): Promise<string> {
	// 	const provider: JsonRpcProvider = new JsonRpcProvider(environment.suiSpecific.api.uri);
	// 	const balances: GetObjectDataResponse[] = await provider.getCoinBalancesOwnedByAddress(ownerAccout, '0x2::sui::SUI');
	// 	let coinToPayGasObjectId: string = '';
	// 	let biggestCoinBalance: number = 0;
	//
	// 	for (const balanceObj of balances) {
	// 		// @ts-ignore
	// 		const coinBalance: number = balanceObj.details?.data?.fields?.balance || 0;
	// 		// @ts-ignore
	// 		const coinId: string = balanceObj.details?.data?.fields?.id?.id;
	//
	// 		if (coinId === primaryCoin || coinId === mergedCoin) {
	// 			continue;
	// 		}
	//
	// 		if (coinBalance > biggestCoinBalance && coinId) {
	// 			biggestCoinBalance = coinBalance;
	// 			coinToPayGasObjectId = coinId;
	// 		}
	// 	}
	//
	// 	return coinToPayGasObjectId;
	// }
}
