import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
// import { SuietWalletConnection } from '../../../react/components/react-container/react-container.component';
// import { SuiJsonValue, SuiTransactionResponse } from '@mysten/sui.js/dist/types/transactions';
// import { environment } from '../../../../environments/environment';
// import { ObjectId } from '@mysten/sui.js/src/types';
// import { GetObjectDataResponse, JsonRpcProvider } from '@mysten/sui.js';

@Injectable()
export default class SuiSuietWalletService {
	public onConnect$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(undefined);
	// public connection$: BehaviorSubject<SuietWalletConnection> = new BehaviorSubject<SuietWalletConnection>(undefined);

	public async connect(): Promise<void> {
		try {
			this.onConnect$.next(true);
		} catch (e) {
			console.error(e);
		}
	}

	// public isConnected(): boolean {
	// 	const connection: SuietWalletConnection = this.connection$.getValue();
	//
	// 	return connection?.isConnected && connection.wallet !== undefined;
	// }
	//
	// public async sendTransaction(functionName: string, payload: SuiJsonValue[]): Promise<SuiTransactionResponse> {
	// 	const connection = this.connection$.getValue();
	//
	// 	if (!connection.isConnected) {
	// 		throw new Error('Not connected with Suiet wallet');
	// 	}
	//
	// 	try {
	// 		const txData = {
	// 			kind: 'moveCall' as const,
	// 			data: {
	// 				packageObjectId: environment.suiSpecific.package,
	// 				module: environment.suiSpecific.module,
	// 				function: functionName,
	// 				typeArguments: [],
	// 				arguments: payload,
	// 				gasBudget: 10000,
	// 			},
	// 		};
	//
	// 		// @ts-ignore
	// 		return await connection.signAndExecuteTransaction({
	// 			transaction: txData,
	// 		});
	// 	} catch (error) {
	// 		console.error(error);
	// 	}
	// }
	//
	// public async mergeSuiCoins(primaryCoin: ObjectId): Promise<SuiTransactionResponse> {
	// 	const connection = this.connection$.getValue();
	//
	// 	if (!connection.isConnected) {
	// 		throw new Error('Not connected with Suiet wallet');
	// 	}
	//
	// 	try {
	// 		const txData = {
	// 			kind: 'mergeCoin' as const,
	// 			data: {
	// 				primaryCoin,
	// 				coinToMerge: await this.getSuiCoinIdToMerge(connection.address, primaryCoin),
	// 				gasPayment: primaryCoin,
	// 				gasBudget: 30000,
	// 			},
	// 		};
	//
	// 		// @ts-ignore
	// 		return await connection.signAndExecuteTransaction(txData);
	// 	} catch (error) {
	// 		console.error(error);
	// 	}
	// }
	//
	// private async getSuiCoinIdToMerge(ownerAccout: string, primaryCoin: string): Promise<string> {
	// 	const provider: JsonRpcProvider = new JsonRpcProvider(environment.suiSpecific.api.uri);
	// 	const balances: GetObjectDataResponse[] = await provider.getCoinBalancesOwnedByAddress(ownerAccout, '0x2::sui::SUI');
	// 	let coinToMergeObjectId: string = '';
	// 	let biggestCoinBalance: number = 0;
	//
	// 	for (const balanceObj of balances) {
	// 		// @ts-ignore
	// 		const coinBalance: number = balanceObj.details?.data?.fields?.balance || 0;
	// 		// @ts-ignore
	// 		const coinId: string = balanceObj.details?.data?.fields?.id?.id;
	//
	// 		if (coinId === primaryCoin) {
	// 			continue;
	// 		}
	//
	// 		if (coinBalance > biggestCoinBalance && coinId) {
	// 			biggestCoinBalance = coinBalance;
	// 			coinToMergeObjectId = coinId;
	// 		}
	// 	}
	//
	// 	return coinToMergeObjectId;
	// }
}
