import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaucetComponent } from './components/faucet/faucet.component';

const routes: Routes = [
	{
		path: '',
		component: FaucetComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TestnetRoutingModule {
}
