import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ONFTGatewayComponent } from './components/onft-gateway/onft-gateway.component';

const routes: Routes = [
	{
		path: '',
		component: ONFTGatewayComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ONFTGatewayRoutingModule {
}
