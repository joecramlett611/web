import { Routes } from '@angular/router';

export const oNFTGatewayRoutes: Routes = [
	{
		path: 'gateway',
		loadChildren: () => import('./onft-gateway.module').then(m => m.ONFTGatewayModule),
		data: { preload: false },
	},
];
