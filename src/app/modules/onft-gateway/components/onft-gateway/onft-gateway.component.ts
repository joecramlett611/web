import { Component, OnDestroy, OnInit } from '@angular/core';
import Web3Service from '../../../web3/services/web3.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Subscription } from 'rxjs';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import NotificationsService from '../../../shared/services/notifications.service';
import { environment } from '../../../../../environments/environment';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ContractReceipt, ContractTransaction } from 'ethers';
import ONFTGatewayService from '../../services/nft-gateway.service';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';

@Component({
	selector: 'app-onft-gateway',
	templateUrl: './onft-gateway.component.html',
	styleUrls: ['./onft-gateway.component.scss']
})
export class ONFTGatewayComponent implements OnInit, OnDestroy {
	public get isV1User(): boolean {
		return this.isConnectedToWallet && [
			'0x469f25c297b2e84898cfb1ce596f2553285f3e9b',
			'0x6ff5723435b7dfc2371b57fb5cb4c373e5995c78',
			'0xbd1df00dD7021b1c21e44140150F5F5c5D800fBC',
			'0x2fAAAa87963fdE26B42FB5CedB35a502d3ee09B3',
		].includes(this.userAddress);
	}

	public get differentVersionNumber(): number {
		return this.gatewayVersion === this.newestGatewayVersion ? 1 : this.newestGatewayVersion;
	}

	public get isBridging(): boolean {
		return this.state?.status && (this.state.status === 'approving' || this.state.status === 'pending' || this.state.status === 'completed');
	}

	public get isDisabled(): boolean {
		return this.tokenURIError
			|| this.form.get('collectionAddress').value === undefined
			|| this.form.get('tokenId').value === undefined
			|| this.isBridging;
	}

	public isConnectedToWallet: boolean;
	public connectedChain: ChainSymbolEnum;
	public state: { status: 'pending' | 'approving' | 'approval_error' | 'error' | 'completed' };
	public form: FormGroup;
	public selectedChainName: SupportedChainNameEnum;
	public chainNameToSymbolMap: Partial<ChainNameToSymbolMap>;
	public tokenURI: string;
	public tokenURIError: boolean;
	public tokenImage: string;
	public isTokenImageLoaded: boolean;
	public gatewayVersion: 1 | 2;
	private readonly newestGatewayVersion: 1 | 2 = 2;
	private subscriptions: Subscription;
	private userAddress: string;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly formBuilder: FormBuilder,
		private readonly oNFTGatewayService: ONFTGatewayService,
		private readonly notificationService: NotificationsService,
	) {
	}

	public ngOnInit(): void {
		this.gatewayVersion = this.newestGatewayVersion;
		this.subscriptions = new Subscription();
		this.chainNameToSymbolMap = {};

		for (const chainName in environment.chainNameToSymbolMap) {
			if (
				!environment.chainNameToSymbolMap[chainName]
				|| !this.isSupportedNetwork(environment.chainNameToSymbolMap[chainName])
			) {
				continue;
			}
			this.chainNameToSymbolMap[chainName] = environment.chainNameToSymbolMap[chainName];
		}

		this.form = this.formBuilder.group({
			collectionAddress: [undefined, Validators.maxLength(150)],
			tokenId: [undefined, Validators.required],
			dstChainSymbol: [undefined, Validators.required],
		});

		this.subscriptions.add(
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isWalletConnected();
					this.userAddress = await this.web3Service.getUserAddress();

					if (!this.isConnectedToWallet || !this.userAddress) {
						return;
					}
					clearInterval(walletInterval);
					this.connectedChain = this.web3Service.getConnectedChainSymbol();
					this.chainNameToSymbolMap[environment.chainSymbolToNameMap[this.connectedChain]] = undefined;
					this.onTargetChainChange(
						this.connectedChain !== ChainSymbolEnum.eth
							? SupportedChainNameEnum.Ethereum
							: SupportedChainNameEnum.Polygon
					);
				}, 250);
			}),
		);

		this.subscriptions.add(
			this.form.valueChanges.pipe(
				debounceTime(500),
				distinctUntilChanged(),
			).subscribe(() => {
				if (!this.form.get('collectionAddress').value || this.form.get('tokenId').value === undefined) {
					return;
				}

				this.onTokenChange();
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public async onSubmit(): Promise<void> {
		if (this.tokenURIError) {
			return;
		}

		const collectionAddress: string = this.form.get('collectionAddress').value;
		const tokenId: number = this.form.get('tokenId').value;
		const dstChainSymbol: ChainSymbolEnum = this.form.get('dstChainSymbol').value;

		this.state = {
			status: null,
		};

		try {
			const tx: ContractTransaction = await this.oNFTGatewayService.sendTo(
				this.gatewayVersion,
				collectionAddress,
				tokenId,
				dstChainSymbol,
				this.state,
			);

			const txResult: ContractReceipt = await tx.wait();
			this.notificationService.success('Bridging NFT', 'Your NFT will be transferred to the destination chain in a few minutes');

			if (txResult && txResult.status !== 1) {
				this.notificationService.error('Unknown status', 'Couldn\'t confirm the tx status. Please, check it in the blockchain explorer.');
			}
		} catch (e) {
			console.error(e);
			this.notificationService.error('Error', 'Couldn\'t initialize bridging. Please, try again in a moment.');
		}
	}

	public connectWallet(): void {
		this.web3Service.selectWalletType();
	}

	public onTargetChainChange(chainName: SupportedChainNameEnum): void {
		const chainSymbol: ChainSymbolEnum = this.chainNameToSymbolMap[chainName];

		if (!chainSymbol) {
			this.notificationService.error('Invalid chain');
			return;
		}

		this.selectedChainName = chainName;
		this.form.get('dstChainSymbol').setValue(chainSymbol);
	}

	public mapChainNameToEnum(chainName: string): SupportedChainNameEnum {
		return chainName as SupportedChainNameEnum;
	}

	public toggleVersion(): void {
		this.gatewayVersion = this.gatewayVersion === this.newestGatewayVersion ? 1 : this.newestGatewayVersion;
	}

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol);
	}

	private async onTokenChange(): Promise<void> {
		this.tokenImage = undefined;
		this.isTokenImageLoaded = false;
		this.tokenURIError = false;
		this.tokenURI = undefined;
		const collectionAddress: string = this.form.get('collectionAddress').value;
		const tokenId: number = this.form.get('tokenId').value;

		if (!collectionAddress || tokenId === undefined || tokenId === null || tokenId.toString() === '') {
			return;
		}

		try {
			this.tokenURI = await this.oNFTGatewayService.getTokenURI(collectionAddress, tokenId);

			if (this.tokenURI) {
				fetch(this.tokenURI)
					.then((response) => response.json())
					.then((metadata?: INFTMetadata) => {
						if (!metadata || !metadata.image) {
							return;
						}

						this.tokenImage = metadata.image.replace('ipfs://', 'https://omnisea.infura-ipfs.io/ipfs/');
					});
			}
		} catch (e) {
			console.error(e);
			this.tokenURIError = true;
			this.notificationService.error('Token not found');
		}
	}
}
