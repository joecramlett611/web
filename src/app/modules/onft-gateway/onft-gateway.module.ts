import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import ONFTGatewayService from './services/nft-gateway.service';
import { ONFTGatewayComponent } from './components/onft-gateway/onft-gateway.component';
import { ONFTGatewayRoutingModule } from './onft-gateway-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		ONFTGatewayComponent,
	],
	providers: [
		ONFTGatewayService,
	],
	imports: [
		SharedModule,
		ONFTGatewayRoutingModule,
		FormsModule,
		ReactiveFormsModule,
	],
	exports: [
		ONFTGatewayComponent,
	],
})
export class ONFTGatewayModule {
}
