import { Injectable } from '@angular/core';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BigNumber, ContractTransaction, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { environment } from '../../../../environments/environment';
import NotificationsService from '../../shared/services/notifications.service';
import { IEstimatedFees } from '../../omnichain/interfaces/estimated-fees.interface';
import { OmnichainActionTypeEnum } from '../../omnichain/enums/omnichain-action-type.enum';
import OmnichainService from '../../omnichain/services/omnichain.service';

@Injectable()
export default class ONFTGatewayService {
	public readonly oNFTGatewayABI: string[] = [
		'function sendTo(address collection, uint256 tokenId, string memory dstChainName, uint256 gas, uint256 redirectFee) public payable',
		'event Transferred(address srcCollection, uint256 tokenId, address owner)',
	];
	private readonly erc721MetadataAbi: string[] = [
		'function tokenURI(uint256 tokenId) external view returns (string memory)',
		'function approve(address to, uint256 tokenId) external',
		'function getApproved(uint256 tokenId) external view returns (address operator)',
	];
	private bridgingStatusInterval?: NodeJS.Timer;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly omnichainService: OmnichainService,
		private readonly notificationsService: NotificationsService,
	) {
	}

	public async sendTo(
		gatewayVersion: number,
		collectionAddress: string,
		tokenId: number,
		dstChainSymbol: ChainSymbolEnum,
		state: { status: 'pending' | 'approving' | 'approval_error' | 'error' | 'completed' },
	): Promise<ContractTransaction> {
		if (this.bridgingStatusInterval) {
			try {
				clearInterval(this.bridgingStatusInterval);
			} catch (e) {
				console.error(e);
			}
		}
		if (!dstChainSymbol) {
			throw new Error('No destination chain');
		}
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to bridge');
		}
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to bridge');
		}
		const gatewayContractAddress: string = this.getGatewayContractAddress(connectedChain, gatewayVersion);
		const gatewayContract = new ethers.Contract(
			gatewayContractAddress,
			this.oNFTGatewayABI,
			signer,
		);
		const erc721Contract = new ethers.Contract(
			collectionAddress,
			this.erc721MetadataAbi,
			signer,
		);
		const allowedOperator: string = await erc721Contract.getApproved(tokenId);

		if (allowedOperator !== gatewayContractAddress) {
			try {
				state.status = 'approving';
				const approvalTx: ContractTransaction = await erc721Contract.approve(gatewayContractAddress, tokenId);

				await approvalTx.wait();
			} catch (e) {
				console.error(e);
				state.status = 'approval_error';
				throw e;
			}
		}
		state.status = 'pending';
		let estimatedFees: IEstimatedFees;

		try {
			const encodedPayload: string = this.web3Service.encodePayload(
				[
					collectionAddress,
					tokenId,
					dstChainSymbol,
					environment.chainSymbolToBridgeNFTGas[dstChainSymbol],
					0,
				],
				['address', 'uint256', 'string', 'uint256', 'uint256'],
			);

			estimatedFees = await this.omnichainService.getEstimatedFees(
				dstChainSymbol,
				encodedPayload,
				OmnichainActionTypeEnum.NFT_GATEWAY_SEND,
			);
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Cannot estimate bridging fee');
			state.status = 'error';

			return;
		}

		try {
			const connectedUserAddress: string = await signer.getAddress();
			const connectedUserBalance: BigNumber = await this.web3Service.getNativeAssetBalance(connectedUserAddress);
			const fee: BigNumber = BigNumber.from(estimatedFees.fee);
			const safeFee: BigNumber = fee.add(fee.div(10)); // TODO: (Must) Why LZ Endpoint estimateFees() calculates slightly too low fee?

			if (connectedUserBalance.lt(safeFee)) {
				const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
				const nativeAsset: string = environment.chainSymbolToNativeAsset[connectedChain];
				const tipMessage: string = `At least ${estimatedFeeFormatted} ${nativeAsset} required`;

				this.notificationsService.error('Insufficient balance', tipMessage);
				throw new Error('[ONFTGateway] Insufficient balance');
			}
			const tx: ContractTransaction = await gatewayContract.sendTo(
				collectionAddress,
				tokenId,
				dstChainSymbol,
				environment.chainSymbolToBridgeNFTGas[dstChainSymbol],
				estimatedFees.redirectFee,
				{value: safeFee},
			);

			try {
				const dstGatewayContract = new ethers.Contract(
					this.getGatewayContractAddress(dstChainSymbol, gatewayVersion),
					this.oNFTGatewayABI,
					this.web3Service.getEVMNetworkProvider(dstChainSymbol),
				);

				dstGatewayContract.on('Transferred', async (srcCollectionAddress: string, tId: BigNumber, ownerAddress: string) => {
					if (ownerAddress === connectedUserAddress && tId.toNumber() === tokenId) {
						this.notificationsService.success('Success', 'NFT bridging completed');
						state.status = 'completed';
					}
				});
			} catch (e) {
				console.error(e);
			}

			return tx;
		} catch (e) {
			console.error(e);
			state.status = 'error';
			const errorMessage: string = e?.message || e;
			const errorDataMessage: string = e?.data?.message;

			if (errorMessage.includes('[ONFTGateway]') || errorDataMessage.includes('[ONFTGateway]')) {
				console.error(e);

				return;
			}

			if (errorDataMessage) {
				if (errorDataMessage.includes('insufficient funds for gas') || errorDataMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[connectedChain];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}
			}

			if (errorMessage) {
				if (errorMessage.includes('MetaMask Tx Signature: User denied transaction signature')) {
					// Canceled on User's action
					return;
				}

				if (errorMessage.includes('insufficient funds for gas') || errorMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[connectedChain];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}

				if (errorMessage.includes('insufficient balance for transfer')) {
					this.notificationsService.error('Insufficient funds', 'Looks like you currently don\'t have required amount to bridge');
				}
			}

			throw e;
		}
	}

	public async getTokenURI(collectionAddress: string, tokenId: number): Promise<string> {
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to get tokenURI');
		}
		const erc721Contract = new ethers.Contract(
			collectionAddress,
			this.erc721MetadataAbi,
			signer,
		);
		const ipfsURI: string = await erc721Contract.tokenURI(tokenId);

		return ipfsURI.replace('ipfs://', 'https://omnisea.infura-ipfs.io/ipfs/');
	}

	private getGatewayContractAddress(connectedChain: ChainSymbolEnum, version: number): string {
		switch (version) {
			case 1:
				return environment.v1ONFTGatewayAddressesMap[connectedChain];
			case 2:
			default:
				return environment.oNFTGatewayAddressesMap[connectedChain];
		}
	}
}
