import { Routes } from '@angular/router';

export const userProfileRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./user-profile.module').then(m => m.UserProfileModule),
		data: { preload: false },
	},
];
