import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UserActivityComponent } from './components/user-activity/user-activity.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';

@NgModule({
	declarations: [
		UserActivityComponent,
		UserProfileComponent,
	],
	imports: [
		UserProfileRoutingModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
	],
	exports: [
		UserActivityComponent,
		UserProfileComponent,
	],
})
export class UserProfileModule {
}
