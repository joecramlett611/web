import { Component, OnDestroy, OnInit } from '@angular/core';
import UserActivityService from '../../../user/services/user-activity-service';
import { Subscription } from 'rxjs';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import * as moment from 'moment';
import { environment } from '../../../../../environments/environment';
import { Message } from '@layerzerolabs/scan-client';
import { Params, Router } from '@angular/router';
import { UserActivityTypeEnum } from '../../../collections/enums/user-activity-type.enum';
import { IUserActivity } from '../../../collections/interfaces/user-activity.interface';
import { IClaimRefundTx } from '../../../collections/interfaces/claim-refund-tx.interface';
import { IClaimEarnedTx } from '../../../collections/interfaces/claim-earned-tx.interface';
import { IMintData } from '../../../collections/interfaces/mint-data.interface';

@Component({
	selector: 'app-user-activity',
	templateUrl: './user-activity.component.html',
	styleUrls: ['./user-activity.component.scss']
})
export class UserActivityComponent implements OnInit, OnDestroy {
	public userActivity: {
		[UserActivityTypeEnum.CREATE]: Array<IUserActivity<ICollectionDetails>>,
		[UserActivityTypeEnum.MINT]: Array<IUserActivity<IMintData>>,
		[UserActivityTypeEnum.REFUND]: Array<IUserActivity<IClaimRefundTx>>,
		[UserActivityTypeEnum.CLAIM_EARNED]: Array<IUserActivity<IClaimEarnedTx>>,
	};
	public readonly loadingState: {
		[UserActivityTypeEnum.CREATE]: boolean,
		[UserActivityTypeEnum.MINT]: boolean,
		[UserActivityTypeEnum.CLAIM_EARNED]: boolean,
		[UserActivityTypeEnum.REFUND]: boolean,
	} = {
		[UserActivityTypeEnum.CREATE]: false,
		[UserActivityTypeEnum.MINT]: false,
		[UserActivityTypeEnum.CLAIM_EARNED]: false,
		[UserActivityTypeEnum.REFUND]: false,
	};
	public selectedActivityType: UserActivityTypeEnum;

	private subscriptions: Subscription;

	constructor(
		private readonly userActivityService: UserActivityService,
		private readonly router: Router,
	) {
	}

	public async ngOnInit(): Promise<void> {
		this.userActivity = {
			[UserActivityTypeEnum.CREATE]: [],
			[UserActivityTypeEnum.MINT]: [],
			[UserActivityTypeEnum.CLAIM_EARNED]: [],
			[UserActivityTypeEnum.REFUND]: [],
		};
		this.selectedActivityType = UserActivityTypeEnum.CREATE;
		this.subscriptions = new Subscription();
		this.subscriptions.add(
			this.userActivityService.userActivity$.CREATE.subscribe((userActivity) => {
				const createCollectionsUserActivity: Array<IUserActivity<ICollectionDetails>> = userActivity.sort(
					(a, b) => b.lzMessage.updated - a.lzMessage.updated,
				);

				this.userActivity[UserActivityTypeEnum.CREATE] = [...createCollectionsUserActivity];
			}),
		);

		this.subscriptions.add(
			this.userActivityService.userActivity$.MINT.subscribe((userActivity) => {
				const mintUserActivity: Array<IUserActivity<IMintData>> = userActivity.sort(
					(a, b) => b.lzMessage.updated - a.lzMessage.updated,
				);

				this.userActivity[UserActivityTypeEnum.MINT] = [...mintUserActivity];
			}),
		);

		this.subscriptions.add(
			this.userActivityService.userActivity$.REFUND.subscribe((userActivity) => {
				const refundUserActivity: Array<IUserActivity<IClaimRefundTx>> = userActivity.sort(
					(a, b) => b.lzMessage.updated - a.lzMessage.updated,
				);

				this.userActivity[UserActivityTypeEnum.REFUND] = [...refundUserActivity];
			}),
		);

		this.subscriptions.add(
			this.userActivityService.userActivity$.CLAIM_EARNED.subscribe((userActivity) => {
				const claimEarnedUserActivity: Array<IUserActivity<IClaimEarnedTx>> = userActivity.sort(
					(a, b) => b.lzMessage.updated - a.lzMessage.updated,
				);

				this.userActivity[UserActivityTypeEnum.CLAIM_EARNED] = [...claimEarnedUserActivity];
			}),
		);

		this.userActivityService.syncUserActivities(this.loadingState);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public getImageFromFileURI(fileURI: string) {
		if (!fileURI) {
			return undefined;
		}

		return `https://omnisea.infura-ipfs.io/ipfs/${fileURI.split('_')[0]}`;
	}

	public getHumanizedDateFromTimestamp(unix: number): string {
		if (!unix) {
			return undefined;
		}

		const now: number = moment().unix();
		const secondsDiff: number = moment(unix).diff(now);

		return moment.duration(secondsDiff, 'seconds').humanize(true);
	}

	public onActivityTypeChange(activityType: string): void {
		this.selectedActivityType = UserActivityTypeEnum[activityType];
	}

	public goToLzScan(userActivity: IUserActivity): void {
		const lzMessage: Message = userActivity.lzMessage;
		const URI: string = `${environment.lzScanURI}/${lzMessage.srcChainId}/address/${lzMessage.srcUaAddress}/message/${lzMessage.dstChainId}/address/${lzMessage.dstUaAddress}/nonce/${lzMessage.srcUaNonce}`;

		window.open(URI, '_blank');
	}

	public goToCollection(
		id: number,
	): void {
		const queryParams: Params = {
			id,
		};

		this.router.navigate(
			['/drop'],
			{queryParams},
		);
	}
}
