import { IStepperStepConfig } from '../interfaces/stepper-step-config.interface';

export type StepperConfigType = IStepperStepConfig[];
