import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
	NgxUiLoaderModule,
} from 'ngx-ui-loader';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { ToastrComponent } from './components/toastr/toastr.component';
import { ToastrErrorComponent } from './components/toastr/toastr-error';
import { ToastrModule } from 'ngx-toastr';
import NotificationsService from './services/notifications.service';
import StaticFilesService from './services/static-files.service';
import { OmnichainModule } from '../omnichain/omnichain.module';
import GlobalStateService from './global-state.service';

@NgModule({
	declarations: [
		ToastrComponent,
		ToastrErrorComponent,
	],
	imports: [
		CommonModule,
		RouterModule,
		FlexLayoutModule,
		// @ts-ignore
		NgxUiLoaderModule.forRoot({
			bgsColor: '#ec255a',
			bgsOpacity: 0.5,
			bgsPosition: 'center-center',
			bgsSize: 60,
			bgsType: 'ball-scale-multiple',
			blur: 11,
			delay: 0,
			fastFadeOut: true,
			fgsColor: '#ec255a',
			fgsPosition: 'center-center',
			fgsSize: 60,
			fgsType: 'ball-scale-multiple',
			gap: 24,
			logoPosition: 'center-center',
			logoSize: 120,
			logoUrl: '',
			masterLoaderId: 'master',
			overlayBorderRadius: '0',
			overlayColor: 'rgba(40, 40, 40, 0.8)',
			pbColor: 'ec255a',
			pbDirection: 'ltr',
			pbThickness: 3,
			hasProgressBar: true,
			textColor: '#000000',
			textPosition: 'bottom-center',
			maxTime: -1,
			minTime: 300
		}),
		NgxSkeletonLoaderModule,
		NgxTippyModule,
		ToastrModule.forRoot({
			toastComponent: ToastrComponent,
			toastClass: 'notyf confirm',
			positionClass: 'toast-bottom-center',
			preventDuplicates: true,
			includeTitleDuplicates: true,
			tapToDismiss: true,
			progressBar: true,
			maxOpened: 3,
		}),
		OmnichainModule,
	],
	providers: [
		GlobalStateService,
		NotificationsService,
		StaticFilesService,
	],
	exports: [
		CommonModule,
		FlexLayoutModule,
		NgxUiLoaderModule,
		NgxSkeletonLoaderModule,
		NgxTippyModule,
		ToastrModule,
		ToastrComponent,
		ToastrErrorComponent,
		OmnichainModule,
	],
})
export class SharedModule {
}
