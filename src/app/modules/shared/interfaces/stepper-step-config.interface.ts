import { StepperStatusEnum } from '../enums/stepper-status.enum';

export interface IStepperStepConfig {
	title: string;
	subtitle?: string;
	iconText?: string;
	isActive: boolean;
	status: StepperStatusEnum;
}
