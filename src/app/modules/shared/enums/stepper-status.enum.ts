export enum StepperStatusEnum {
	PENDING = 'PENDING',
	COMPLETE = 'COMPLETE',
	ERROR = 'ERROR',
}
