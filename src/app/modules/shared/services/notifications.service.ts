import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ToastrErrorComponent } from '../components/toastr/toastr-error';
import { IndividualConfig } from 'ngx-toastr/toastr/toastr-config';
import { ToastrComponent } from '../components/toastr/toastr.component';

@Injectable()
export default class NotificationsService {
	constructor(private readonly toastrService: ToastrService) {
	}

	public success(title: string, message?: string, config?: Partial<IndividualConfig>): void {
		this.toastrService.success(message || '', title || 'Success', {
			toastComponent: ToastrComponent,
			toastClass: 'notyf confirm',
			...config,
		});
	}

	public error(title: string, message?: string, config?: Partial<IndividualConfig>): void {
		this.toastrService.error(message || '', title || 'Error', {
			toastComponent: ToastrErrorComponent,
			toastClass: 'notyf error',
			...config,
		});
	}
}
