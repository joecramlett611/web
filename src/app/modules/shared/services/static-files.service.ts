import { Injectable } from '@angular/core';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';

@Injectable()
export default class StaticFilesService {
	public readonly filesUrls: Record<string, string> = {};

	constructor(private readonly firebaseStorageService: FirebaseStorageService) {
	}

	public async getFiles(filesNames: string[]): Promise<Record<string, string>> {
		const storagePaths = {};

		for (const fileName of filesNames) {
			storagePaths[fileName] = await this.getFile(fileName);
		}

		return storagePaths;
	}

	public async getFile(fileName: string): Promise<string> {
		try {
			if (this.filesUrls[fileName]) {
				return this.filesUrls[fileName];
			}
			this.filesUrls[fileName] = await this.firebaseStorageService.getFileURL(fileName);

			return this.filesUrls[fileName];
		} catch (e) {
			console.error(e);
		}
	}
}
