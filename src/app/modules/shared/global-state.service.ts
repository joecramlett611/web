import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GlobalActionTypeEnum } from './enums/global-action-type.enum';

@Injectable()
export default class GlobalStateService {
	public readonly actions: Record<GlobalActionTypeEnum, BehaviorSubject<unknown>> = {
		[GlobalActionTypeEnum.SELECT_WALLET_TYPE]: new BehaviorSubject<void>(undefined),
	};
}
