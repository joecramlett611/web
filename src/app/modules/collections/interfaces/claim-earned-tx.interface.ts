import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BigNumberish } from 'ethers';
import { SupportedAssetEnum } from '../../web3/enum/supported-asset.enum';
import { IsolatedAssetEnum } from '../../web3/enum/isolated-asset.enum';

export interface IClaimEarnedTx {
	collectionId: number;
	collectionName: string;
	dstChainSymbol: ChainSymbolEnum;
	srcChainSymbol: ChainSymbolEnum;
	at: number;
	price: BigNumberish;
	assetName: SupportedAssetEnum | IsolatedAssetEnum;
	txHash: string;
	status?: number;
}
