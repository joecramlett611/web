export interface ICreateCollectionTx {
	collectionId: number;
	hash: string;
}
