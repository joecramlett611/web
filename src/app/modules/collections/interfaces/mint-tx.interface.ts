import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';

export interface IMintTx {
	collectionId: number;
	dstChainSymbol: ChainSymbolEnum;
	srcChainSymbol: ChainSymbolEnum;
	txHash?: string;
	at: number;
	status?: number;
}
