import { UserActivityTypeEnum } from '../enums/user-activity-type.enum';
import { Message } from '@layerzerolabs/scan-client';

export interface IUserActivity<MetadataType = undefined> {
	type: UserActivityTypeEnum;
	txHash: string;
	status: 'PENDING' | 'SUCCESS' | 'ERROR';
	metadata?: MetadataType;
	lzMessage?: Message & {updated?: number};
}
