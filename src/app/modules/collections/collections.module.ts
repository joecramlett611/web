import { NgModule } from '@angular/core';
import { CollectionsRoutingModule } from './collections-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CollectionDetailsComponent } from './components/collection-details/collection-details.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
	declarations: [
		CollectionDetailsComponent,
	],
	imports: [
		CollectionsRoutingModule,
		SharedModule,
		SwiperModule,
	],
	exports: [
		CollectionDetailsComponent,
	],
})
export class CollectionsModule {
}
