export type NftAttributes = Record<string, string | boolean | number>;
