import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';

export type CollectionRefundsListType = Record<ChainSymbolEnum, number>;
