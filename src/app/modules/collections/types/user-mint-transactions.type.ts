import { IMintTx } from '../interfaces/mint-tx.interface';

export type UserMintTransactionsType = Record<string, Record<string, IMintTx>>;
