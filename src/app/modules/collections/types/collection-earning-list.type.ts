import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';

export type CollectionEarningListType = Record<ChainSymbolEnum, number>;
