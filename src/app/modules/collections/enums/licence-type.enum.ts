export enum LicenceTypeEnum {
	CREATOR = 'CREATOR',
	OWNER = 'OWNER',
	ANYONE = 'ANYONE',
}
