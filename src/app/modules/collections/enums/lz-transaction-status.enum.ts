export enum LzTransactionStatusEnum {
	INFLIGHT = 'INFLIGHT',
	DELIVERED = 'DELIVERED',
	FAILED = 'FAILED',
}
