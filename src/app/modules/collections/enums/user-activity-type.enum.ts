export enum UserActivityTypeEnum {
	CREATE = 'CREATE',
	MINT = 'MINT',
	REFUND = 'REFUND',
	CLAIM_EARNED = 'CLAIM_EARNED',
}
