export enum MusicCategoryEnum {
	ROCK = 'ROCK',
	POP = 'POP',
	ELECTRONIC_DANCE = 'ELECTRONIC_DANCE',
	RNB_SOUL = 'RNB_SOUL',
	CLASSICAL = 'CLASSICAL',
	JAZZ = 'JAZZ',
	BLUES = 'BLUES',
	HIPHOP = 'HIPHOP',
	PUNK = 'PUNK',
	COUNTRY = 'COUNTRY',
	FUNK = 'FUNK',
	INDIE = 'INDIE',
	KPOP = 'KPOP',
	GENERATIVE = 'GENERATIVE',
	REGGAE = 'REGGAE',
	OTHERS = 'OTHERS',
}
