import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatestWith, debounceTime, of, Subject, Subscription, switchMap } from 'rxjs';
import Web3Service from '../../../web3/services/web3.service';
import { BigNumber, BigNumberish, ContractReceipt, ContractTransaction, ethers } from 'ethers';
import { Web3Provider } from '@ethersproject/providers';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { environment } from '../../../../../environments/environment';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import { CollectionIpfsMetadata } from '../../../create/dto/collection-metadata.dto';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import * as moment from 'moment';
import CollectionsService from '../../../chain-data/services/collections.service';
import { Contract } from '@ethersproject/contracts';
import { ActivatedRoute, Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import FirebaseStorageService from '../../../firebase/services/firebase-storage.service';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import NftsService from '../../../nfts/services/nfts.service';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import { CollectionRefundsListType } from '../../types/collection-refunds-list.type';
import { CollectionEarningListType } from '../../types/collection-earning-list.type';
import { IProfile } from '../../../user/interfaces/profile.interface';
import ProfileService from '../../../user/services/profile.service';
import { ContentTypeEnum } from '../../enums/content-type.enum';
import { IEstimatedFees } from '../../../omnichain/interfaces/estimated-fees.interface';
import OmnichainService from '../../../omnichain/services/omnichain.service';
import { OmnichainActionTypeEnum } from '../../../omnichain/enums/omnichain-action-type.enum';
import { Title } from '@angular/platform-browser';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';
import Swiper, { Autoplay, FreeMode } from 'swiper';
import 'swiper/css';
import { ConnectedWalletDetails } from '../../../web3/types/connected-wallet-details.type';
import { WalletTypeEnum } from '../../../web3/enum/wallet-type.enum';
import AptosMartianWalletService from '../../../aptos/services/aptos-martian-wallet.service';
import { IAptosTransactionDetails } from '../../../aptos/interfaces/aptos-transaction-details.interface';
import { AptosResourceTypeEnum } from '../../../aptos/enums/aptos-event-type.enum';
import AptosPetraWalletService from '../../../aptos/services/aptos-petra-wallet.service';
import { WalletProviderEnum } from '../../../web3/enum/wallet-provider.enum';
import { IAllowlist } from '../../../aptos/interfaces/allowlist.interface';
// import SuiEthosWalletService from '../../../sui/services/sui-ethos-wallet.service';
// import { SuiTransactionResponse } from '@mysten/sui.js/dist/types/transactions';
// import SuiSuietWalletService from '../../../sui/services/sui-suiet-wallet.service';
import AptosPontemWalletService from '../../../aptos/services/aptos-pontem-wallet.service';

@Component({
	selector: 'app-collection-details',
	templateUrl: './collection-details.component.html',
	styleUrls: ['./collection-details.component.scss'],
})
export class CollectionDetailsComponent implements OnInit, OnDestroy {
	public get isNetworkSupportedIfConnected(): boolean {
		return !this.connectedChainSymbol
			|| [...environment.supportedChains, ...environment.isolatedChains].includes(this.connectedChainSymbol);
	}

	public get isConnectedToIsolatedChain(): boolean {
		return this.isConnectedToWallet && environment.isolatedChains.includes(this.connectedChainSymbol);
	}

	public get isOngoing(): boolean {
		const isRemaining: boolean = !this.totalSupply || this.totalSupply > this.totalMinted;

		if (!isRemaining) {
			return false;
		}
		const now: number = moment().unix();

		if (!this.dateFromAsUnix && !this.dateToAsUnix) {
			return true;
		}

		if (this.dateToAsUnix > 0) {
			return now >= this.dateFromAsUnix && now <= this.dateToAsUnix;
		}

		return now >= this.dateFromAsUnix;
	}

	public get isBeforeStart(): boolean {
		return this.dateFromAsUnix > 0 && moment().unix() < this.dateFromAsUnix;
	}

	public get isFinished(): boolean {
		return this.dateToAsUnix > 0 && moment().unix() > this.dateToAsUnix;
	}

	public get isCreator(): boolean {
		return this.userAddress?.length > 0 && this.details && this.userAddress === this.details.creator;
	}

	public get isConnectedToCollectionChain(): boolean {
		return this.collectionChainSymbol === this.connectedChainSymbol;
	}

	public get finalPriceFormatted(): string {
		return this.mintPriceInUnits ? (Number(this.mintPriceInUnits) * this.mintQuantity).toString() : '0';
	}

	public get actionButtonLabel(): string {
		if (!this.mintedCountLoaded || !this.isLoaded) {
			return 'Loading...';
		}

		if (!this.isAssetBalanceError && this.assetBalance === undefined) {
			return 'Checking balance...';
		}

		if (!this.isBalanceEnoughToMint) {
			if (!environment.isMainnet) {
				return 'Fund account';
			}

			return `Insufficient ${this.details.assetName}`;
		}

		if (!this.isAllowedToMint) {
			return 'Not Whitelisted';
		}

		if (!this.isNetworkSupportedIfConnected) {
			return 'Unsupported Network';
		}

		if (this.isMinting) {
			return 'Minting...';
		}

		if (this.isApprovingAsset) {
			return 'Approving...';
		}

		if (this.isMintingError) {
			return 'Try again';
		}

		return 'Mint';
	}

	public get isActionButtonDisabled(): boolean {
		return this.isConnectedToWallet && (!this.canMint() || this.isMinting || this.isApprovingAsset || !this.isNetworkSupportedIfConnected);
	}

	public get publicFromFormatted(): string {
		if (!this.allowlist?.publicFrom) {
			return ' Live';
		}
		const secondsToStart: number = this.allowlist.publicFrom - moment().unix();

		if (secondsToStart <= 0) {
			return ' Live';
		}

		return this.allowlist?.publicFrom < moment().unix()
			? 'Public Live'
			: 'Public ' + (moment.duration(secondsToStart, 'seconds').humanize(true)).toLowerCase();
	}

	public get isAllowedToMint(): boolean {
		if (this.allowlist?.isEnabled && moment().unix() < this.allowlist.publicFrom) {
			return this.isUserAllowlisted !== false; // undefined or true
		}

		return true;
	}

	public get maxToMint(): number {
		if (!this.allowlist?.isEnabled) {
			return this.remainingCount;
		}
		const publicFrom: number = this.allowlist.publicFrom;
		const now: number = moment().unix();

		if (now < publicFrom && this.isUserAllowlisted) {
			return this.allowlist.maxPerAddress;
		}

		return this.allowlist.publicMaxPerAddress;
	}

	public get maxMintInfo(): string {
		if (!this.allowlist?.isEnabled) {
			return '';
		}
		const publicFrom: number = this.allowlist.publicFrom;
		const now: number = moment().unix();

		if (now < publicFrom && this.isUserAllowlisted) {
			return `You're whitelisted to mint ${this.allowlist.maxPerAddress} tokens.`;
		}

		return `Maximum number of tokens to mint during Public Mint is ${this.allowlist.publicMaxPerAddress}.`;
	}

	public get isBalanceEnoughToMint(): boolean {
		if (!this.mintPriceInUnits || !this.details || !this.connectedChainSymbol) {
			return true;
		}

		if (!this.assetBalance) {
			return false;
		}
		let assetBalance: number = 0;

		if (this.connectedWalletDetails.type === WalletTypeEnum.EVM) {
			assetBalance = Number(ethers.utils.formatUnits(
				this.assetBalance,
				environment.assetToDigits[this.details.assetName][this.connectedChainSymbol],
			));
		}

		if (this.connectedWalletDetails.type === WalletTypeEnum.APTOS) {
			assetBalance = this.assetBalance.toNumber() / (Math.pow(10, environment.aptosSpecific.coins.APT.decimals));
		}

		// if (this.connectedWalletDetails.type === WalletTypeEnum.SUI) {
		// 	assetBalance = this.assetBalance.toNumber() / (Math.pow(10, environment.suiSpecific.coins.SUI.decimals));
		// }

		return assetBalance >= this.mintPriceInUnits;
	}

	public isLoaded: boolean;
	public isLoadingError: boolean;
	public isMinting: boolean;
	public isMintingSuccess: boolean;
	public isMintingError: boolean;
	public isMintingCrossChain: boolean;
	public isMintedCrossChain: boolean;
	public isApprovingAsset: boolean;
	public isApproveAssetError: boolean;
	public subscriptions: Subscription;
	public isConnectedToWallet: boolean;
	public imageSrc: string | ArrayBuffer;
	public hdImageSrc: string | ArrayBuffer;
	public hdImageLoaded: boolean;
	public readonly chainSymbols: ChainSymbolEnum[];
	public readonly chainNameToSymbolMap: ChainNameToSymbolMap;
	public details: ICollectionDetails;
	public collectionAddress: string;
	public metadataURI: string;
	public collectionChainName: string;
	public srcChainName: string;
	public collectionChainSymbol: ChainSymbolEnum;
	public mintPriceInUnits: BigNumberish;
	public mintPriceFormatted: string;
	public totalSupply: number;
	public totalMinted: number;
	public mintedCountLoaded: boolean;
	public mintedOnCollectionChainCount: number;
	public remainingCount: number;
	public metadata: CollectionIpfsMetadata;
	public dateTo: string;
	public dateFrom: string;
	public hasEnded: boolean;
	public dateFromAsUnix: number;
	public dateToAsUnix: number;
	public assetBalance: BigNumber;
	public nfts: INFTDetails[];
	public selectedTab: 'details' | 'nfts' | 'description' | 'earnings' | 'refunds';
	public refundsList: Partial<CollectionRefundsListType>;
	public isRefundable: boolean;
	public refundsClaimedToChainMap: Partial<Record<ChainSymbolEnum, boolean>>;
	public earningsList: Partial<CollectionEarningListType>;
	public isEarning: boolean;
	public earningsClaimedToChainMap: Partial<Record<ChainSymbolEnum, boolean>>;
	public otherChains: Partial<Array<ChainSymbolEnum>>;
	public isRiskingSupplyExceeded: boolean;
	public profile?: IProfile;
	public contentType: ContentTypeEnum;
	public connectedChainSymbol: ChainSymbolEnum;
	public mintingErrorReason: string;
	public isMintingErrorRefundable: string;
	public nftSourceType: 'file' | 'ipfs';
	public selectedNFT: INFTDetails;
	public mintedTokenFileURI: string;
	public mintQuantity: number;
	public isUserAllowlisted: boolean;
	public allowlist: IAllowlist;
	private id: number;
	private slug?: string;
	private connectedWalletDetails: ConnectedWalletDetails;
	private isAssetBalanceError: boolean;
	private creatorAddress: string;
	private tokenFactoryContractAddress: string;
	private readonly v1MintParamsStruct: string = '(string dstChainName, address coll, uint256 mintPrice, string memory assetName, address creator, uint256 gas, uint256 redirectFee)';
	private readonly v2MintParamsStruct: string = '(string dstChainName, address coll, uint256 mintPrice, string memory assetName, uint256 quantity, address creator, uint256 gas, uint256 redirectFee)';
	private readonly tokenFactoryContractAbi: Record<number, string[]> = {
		1: [ // TODO: (must): Set for 1 old ABI
			// tslint:disable-next-line:max-line-length
			`function mintToken(${this.v1MintParamsStruct} params) public payable`,
			'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
		],
		2: [
			// tslint:disable-next-line:max-line-length
			`function mintToken(${this.v2MintParamsStruct} params) public payable`,
			'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
		],
		3: [
			// tslint:disable-next-line:max-line-length
			`function mintToken(${this.v2MintParamsStruct} params) public payable`,
			'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
		],
	};
	private readonly erc20Abi: string[] = [
		'function allowance(address owner, address spender) public view returns (uint256)',
		'function approve(address spender, uint256 amount) public returns (bool)',
	];
	private readonly erc721Abi: string[] = [
		'event TokenMinted(address collAddr, address owner, uint256 tokenId)',
	];
	private userAddress: string;
	private walletCheckOnMint: NodeJS.Timer;
	private mintTx: ContractTransaction;
	private swiper: Swiper;
	private suiCoinObject: {
		balance: number,
		coinObjectId: string,
		biggestCoinBalance: number,
	};
	private refreshSupplyInfo$: Subject<void> = new Subject<void>();

	constructor(
		private readonly web3Service: Web3Service,
		private readonly profileService: ProfileService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly collectionsService: CollectionsService,
		private readonly collectionDataService: CollectionsDataService,
		private readonly nftsService: NftsService,
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly omnichainService: OmnichainService,
		private readonly aptosMartianWalletService: AptosMartianWalletService,
		private readonly aptosPontemWalletService: AptosPontemWalletService,
		private readonly aptosPetraWalletService: AptosPetraWalletService,
		// private readonly suiEthosWalletService: SuiEthosWalletService,
		// private readonly suiSuietWalletService: SuiSuietWalletService,
		private readonly titleService: Title,
	) {
		this.chainSymbols = [...environment.supportedChains, ...environment.isolatedChains];
		this.chainNameToSymbolMap = {
			[SupportedChainNameEnum.Ethereum]: ChainSymbolEnum.eth,
			[SupportedChainNameEnum.BSC]: ChainSymbolEnum.bsc,
			[SupportedChainNameEnum.Avalanche]: ChainSymbolEnum.avax,
			[SupportedChainNameEnum.Fantom]: ChainSymbolEnum.ftm,
			[SupportedChainNameEnum.Polygon]: ChainSymbolEnum.polygon,
			[SupportedChainNameEnum.Arbitrum]: ChainSymbolEnum.arb,
			[SupportedChainNameEnum.Optimism]: ChainSymbolEnum.optimism,
			[SupportedChainNameEnum.Moonbeam]: ChainSymbolEnum.moonbeam,
			[SupportedChainNameEnum.Harmony]: ChainSymbolEnum.harmony,
			[SupportedChainNameEnum.Metis]: ChainSymbolEnum.metis,
			[SupportedChainNameEnum.Evmos]: ChainSymbolEnum.evmos,
			[SupportedChainNameEnum.Gate]: ChainSymbolEnum.gate,
			[SupportedChainNameEnum.Oasis]: ChainSymbolEnum.oasis,
			[SupportedChainNameEnum.Aptos]: ChainSymbolEnum.aptos,
			[SupportedChainNameEnum.Sui]: ChainSymbolEnum.sui,
			[SupportedChainNameEnum.Metis]: ChainSymbolEnum.metis,
			[SupportedChainNameEnum.ZkSync]: ChainSymbolEnum.zkSync,
		};
	}

	public async ngOnInit(): Promise<void> {
		this.uiLoaderService.stopAll();
		this.subscriptions = new Subscription();
		this.nfts = [];
		this.mintQuantity = 1;

		this.subscriptions.add(this.route.queryParams.pipe(
			combineLatestWith(
				this.route.params,
			),
			switchMap((params: Params[]) => {
				const queryParams: Params = params[0];
				const routeParams: Params = params[1];

				if ((!!this.id && queryParams.id !== this.id) || !!this.slug && routeParams.slug !== this.slug) {
					window.location.reload();
				}

				if (queryParams?.id) {
					this.id = queryParams.id;
					this.collectionAddress = queryParams.collectionAddress;
					this.creatorAddress = queryParams.creatorAddress;
					this.metadataURI = queryParams.metadataURI;
					this.earningsClaimedToChainMap = {};
					this.refundsClaimedToChainMap = {};

					return of(true);
				} else if (routeParams?.slug) {
					this.slug = routeParams.slug;

					return of(true);
				}
				const url: string = window.location.href;

				if (url.includes('drop/')) {
					this.slug = url.split('drop/')[1];

					return of(true);
				}

				return of(false);
			})
		).subscribe(async (isValidURL: boolean) => {
			if (!isValidURL) {
				this.notificationsService.error('Sorry', 'We couldn\'t find this Drop');

				return;
			}

			this.getCollectionDetails();
		}));

		this.subscriptions.add(
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isWalletConnected();
					this.connectedWalletDetails = await this.web3Service.getConnectedWallet();

					if (this.isConnectedToWallet && this.details) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						this.connectedChainSymbol = this.web3Service.getConnectedChainSymbol();

						if (this.allowlist?.isEnabled) {
							this.isAllowlisted().then((isAllowlisted) => {
								this.isUserAllowlisted = isAllowlisted;
							}).catch((e) => {
								this.isUserAllowlisted = true;
								console.error(e);
							});
						}

						if (!this.isConnectedToIsolatedChain) {
							this.getRefundsList();
							this.getEarningList();
						}

						if (!this.connectedChainSymbol) {
							setTimeout(async () => {
								await this.setConnectedChainData();
							}, 3000);
							return;
						}
						await this.setConnectedChainData();
					}
				}, 250);
			}),
		);

		this.refreshSupplyInfo$
			.pipe(
				debounceTime(this.mintedCountLoaded ? 5000 : 0),
			)
			.subscribe(() => {
				console.log('Get supply info at: ', moment().unix());
				this.getCollectionDataFromChain();
			});
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public connectWallet(): void {
		this.web3Service.selectWalletType();
	}

	public async onMint(): Promise<void> {
		if (!environment.isMainnet && !this.isAssetBalanceError && !this.isBalanceEnoughToMint) {
			this.goToFaucet();

			return;
		}

		if (!this.canMint()) {
			return;
		}

		this.mint();
	}

	public async refreshMetadataAndUpdateInfo(): Promise<void> {
		if (this.details?.metadata?.media?.length > 0) {
			// this.hdImageSrc = `https://omnisea.infura-ipfs.io/ipfs/${this.details.metadata.media.split('_')[0]}`;
		}
	}

	public isIsolatedChain(chainSymbol: ChainSymbolEnum): boolean {
		return environment.isolatedChains.includes(chainSymbol);
	}

	private async mint(): Promise<void> {
		this.isMintingSuccess = false;
		this.isMintingError = false;
		this.mintingErrorReason = '';
		this.isApproveAssetError = false;
		this.isMintingCrossChain = false;
		this.isMintedCrossChain = false;
		this.mintedTokenFileURI = undefined;
		this.titleService.setTitle(`Omnisea - Minting ⌛`);
		const walletType: WalletTypeEnum = this.connectedWalletDetails?.type;

		/// Sui MINT:
		// if (walletType === WalletTypeEnum.SUI) {
		// 	this.uiLoaderService.startBackgroundLoader('mintingLoader');
		// 	const userAddress: string = this.connectedWalletDetails.address;
		// 	const biggestCoinBalance: number = this.suiCoinObject.biggestCoinBalance / (Math.pow(10, environment.suiSpecific.coins.SUI.decimals));
		//
		// 	if (Number(this.mintPriceInUnits) > 0 && biggestCoinBalance < (Number(this.mintPriceInUnits) * this.mintQuantity)) {
		// 		let coinMergeTxResponse: SuiTransactionResponse;
		//
		// 		if (this.connectedWalletDetails.provider === WalletProviderEnum.SUI_SUIET && this.suiCoinObject.biggestCoinBalance) {
		// 			try {
		// 				await this.suiSuietWalletService.mergeSuiCoins(this.suiCoinObject.coinObjectId);
		// 			} catch (e) {
		// 				console.error(e);
		// 				this.notificationsService.error('SUI Coins Split', 'Insufficient total SUI Coins balance. Please, merge.');
		// 			}
		// 		}
		//
		// 		if (this.connectedWalletDetails.provider === WalletProviderEnum.SUI_ETHOS) {
		// 			try {
		// 				await this.suiEthosWalletService.mergeSuiCoins(this.suiCoinObject.coinObjectId);
		// 			} catch (e) {
		// 				console.error(e);
		// 				this.notificationsService.error('SUI Coins Split', 'Insufficient total SUI Coins balance. Please, merge.');
		// 			}
		// 		}
		// 	}
		//
		// 	try {
		// 		this.isMinting = true;
		// 		let txResponse: SuiTransactionResponse;
		// 		let txHash: string;
		//
		// 		switch (this.connectedWalletDetails.provider) {
		// 			case WalletProviderEnum.SUI_ETHOS:
		// 				txResponse = await this.suiEthosWalletService.sendTransaction(
		// 					environment.suiSpecific.functions.mint,
		// 					[
		// 						environment.suiSpecific.objects.launchpadConfig,
		// 						environment.suiSpecific.objects.launchpadMetadata,
		// 						this.details.collectionName,
		// 						this.details.creator,
		// 						this.mintQuantity,
		// 						this.suiCoinObject.coinObjectId,
		// 					],
		// 				);
		//
		// 				txHash = txResponse?.certificate?.transactionDigest;
		// 				break;
		// 			case WalletProviderEnum.SUI_SUIET:
		// 				txResponse = await this.suiSuietWalletService.sendTransaction(
		// 					environment.suiSpecific.functions.mint,
		// 					[
		// 						environment.suiSpecific.objects.launchpadConfig,
		// 						environment.suiSpecific.objects.launchpadMetadata,
		// 						this.details.collectionName,
		// 						this.details.creator,
		// 						this.mintQuantity,
		// 						this.suiCoinObject.coinObjectId,
		// 					],
		// 				);
		//
		// 				txHash = txResponse?.certificate?.transactionDigest;
		// 				break;
		// 		}
		//
		// 		if (!txHash || txResponse?.effects?.status?.status !== 'success') {
		// 			this.notificationsService.error('Error', 'Status unconfirmed. Check if NFT was minted in your wallet');
		// 			throw new Error('Transaction has failed');
		// 		}
		// 		this.collectionsService.saveMintTx(
		// 			this.details,
		// 			userAddress,
		// 			txHash,
		// 			1,
		// 		);
		//
		// 		try {
		// 			this.collectionsService.incrementCollectionMintsCount(
		// 				this.details,
		// 				userAddress,
		// 				Number(this.mintPriceFormatted),
		// 				txHash,
		// 			);
		// 		} catch (e) {
		// 			console.error(e);
		// 		}
		// 		this.isMintingSuccess = true;
		// 		this.titleService.setTitle(`Omnisea - Minted 🥳`);
		// 		this.uiLoaderService.stopBackgroundLoader('mintingLoader');
		// 		// this.refreshDetailsAndMetadata();
		// 		this.notificationsService.success(
		// 			'Success',
		// 			'Minted NFT is already on your blockchain address',
		// 		);
		// 		this.isMinting = false;
		//
		// 		try {
		// 			const mintEvents = [];
		//
		// 			for (const event of txResponse.effects.events) {
		// 				if (!event.moveEvent || !event.moveEvent.fields.name) {
		// 					continue;
		// 				}
		// 				mintEvents.push(event);
		// 			}
		// 			if (!mintEvents?.length) {
		// 				return;
		// 			}
		// 			const lastMintEvent = mintEvents[mintEvents.length - 1];
		//
		// 			try {
		// 				this.setMintedSuiTokenFile(lastMintEvent.moveEvent.fields.name.slice(-1));
		// 			} catch (e) {
		// 				console.error(e);
		// 			}
		// 		} catch (e) {
		// 			console.error(e);
		// 		}
		// 	} catch (e) {
		// 		console.error(e);
		// 		this.notificationsService.error('Error', 'Mint transaction has failed');
		// 		this.mintingErrorReason = 'Minting process failed due to the transaction error';
		// 		this.isMintingError = true;
		// 		this.isMinting = false;
		// 		throw e;
		// 	}
		//
		// 	return;
		// }

		/// APTOS MINT:
		if (walletType === WalletTypeEnum.APTOS) {
			this.uiLoaderService.startBackgroundLoader('mintingLoader');
			const userAddress: string = this.connectedWalletDetails.address;

			try {
				this.isMinting = true;
				let txHash: string;
				let txResult: IAptosTransactionDetails<{ uri: string }>;

				switch (this.connectedWalletDetails.provider) {
					case WalletProviderEnum.APTOS_MARTIAN:
						txHash = await this.aptosMartianWalletService.sendTransaction(userAddress, {
							function: environment.tokenFactoryMap[this.details.tokenFactoryVersion][ChainSymbolEnum.aptos].replace(
								'{moduleName}',
								environment.aptosSpecific.versionToModuleName[this.details.tokenFactoryVersion],
							),
							type_arguments: [],
							arguments: [
								this.details.creator,
								this.details.collectionName,
								this.mintQuantity || 1,
							],
						});
						txResult = await this.aptosMartianWalletService.getTransaction(txHash);
						break;
					case WalletProviderEnum.APTOS_PONTEM:
						txHash = await this.aptosPontemWalletService.sendTransaction(userAddress, {
							function: environment.tokenFactoryMap[this.details.tokenFactoryVersion][ChainSymbolEnum.aptos].replace(
								'{moduleName}',
								environment.aptosSpecific.versionToModuleName[this.details.tokenFactoryVersion],
							),
							type_arguments: [],
							arguments: [
								this.details.creator,
								this.details.collectionName,
								this.mintQuantity || 1,
							],
						});
						txResult = await this.aptosPontemWalletService.getTransaction(txHash);
						break;
					case WalletProviderEnum.APTOS_PETRA:
						txResult = await this.aptosPetraWalletService.sendTransaction<[string, string, number], { uri: string }>({
							function: environment.tokenFactoryMap[this.details.tokenFactoryVersion][ChainSymbolEnum.aptos].replace(
								'{moduleName}',
								environment.aptosSpecific.versionToModuleName[this.details.tokenFactoryVersion],
							),
							type_arguments: [],
							arguments: [
								this.details.creator,
								this.details.collectionName,
								this.mintQuantity || 1,
							],
						});
						txHash = txResult?.hash;
						break;
				}

				if (!txResult.success) {
					this.notificationsService.error('Error', 'Status unconfirmed. Check if NFT was minted in your wallet');
					throw new Error('Transaction has failed');
				}

				this.collectionsService.saveMintTx(
					this.details,
					userAddress,
					txHash,
					txResult.success ? 1 : 0,
				);

				try {
					this.collectionsService.incrementCollectionMintsCount(
						this.details,
						userAddress,
						Number(this.mintPriceFormatted),
						txHash,
					);
				} catch (e) {
					console.error(e);
				}
				this.isMintingSuccess = true;
				this.titleService.setTitle(`Omnisea - Minted 🥳`);
				this.uiLoaderService.stopBackgroundLoader('mintingLoader');
				// this.refreshDetailsAndMetadata();
				this.notificationsService.success(
					'Success',
					'Minted NFT is already on your blockchain address',
				);
				this.isMinting = false;
				const lastMintEvent = txResult.events.find(
					(event) => event.type === AptosResourceTypeEnum.MINTED_EVENT,
				);

				if (!lastMintEvent) {
					return;
				}
				const lastTokenURI: string = lastMintEvent.data.uri;

				try {
					this.setMintedAptosTokenFile(lastTokenURI);
				} catch (e) {
					console.error(e);
				}
			} catch (e) {
				console.error(e);
				this.notificationsService.error('Error', 'Mint transaction has failed');
				this.mintingErrorReason = 'Minting process failed due to the transaction error';
				this.isMintingError = true;
				this.isMinting = false;
				throw e;
			}

			return;
		}

		/// EVM MINT
		if (!this.collectionAddress) {
			this.notificationsService.error('Error', 'Collection\'s address unknown');
			return;
		}
		let estimatedFees: IEstimatedFees;

		try {
			let web3Provider: Web3Provider;
			try {
				if (!this.web3Service.isWalletConnected()) {
					await this.web3Service.connectEVMWalletWithModal();
				}

				if (this.walletCheckOnMint) {
					clearInterval(this.walletCheckOnMint);
				}

				web3Provider = this.web3Service.evmProvider$.getValue();
			} catch (e) {
				console.error(e);
				this.walletCheckOnMint = setInterval(() => {
					this.mint();
				}, 15000);

				return;
			}
			const signer: JsonRpcSigner = web3Provider.getSigner();

			if (!this.tokenFactoryContractAddress) {
				this.tokenFactoryContractAddress =
					environment.tokenFactoryMap[this.details.tokenFactoryVersion || 1][this.connectedChainSymbol];
			}
			const contract = new ethers.Contract(
				this.tokenFactoryContractAddress,
				this.tokenFactoryContractAbi[this.details.tokenFactoryVersion || 1],
				signer,
			);

			this.uiLoaderService.startBackgroundLoader('mintingLoader');
			if (this.mintPriceInUnits > 0) {
				const assetAddress: string = environment.chainSymbolToAsset[this.details.assetName][this.connectedChainSymbol];
				const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, signer);
				const allowance: number = Number(ethers.utils.formatUnits(
					await this.getAllowance(assetContract, this.tokenFactoryContractAddress),
					environment.assetToDigits[this.details.assetName][this.connectedChainSymbol],
				));

				if (allowance < this.mintPriceInUnits) {
					try {
						this.isApprovingAsset = true;
						const approvalTx: ContractTransaction = await this.approveSpent(
							assetContract,
							this.tokenFactoryContractAddress,
						);

						await approvalTx.wait();
						this.isApprovingAsset = false;
					} catch (e) {
						this.isApprovingAsset = false;
						this.isApproveAssetError = true;
						throw e;
					}
				}
			}

			if (!environment.chainSymbolToAsset[this.details.assetName][this.collectionChainSymbol]) {
				this.notificationsService.error('Error', 'Invalid payment asset');
				throw new Error('Wrong asset');
			}

			try {
				this.isMinting = true;
				if (this.collectionChainSymbol === this.connectedChainSymbol) {
					this.mintTx = await contract.mintToken(
						[
							this.collectionChainSymbol,
							this.collectionAddress,
							this.mintPriceInUnits,
							this.details.assetName,
							this.mintQuantity || 1,
							this.details.creator,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							0,
						],
					);
				} else {
					// _getMintPayload(address collectionAddress, uint256 price, address creator, string memory assetName, uint256 quantity)
					const encodedFnData: string = this.web3Service.encodePayload(
						[
							1,
							this.collectionAddress,
							true,
							this.mintPriceInUnits,
							this.userAddress,
							this.details.creator,
							this.details.assetName,
							this.mintQuantity || 1,
						],
						['uint', 'address', 'bool', 'uint', 'address', 'address', 'string', 'uint256'],
					);
					const encodedPayload: string = this.web3Service.encodePayload(
						[
							this.collectionChainSymbol,
							this.userAddress || '0xMock',
							encodedFnData,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							this.userAddress || '0xMock',
							this.collectionChainSymbol,
							this.userAddress || '0xMock',
							0,
						],
						['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
					);

					estimatedFees = await this.omnichainService.getEstimatedFees(
						this.collectionChainSymbol,
						encodedPayload,
						OmnichainActionTypeEnum.MINT_TOKEN,
					);
					this.mintTx = await contract.mintToken(
						[
							this.collectionChainSymbol,
							this.collectionAddress,
							this.mintPriceInUnits,
							this.details.assetName,
							this.mintQuantity || 1,
							this.details.creator,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							estimatedFees.redirectFee,
						],
						{value: estimatedFees.fee},
					);
				}
				const userAddress: string = this.userAddress || await signer.getAddress();
				this.collectionsService.saveMintTx(
					this.details,
					userAddress,
					this.mintTx.hash,
				);

				try {
					this.collectionsService.incrementCollectionMintsCount(
						this.details,
						userAddress,
						Number(this.mintPriceFormatted),
						this.mintTx.hash,
					);
				} catch (e) {
					console.error(e);
				}

				this.mintTx.wait().then(async (txResult: ContractReceipt) => {
					this.collectionsService.saveMintTx(
						this.details,
						this.userAddress || await signer.getAddress(),
						this.mintTx.hash,
						txResult?.status,
					);
					this.isMintingCrossChain = this.connectedChainSymbol !== this.collectionChainSymbol;
					this.isMintingSuccess = true;
					this.titleService.setTitle(`Omnisea - Minted 🥳`);

					if (this.isConnectedToCollectionChain) {
						this.uiLoaderService.stopBackgroundLoader('mintingLoader');
					}
					// this.refreshDetailsAndMetadata();
					this.notificationsService.success(
						'Success',
						this.isMintingCrossChain ? 'Your NFT will be minted in a moment' : 'Minted NFT is already on your blockchain address',
					);
					this.listenToMintEvent();
				}).catch((e) => {
					this.mintingErrorReason = 'Minting process failed due to the transaction error';
					this.notificationsService.error('Error', this.mintingErrorReason);
					this.isMintingError = true;
					console.error(e);
					throw e;
				}).finally(() => {
					this.isMinting = false;
				});
			} catch (e) {
				console.error(e);
				this.isMintingError = true;
				throw e;
			}
		} catch (e) {
			this.uiLoaderService.stopBackgroundLoader('mintingLoader');
			this.isMinting = false;
			this.isApprovingAsset = false;
			const errorMessage: string = e?.message || e;
			const errorDataMessage: string = e?.data?.message;

			if (errorDataMessage) {
				if (errorDataMessage.includes('insufficient funds for gas') || errorDataMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.mintingErrorReason = tipMessage;
					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}
			}

			if (errorMessage) {
				if (errorMessage.includes('MetaMask Tx Signature: User denied transaction signature')) {
					this.mintingErrorReason = 'You didn\'t confirm the transaction.';
					// Canceled on User's action
					return;
				}

				if (errorMessage.includes('insufficient funds for gas') || errorMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;
					this.mintingErrorReason = tipMessage;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}

				if (errorMessage.includes('insufficient balance for transfer')) {
					if (!environment.isMainnet) {
						this.notificationsService.error('Insufficient funds', 'You\'ll be redirected to the faucet to get some test tokens.');
						setTimeout(() => {
							this.goToFaucet();
						}, 5000);
						return;
					}
					this.mintingErrorReason = 'Looks like you currently don\'t have required amount to mint';
					this.notificationsService.error('Insufficient funds', this.mintingErrorReason);

					return;
				}

				if (errorMessage.includes('invalid contract address or ENS name')) {
					console.error(e);
					this.mintingErrorReason = 'Network error. Refresh and try again in a moment.';
					this.notificationsService.error('Error', this.mintingErrorReason);

					return;
				}

				if (errorMessage.includes('underlying network changed')) {
					this.mintingErrorReason = 'Refreshing due to the connected network change during the process';
					this.notificationsService.error('Error', this.mintingErrorReason);
					setTimeout(() => {
						window.location.reload();
					}, 5000);

					return;
				}
			}
			console.error(e);
			this.mintingErrorReason = 'Unknown error. Please, refresh this page and try again';
			this.notificationsService.error('Error', this.mintingErrorReason);
		}
	}

	public goToCollectionOnExplorer(): void {
		this.web3Service.goToAddressOnExplorer(
			this.collectionAddress,
			this.collectionChainSymbol,
			this.getCollectionExplorerPageName(),
		);
	}

	public goToIpfsFileView(): void {
		if (!this?.details?.metadata?.media) {
			return;
		}

		window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.details.metadata.media.split('_')[0]}`, '_blank');
	}

	public goToIpfsMetadataView(): void {
		if (!this?.details?.metadataURI) {
			return;
		}

		window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.details.metadataURI}`, '_blank');
	}

	public goToSelectedNftIPFSFile(): void {
		if (this.nftSourceType === 'file' && this.selectedNFT?.fileIpfsHash) {
			window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.selectedNFT.fileIpfsHash.split('_')[0]}`, '_blank');
		}

		if (this.nftSourceType === 'ipfs' && this.selectedNFT?.fileURL) {
			window.open(this.selectedNFT?.fileURL.toString(), '_blank');
		}
	}

	public goToSelectedNftIPFSMetadata(): void {
		if (!this?.selectedNFT?.tokenId || !this.details?.tokensURI) {
			return;
		}

		window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.details.tokensURI}/${this.selectedNFT.tokenId}.json`, '_blank');
	}

	public shareOnTwitter(): void {
		window.open(`https://twitter.com/intent/tweet?text=${this.details.collectionName} drop 👇 &url=${encodeURIComponent(window.location.href)}`, '_blank');
	}

	public getCreatorAlias(): string {
		const creatorAddress: string = this.details.creator;

		if (this.profile?.name) {
			return this.profile.name;
		}

		return `${creatorAddress.substring(0, 5)}...${creatorAddress.slice(-3)}`;
	}

	public async goToCreator(): Promise<void> {
		this.router.navigate(['/user'], {queryParams: {userAddress: this.details.creator}});
	}

	public mapChainSymbolToName(chainSymbol: ChainSymbolEnum): SupportedChainNameEnum {
		switch (chainSymbol) {
			case ChainSymbolEnum.ftm:
				return SupportedChainNameEnum.Fantom;
			case ChainSymbolEnum.avax:
				return SupportedChainNameEnum.Avalanche;
			case ChainSymbolEnum.arb:
				return SupportedChainNameEnum.Arbitrum;
			case ChainSymbolEnum.bsc:
				return SupportedChainNameEnum.BSC;
			case ChainSymbolEnum.optimism:
				return SupportedChainNameEnum.Optimism;
			case ChainSymbolEnum.polygon:
				return SupportedChainNameEnum.Polygon;
			case ChainSymbolEnum.moonbeam:
				return SupportedChainNameEnum.Moonbeam;
			case ChainSymbolEnum.harmony:
				return SupportedChainNameEnum.Harmony;
			case ChainSymbolEnum.evmos:
				return SupportedChainNameEnum.Evmos;
			case ChainSymbolEnum.gate:
				return SupportedChainNameEnum.Gate;
			case ChainSymbolEnum.oasis:
				return SupportedChainNameEnum.Oasis;
			case ChainSymbolEnum.aptos:
				return SupportedChainNameEnum.Aptos;
			case ChainSymbolEnum.sui:
				return SupportedChainNameEnum.Sui;
			case ChainSymbolEnum.eth:
			default:
				return SupportedChainNameEnum.Ethereum;
		}
	}

	public getMintedFromChainCount(earnedFromChain: number): number {
		const mintPrice: number = Number(this.mintPriceFormatted);

		return earnedFromChain ? Math.ceil(earnedFromChain / mintPrice) : 0;
	}

	public getMintedFromCollectionChain(): number {
		if (this.totalMinted === 0) {
			return 0;
		}
		let omniMintedCount: number = 0;

		for (const chain of this.otherChains) {
			omniMintedCount += this.getMintedFromChainCount(this.earningsList[chain] || 0);
		}

		return this.totalMinted - omniMintedCount;
	}

	public canClaimRefunds(dstChainSymbol: ChainSymbolEnum): boolean {
		return this.isConnectedToCollectionChain && this.refundsList[dstChainSymbol] > 0 && !this.refundsClaimedToChainMap[dstChainSymbol];
	}

	public canClaimEarned(dstChainSymbol: ChainSymbolEnum): boolean {
		return this.isConnectedToCollectionChain && this.earningsList[dstChainSymbol] > 0 && !this.earningsClaimedToChainMap[dstChainSymbol];
	}

	public onClaimEarned(dstChainSymbol: ChainSymbolEnum) {
		if (!this.canClaimEarned(dstChainSymbol)) {
			return;
		}

		this.collectionsService.claimEarned(this.details, dstChainSymbol).then((result: boolean) => {
			if (result) {
				this.earningsClaimedToChainMap[dstChainSymbol] = true;
				this.notificationsService.success('Claimed', 'Omnichain Claim takes a few minutes to transfer your earnings.');

				return;
			}

			this.notificationsService.error('Error', 'Unable to claim your earnings. Network may be congested. Please, try again in a few minutes.');
		});
	}

	public onRefund(dstChainSymbol: ChainSymbolEnum) {
		if (!this.canClaimRefunds(dstChainSymbol)) {
			return;
		}

		this.collectionsService.refund(this.details, dstChainSymbol).then((result: boolean) => {
			if (result) {
				this.refundsClaimedToChainMap[dstChainSymbol] = true;
				this.notificationsService.success('Claimed', 'Omnichain Refund takes a few minutes to transfer your funds.');

				return;
			}

			this.notificationsService.error('Error', 'Unable to refund. Network may be congested. Please, try again in a few minutes.');
		});
	}

	public getEarnedAmountByMintCount(mintedOnCollectionChainCount: number) {
		if (!mintedOnCollectionChainCount) {
			return 0;
		}
		const mintPrice: number = Number(this.mintPriceFormatted);

		return mintedOnCollectionChainCount * mintPrice;
	}

	private async getAllowance(contract: Contract, spenderAddress: string): Promise<BigNumber> {
		return await contract.allowance(contract.signer.getAddress(), spenderAddress);
	}

	private async approveSpent(contract, spenderAddress: string): Promise<ContractTransaction> {
		return await contract.approve(spenderAddress, BigNumber.from(2).pow(256).sub(1));
	}

	private async getCollectionDetails(): Promise<void> {
		if (this.id) {
			try {
				this.collectionsService.getCollectionMintsCountFromDatabase(this.id, async (mintCount) => {
					// if (this.totalSupply > 0 && mintCount >= this.totalSupply) {
					// this.isRiskingSupplyExceeded = true;
					// }
					this.refreshSupplyInfo$.next();
				});
			} catch (e) {
				console.error(e);
			}

			this.collectionDataService.getCollectionDetails(this.id, async (details: ICollectionDetails) => {
				this.onCollectionLoaded(details);
			}, async () => {
				console.error(`Loading ${this.collectionAddress} collection from fallback`);
				this.notificationsService.error('Error', 'Could not load the collection data');
			});

			return;
		}
		this.collectionDataService.getCollectionBySlug(this.slug).then(async (details: ICollectionDetails) => {
			await this.onCollectionLoaded(details);

			try {
				this.collectionsService.getCollectionMintsCountFromDatabase(this.details.id, async (mintCount) => {
					// if (this.totalSupply > 0 && mintCount >= this.totalSupply) {
						// this.isRiskingSupplyExceeded = true;
					// }
					this.refreshSupplyInfo$.next();
				});
			} catch (e) {
				console.error(e);
			}
		}, async () => {
			this.notificationsService.error('Error', `Could not load the ${this.slug} collection`);
		});
	}

	public onQuantityChange(isAdding: boolean): void {
		if (isAdding && this.mintQuantity >= this.remainingCount) {
			return;
		}

		if (this.mintQuantity === 1 && !isAdding) {
			return;
		}
		this.mintQuantity = isAdding ? this.mintQuantity + 1 : this.mintQuantity - 1;
	}

	private refreshTheStartDate(): void {
		let secondsToStart: number = moment(this.details.dropFrom).diff(moment().unix());

		this.dateFrom = moment.duration(secondsToStart, 'seconds').humanize(true);
		setInterval(() => {
			if (!this.details?.dropFrom) {
				return;
			}
			secondsToStart = moment(this.details.dropFrom).diff(moment().unix());

			this.dateFrom = moment.duration(secondsToStart, 'seconds').humanize(true);
		}, 5000);
	}

	private async refreshDetails(): Promise<void> {
		if (!this.imageSrc) {
			this.imageSrc = await this.firebaseStorageService.getIpfsFileURL(this.details.fileURI);
		}
		this.mintPriceInUnits = this.details.mintPrice;
		this.mintPriceFormatted = this.mintPriceInUnits ? this.mintPriceInUnits.toString() : '0';

		if (!this.totalSupply) {
			this.totalSupply = this.details.totalSupply;
		}
		if (!this.totalMinted) {
			this.totalMinted = this.details.totalMinted;
		}

		if (!this.isConnectedToIsolatedChain) {
			this.getRefundsList();
			this.getEarningList();
		}

		if (!this.details.dropFrom) {
			return;
		}
		this.dateFromAsUnix = this.details.dropFrom;
		this.dateToAsUnix = this.details.dropTo;
		this.refreshTheStartDate();

		if (this.details.dropTo) {
			const secondsToEnd: number = moment(this.details.dropTo).diff(moment().unix());

			this.dateTo = moment.duration(secondsToEnd, 'seconds').humanize(true);

			return;
		}
	}

	private async refreshDetailsAndMetadata(): Promise<void> {
		try {
			await this.refreshDetails();
			await this.refreshMetadataAndUpdateInfo();
		} catch (e) {
			console.error(e);
		}
	}

	private goToFaucet(): void {
		const queryParams: Params = {
			collectionAddress: this.collectionAddress,
			collectionChainSymbol: this.collectionChainSymbol,
		};
		this.router.navigate(['/testnet'], {queryParams});
	}

	private async setConnectedChainData(): Promise<void> {
		try {
			if (this.connectedWalletDetails.type === WalletTypeEnum.EVM) {
				this.tokenFactoryContractAddress =
					environment.tokenFactoryMap[this.details.tokenFactoryVersion || 1][this.connectedChainSymbol];
				this.assetBalance = await this.web3Service.getBalance(
					environment.chainSymbolToAsset[this.details.assetName][this.connectedChainSymbol],
				);
			}

			if (this.connectedWalletDetails.type === WalletTypeEnum.APTOS) {
				this.assetBalance = this.connectedWalletDetails.provider === WalletProviderEnum.APTOS_MARTIAN
					? await this.aptosMartianWalletService.getBalance(this.connectedWalletDetails.address)
					: await this.aptosPetraWalletService.getBalance(this.connectedWalletDetails.address);
			}

			// if (this.connectedWalletDetails.type === WalletTypeEnum.SUI) {
			// 	const suiCoinBalanceObj = await this.web3Service.getSuiCoinBalanceAndObjectId(this.userAddress);
			//
			// 	this.assetBalance = BigNumber.from(Math.trunc(suiCoinBalanceObj.balance));
			// 	this.suiCoinObject = suiCoinBalanceObj;
			// }
		} catch (e) {
			console.error(e);
			this.isAssetBalanceError = true;
		}
	}

	private async isAllowlisted(): Promise<boolean> {
		if ([ChainSymbolEnum.aptos, ChainSymbolEnum.sui].includes(this.collectionChainSymbol)) {
			return this.allowlist?.addresses.includes(this.userAddress) || false;
		}

		return await this.collectionsService.isAllowlistedInEVMCollection(
			this.details.collectionAddress,
			this.details.chainSymbol,
		);
	}

	private getCollectionDataFromChain(): void {
		setTimeout(() => {
			if (!this.mintedCountLoaded) {
				this.mintedCountLoaded = true;
				this.isUserAllowlisted = true;
			}
		}, 30000);

		// Aptos:
		if (this.collectionChainSymbol === ChainSymbolEnum.aptos) {
			this.nftsService.getAptosCollectionCreator(this.details.creator, this.details.tokenFactoryVersion).then((creator) => {
				this.nftsService.getAptosCollectionSupplyInfo(creator, this.details.collectionName).then((supplyInfo) => {
					this.totalMinted = supplyInfo.totalSupply;
					this.remainingCount = supplyInfo.remaining;
					this.mintedCountLoaded = true;
				}).catch((e) => {
					this.mintedCountLoaded = true;
					console.error(e);
				});
			}).catch((e) => {
				this.mintedCountLoaded = true;
				console.error(e);
			});

			if (this.allowlist) {
				return;
			}
			this.collectionsService.getAptosAllowlist(
				this.details.creator,
				this.details.collectionName,
				this.details.tokenFactoryVersion,
			).then((allowlist) => {
				this.allowlist = allowlist;

				console.log(this.allowlist);

				if (this.allowlist?.isEnabled && this.isConnectedToWallet) {
					this.isAllowlisted().then((isAllowlisted) => {
						this.isUserAllowlisted = isAllowlisted;
					}).catch((e) => {
						this.isUserAllowlisted = true;
						console.error(e);
					});
				}
			}).catch((e) => {
				this.isUserAllowlisted = true;
				console.error(e);
			});

			return;
		}

		// Sui:
		// if (this.collectionChainSymbol === ChainSymbolEnum.sui) {
		// 	this.nftsService.getSuiCollectionData(this.details.creator, this.details.collectionName).then((suiCollectionData) => {
		// 		this.totalMinted = suiCollectionData.totalSupply;
		// 		this.remainingCount = suiCollectionData.maxSupply - this.totalMinted;
		// 		this.mintedCountLoaded = true;
		// 		this.allowlist = suiCollectionData.allowlist;
		// 	}).catch((e) => {
		// 		this.mintedCountLoaded = true;
		// 		console.error(e);
		// 	});
		//
		// 	return;
		// }

		// EVM:
		this.collectionsService.getEVMCollectionMintedCount(
			this.collectionAddress,
			this.collectionChainSymbol,
			this.details.tokenFactoryVersion,
		).then((mintedCount: number) => {
			this.totalMinted = mintedCount;
			this.mintedCountLoaded = true;

			if (!this.totalSupply) {
				return;
			}
			this.remainingCount = this.totalSupply - this.totalMinted;
		}).catch((e) => {
			this.mintedCountLoaded = true;
			console.error(e);
		});

		if (this.allowlist) {
			return;
		}
		this.collectionsService.getEVMAllowlist(this.details.collectionAddress, this.collectionChainSymbol).then((allowlist) => {
			this.allowlist = allowlist;

			if (this.allowlist?.isEnabled && this.isConnectedToWallet) {
				this.isAllowlisted().then((isAllowlisted) => {
					this.isUserAllowlisted = isAllowlisted;
				});
			}
		}).catch((e) => {
			console.error(e);
		});
	}

	private getEarningList(): void {
		if (this.earningsList || !this.mintPriceInUnits) {
			return;
		}

		this.collectionsService.getEarningsList(this.details, Number(this.mintPriceFormatted)).then((earningsList) => {
			this.earningsList = earningsList;
			this.mintedOnCollectionChainCount = this.getMintedFromCollectionChain();
			this.isEarning = this.getIsEarning();
		});
	}

	private getRefundsList(): void {
		if (this.refundsList || !this.userAddress || !this.mintPriceInUnits) {
			return;
		}

		this.collectionsService.getRefundsList(this.details, this.userAddress).then((refundsList) => {
			this.refundsList = refundsList;
			this.isRefundable = this.getIsRefundable();
		});
	}

	private getIsRefundable(): boolean {
		if (!this.refundsList) {
			return false;
		}

		for (const chainSymbol in this.refundsList) {
			if (!this.refundsList[chainSymbol]) {
				continue;
			}

			if (this.refundsList[chainSymbol] > 0) {
				return true;
			}
		}

		return false;
	}

	private getIsEarning(): boolean {
		if (!this.earningsList) {
			return false;
		}

		for (const chainSymbol in this.earningsList) {
			if (!this.earningsList[chainSymbol]) {
				continue;
			}

			if (this.earningsList[chainSymbol] > 0) {
				return true;
			}
		}

		return false;
	}

	private canMint(): boolean {
		return this.isLoaded && this.isOngoing && this.details.isPublic && this.mintedCountLoaded && this.isAllowedToMint;
	}

	private async listenToMintEvent(): Promise<void> {
		try {
			const erc721Contract = new ethers.Contract(
				this.collectionAddress,
				this.erc721Abi,
				this.web3Service.getEVMNetworkProvider(this.collectionChainSymbol),
			);
			const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();
			const connectedUserAddress: string = await signer.getAddress();

			erc721Contract.on('TokenMinted', async (from: string, ownerAddress: string, tokenId: BigNumber) => {
				if (ownerAddress === connectedUserAddress) {
					try {
						this.setMintedEVMTokenFile(tokenId.toNumber());
					} catch (e) {
						console.error(e);
					}

					this.uiLoaderService.stopBackgroundLoader('mintingLoader');

					if (this.isMintingCrossChain) {
						this.notificationsService.success(
							'Success',
							`NFT was minted on ${this.mapChainSymbolToName(this.collectionChainSymbol)} and belongs to you.`,
						);
						this.isMintedCrossChain = true;
					}
				}
			});
		} catch (e) {
			console.error(e);
		}
	}

	private showNFTs() {
		this.selectedTab = 'nfts';

		setTimeout(() => {
			if (this.swiper || !this.nfts.length) {
				return;
			}
			const hasManyNFTs: boolean = this.nfts.length > 4;

			this.selectedNFT = this.nfts[0];
			this.swiper = new Swiper('#nftSwiper', {
				modules: [FreeMode, Autoplay],
				autoplay: hasManyNFTs ? {
					delay: 2500,
					disableOnInteraction: false,
					pauseOnMouseEnter: true,
				} : false,
				speed: 400,
				loop: false,
				rewind: hasManyNFTs,
				spaceBetween: 12,
				centeredSlides: false,
				slidesPerView: hasManyNFTs ? 4 : this.nfts.length,
				breakpoints: {
					560: {
						slidesPerView: hasManyNFTs ? 4 : this.nfts.length,
					},
					768: {
						slidesPerView: hasManyNFTs ? 4 : this.nfts.length,
					},
					1024: {
						slidesPerView: hasManyNFTs ? 4 : this.nfts.length,
					},
				},
				preloadImages: false,
				lazy: hasManyNFTs,
			});
		});
	}

	private async setMintedEVMTokenFile(tokenId: number): Promise<void> {
		this.mintedTokenFileURI = await this.nftsService.getEVMTokenFileURI(
			this.collectionAddress,
			tokenId,
			this.collectionChainSymbol,
		);
	}

	private async setMintedAptosTokenFile(tokenURI: string): Promise<void> {
		this.mintedTokenFileURI = await this.nftsService.getAptosTokenFileURI(tokenURI);
	}

	private async setMintedSuiTokenFile(tokenId: number): Promise<void> {
		this.mintedTokenFileURI = await this.nftsService.getSuiTokenFileURI(this.details.tokensURI, tokenId);
	}

	private getCollectionExplorerPageName(): string {
		switch (this.collectionChainSymbol) {
			case ChainSymbolEnum.arb:
				return 'token';
			case ChainSymbolEnum.aptos:
				return 'account';
			case ChainSymbolEnum.sui:
				return 'addresses';
			default:
				return 'address';
		}
	}

	private async onCollectionLoaded(details: ICollectionDetails): Promise<void> {
		this.details = details;
		this.collectionChainSymbol = this.details.chainSymbol;
		this.collectionAddress = this.details.collectionAddress;
		this.collectionChainName = this.mapChainSymbolToName(this.collectionChainSymbol);
		this.otherChains = environment.supportedChains.filter((chain) => chain !== this.collectionChainSymbol);
		this.contentType = this.details.contentType || ContentTypeEnum.ART;
		this.nftSourceType = this.details.nftSourceType || 'file';
		this.getCollectionDataFromChain();

		try {
			this.profileService.getProfile(this.details.creator).then(async (profile: IProfile) => {
				this.profile = profile;
			});
		} catch (e) {
			console.error(e);
		}

		if (this.details.srcChain) {
			this.srcChainName = this.mapChainSymbolToName(this.details.srcChain);
		}

		if (this.nftSourceType === 'ipfs' && this.details.totalSupply > 0) {
			if (this.details.tokensURI?.length !== 46 && this.details.tokensURI?.length !== 59) {
				this.notificationsService.error('Invalid IPFS CID');
				throw new Error('Invalid IPFS CID length');
			}
			const nftsToPreview: number = this.details.totalSupply > 10 ? 10 : this.details.totalSupply;
			let isShowingNFTs: boolean;

			for (let i = 0; i < nftsToPreview; i++) {
				this.getNFTMetadataByCIDAndTokenId(
					this.details.tokensURI,
					this.details.isCustomFirstTokenId ? (i + 1) : i,
				).then((nftMetadata: INFTMetadata) => {
					this.nfts.push({
						tokenId: this.details.isCustomFirstTokenId ? (i + 1) : i,
						fileURL: nftMetadata?.image
							? `https://omnisea.infura-ipfs.io/ipfs/${nftMetadata?.image.replace('ipfs://', '')}`
							: '',
					});

					if (!isShowingNFTs) {
						isShowingNFTs = true;
						this.showNFTs();
					}
				});
			}
		} else {
			this.nftsService.getByCollectionId(
				this.details.id,
				async (nfts: Record<string, INFTDetails>) => {
					if (!nfts) {
						return;
					}

					this.nfts = Object.values(nfts);
					for (const nft of this.nfts) {
						this.firebaseStorageService.getIpfsFileURL(nft.smFilePath || nft.smFileIpfsHash).then((url) => {
							nft.fileURL = url;
						});
					}

					if (this.nfts.length) {
						this.showNFTs();
					}
				},
				async () => {
					console.error(`Couldn\'t load NFTs of collection: ${this.collectionAddress}`);
				}
			);
		}

		this.refreshDetailsAndMetadata();
		this.isLoaded = true;
		this.titleService.setTitle(`Omnisea - ${this.details.collectionName}`);
	}

	private async getNFTMetadataByCIDAndTokenId(ipfsCID: string, tokenId: number): Promise<INFTMetadata> {
		const response = await fetch(`https://omnisea.infura-ipfs.io/ipfs/${ipfsCID}/${tokenId}.json`);

		return await response.json();
	}
}
