import { Injectable } from '@angular/core';
import { INFTDetails } from '../../web3/interfaces/nft-details.interface';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { Contract } from '@ethersproject/contracts';
import { BigNumber, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import { collection as colRef, Firestore, getDocs, query, where } from '@firebase/firestore';
import FirestoreService from '../../firebase/services/firestore.service';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';
import { INFTMetadata } from '../interfaces/nft-metadata.interface';
import { environment } from '../../../../environments/environment';
import { AptosClient, TokenClient } from 'aptos';
// import SuiNetworkService from '../../sui/services/sui-network.service';
// import { ISuiCollectionData } from '../../sui/interfaces/sui-collection-data.interface';

@Injectable()
export default class NftsService {
	public readonly omniNftAbi: string[] = [
		'function createdAt() external view returns (uint256)',
		'function creator() external view returns (address)',
		'function getMintedBy(address addr) public view returns (uint256[] memory)',
		'function collectionName() external view returns (string memory)',
		'function hidden() external view returns (bool)',
		'function dropFrom() external view returns (uint256)',
		'function dropTo() external view returns (uint256)',
		'function collectionURI() external view returns (string memory)',
		'function mintPrice() external view returns (uint256)',
		'function totalSupply() external view returns (uint256)',
		'function totalMinted() external view returns (uint256)',
		'function tokenURI(uint256 tokenId) external view returns (string memory)'
	];

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly firestoreService: FirestoreService,
		// private readonly suiNetworkService: SuiNetworkService,
	) {
	}

	public async getByCollectionId(
		collectionId: number,
		onSuccess: (result: Record<string, INFTDetails>) => Promise<void>,
		onError: () => Promise<void>,
	): Promise<void> {
		await this.firebaseDatabaseService.get<Record<string, INFTDetails>>(
			`/tokens/${collectionId}`,
			{},
			onSuccess,
			onError,
		);
	}

	public async setDataFromDatabaseByTokenId(
		collectionId: number,
		nftRef: INFTDetails,
	): Promise<void> {
		await this.firebaseDatabaseService.getById<INFTDetails>(
			`/tokens/${collectionId}/${nftRef.tokenId}`,
			async (nft: INFTDetails) => {
				if (!nft) {
					console.error(`Not found tokenId: ${nftRef.tokenId} of collection with ID: ${collectionId}`);

					return;
				}

				nftRef.smFilePath = nft.smFilePath;
				nftRef.fileIpfsHash = nft.fileIpfsHash;
				nftRef.attributes = nft.attributes;

				this.firebaseStorageService.getIpfsFileURL(nftRef.smFilePath).then((url) => {
					nftRef.fileURL = url;
				});

				this.firebaseStorageService.getFileMetadata(nftRef.smFilePath).then((fileMetadata) => {
					nftRef.contentType = fileMetadata?.contentType.includes('audio') ? ContentTypeEnum.MUSIC : ContentTypeEnum.ART;
				});
			},
			async () => {
				console.error(`Unable to get NFT tokenId: ${nftRef.tokenId} of collection with ID: ${collectionId}`);
			}
		);
	}

	public async getMintedByUser(
		collectionAddress: string,
		collectionId: number,
		chainSymbol: ChainSymbolEnum,
		userAddress: string,
	): Promise<INFTDetails[]> {
		try {
			const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const mintedBy: BigNumber[] = await contract.getMintedBy(userAddress);
			const tokensIds: number[] = mintedBy?.length ? mintedBy.map((tokenId) => tokenId.toNumber()) : [];
			const nfts: INFTDetails[] = [];
			const metadataURI: string = await contract.collectionURI();
			const collectionName: string = await contract.collectionName();
			const totalSupply: number = (await contract.totalSupply()).toNumber();

			if (!tokensIds) {
				return;
			}

			for (const tokenId of tokensIds) {
				const nft: INFTDetails = {
					tokenId,
					collectionAddress,
					collectionName,
					chainSymbol,
					collectionId,
					collectionMetadataURI: metadataURI,
				};

				if (totalSupply > 0) {
					this.setDataFromDatabaseByTokenId(collectionId, nft);
				} else {
					// TODO: Set template image for NFTs of collections with unlimited supply
				}

				nfts.push(nft);
			}

			return nfts;
		} catch (e) {
			console.error(e);
		}
	}

	public async getUserMintsData(userAddress: string): Promise<IMintData[]> {
		if (!userAddress) {
			return;
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getUserMintsData()');
		}
		const qb = query(
			colRef(db, 'mints'),
			where('userAddress', '==', userAddress),
		);
		// @ts-ignore
		const querySnapshot = await getDocs<IMintData>(qb);
		const mintData: IMintData[] = [];

		querySnapshot.forEach((doc) => {
			mintData.push(doc.data());
		});

		return mintData;
	}

	// TODO: (Minor) Move to a new refundsService
	public async getUserRefundsData(userAddress: string): Promise<IClaimRefundTx[]> {
		if (!userAddress) {
			return;
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getUserRefundsData()');
		}
		const qb = query(
			colRef(db, `transactions/${userAddress}/refund`),
		);
		// @ts-ignore
		const querySnapshot = await getDocs<IClaimRefundTx>(qb);
		const data: IClaimRefundTx[] = [];

		querySnapshot.forEach((doc) => {
			data.push(doc.data());
		});

		return data;
	}

	// TODO: (Minor) Move to a new earningsService
	public async getUserClaimEarnedData(userAddress: string): Promise<IClaimEarnedTx[]> {
		if (!userAddress) {
			return;
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getUserClaimEarnedData()');
		}
		const qb = query(
			colRef(db, `transactions/${userAddress}/claimEarned`),
		);
		// @ts-ignore
		const querySnapshot = await getDocs<IClaimEarnedTx>(qb);
		const data: IClaimEarnedTx[] = [];

		querySnapshot.forEach((doc) => {
			data.push(doc.data());
		});

		return data;
	}

	public async getEVMTokenFileURI(collectionAddress: string, tokenId: number, chainSymbol: ChainSymbolEnum): Promise<string> {
		try {
			const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const tokenURI: string = await contract.tokenURI(tokenId);

			if (!tokenURI) {
				return;
			}
			const ipfsGatewayURI: string = tokenURI.replace('ipfs://', 'https://omnisea.infura-ipfs.io/ipfs/');
			const response = await fetch(ipfsGatewayURI);
			const metadata: INFTMetadata = await response.json();

			if (!metadata || !metadata.image) {
				return;
			}

			return metadata.image;
		} catch (e) {
			console.error(e);
			return;
		}
	}

	public async getSuiTokenFileURI(tokensURI: string, tokenId: number): Promise<string> {
		const nftMetadataResponse = await fetch(`https://omnisea.infura-ipfs.io/ipfs/${tokensURI}/${tokenId}.json`);
		const nftMetadata: INFTMetadata = await nftMetadataResponse.json();

		if (!nftMetadata?.image) {
			return;
		}

		return nftMetadata.image.replace('ipfs://', 'https://omnisea.infura-ipfs.io/ipfs/');
	}


	public async getAptosTokenFileURI(tokenURI: string): Promise<string> {
		const ipfsGatewayURI: string = tokenURI.replace('ipfs://', 'https://omnisea.infura-ipfs.io/ipfs/');
		const response = await fetch(ipfsGatewayURI);
		const metadata: INFTMetadata = await response.json();

		if (!metadata || !metadata.image) {
			return;
		}

		return metadata.image;
	}

	public async getAptosCollectionCreator(account: string, moduleVersion: number): Promise<string> {
		const client = new AptosClient(environment.aptosSpecific.api.uri);
		const accountResources = await client.getAccountResources(account);
		const collectionTokenMinter = accountResources.find(
			(resource) => resource.type === environment.aptosSpecific.resources.collectionTokenMinter.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
		);

		// @ts-ignore
		return collectionTokenMinter.data.signer_addr;
	}

	public async getAptosCollectionSupplyInfo(creator: string, collectionName: string): Promise<{
		maxSupply: number,
		totalSupply: number,
		remaining: number,
	}> {
		const client = new AptosClient(environment.aptosSpecific.api.uri);
		const tokenClient = new TokenClient(client);
		const collectionData = await tokenClient.getCollectionData(creator, collectionName);
		const maxSupply: number = Number(collectionData.maximum);
		const totalSupply: number = Number(collectionData.supply);

		if (!maxSupply) {
			return {
				maxSupply,
				totalSupply,
				remaining: Infinity,
			};
		}

		return {
			maxSupply,
			totalSupply,
			remaining: maxSupply - totalSupply,
		};
	}

	// public async getSuiCollectionData(creator: string, collectionName: string): Promise<ISuiCollectionData> {
	// 	return await this.suiNetworkService.getCollectionMintedCount(creator, collectionName);
	// }
}
