import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import NftsService from './services/nfts.service';

@NgModule({
	providers: [
		NftsService,
	],
	imports: [
		SharedModule,
	],
})
export class NftsModule {
}
