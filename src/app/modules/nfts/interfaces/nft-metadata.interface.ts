export interface INFTMetadata {
	name: string;
	description?: string;
	image: string;
	external_link: string;
	fileURI: string;
	animation_url?: string;
	attributes: Record<string, string | boolean | number>;
}


