import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { CollectionIpfsMetadata } from '../dto/collection-metadata.dto';

export interface ICollectionEditDto {
	id: keyof ICollectionDetails | keyof CollectionIpfsMetadata;
	type: 'text' | 'number' | 'date' | 'file' | 'url' | 'textarea';
	label: string;
	isRequired: boolean;
}
