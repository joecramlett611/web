export enum MetadataDisplayTypeEnum {
	ANY = '',
	DATE = 'date',
	NUMBER = 'number',
	BOOST_PERCENTAGE = 'boost_percentage',
	BOOST_NUMBER = 'boost_number',
}
