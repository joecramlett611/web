import { NgModule } from '@angular/core';
import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './components/create/create.component';
import { SharedModule } from '../shared/shared.module';
import { CollectionPreviewComponent } from './components/collection-preview/collection-preview.component';
import { IpfsModule } from '../ipfs/ipfs.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';

@NgModule({
	declarations: [
		CreateComponent,
		CollectionPreviewComponent,
	],
	imports: [
		CreateRoutingModule,
		SharedModule,
		IpfsModule,
		FormsModule,
		ReactiveFormsModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
	],
	exports: [
		CreateComponent,
		CollectionPreviewComponent,
	],
})
export class CreateModule {
}
