import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import * as moment from 'moment';
import { ICollectionEditDto } from '../../interfaces/collection-edit-dto.interface';
import Web3Service from '../../../web3/services/web3.service';
import { BehaviorSubject, debounceTime, Subscription } from 'rxjs';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { environment } from '../../../../../environments/environment';
import { FormGroup } from '@angular/forms';
import { CollectionIpfsMetadata } from '../../dto/collection-metadata.dto';
import { LicenceTypeEnum } from '../../../collections/enums/licence-type.enum';
import IpfsService from '../../../ipfs/services/ipfs.service';
import NotificationsService from '../../../shared/services/notifications.service';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';
import { IProfile } from '../../../user/interfaces/profile.interface';
import ProfileService from '../../../user/services/profile.service';

@Component({
	selector: 'app-collection-preview',
	templateUrl: './collection-preview.component.html',
	styleUrls: ['./collection-preview.component.scss']
})
export class CollectionPreviewComponent implements OnInit {
	public get isValid(): boolean {
		return this.collectionForm ? this.collectionForm.valid : false;
	}

	public get isCollectionRequiredDataFilled(): boolean {
		return !!this.collection.collectionName && !!this.squareImage && !!this.backgroundImage;
	}

	public get isCollectionOptionalDataFilled(): boolean {
		return this.hints.optional.findIndex((hint) => !hint.completed) === -1;
	}

	public get isCollectionAdvancedOptionsFilled(): boolean {
		return this.collection.licence !== LicenceTypeEnum.CREATOR
			|| !this.collection.isPublic
			|| this.collection.srcChain !== this.collection.chainSymbol
			|| this.collection.royalties > 0;
	}

	public get isNFTsHintActive(): boolean {
		return this.isCollectionRequiredDataFilled && !this.nfts.length;
	}

	public get isAdvancedOptionsHintActive(): boolean {
		return this.isCollectionRequiredDataFilled
			&& this.nfts.length
			&& this.isCollectionOptionalDataFilled
			&& !this.isCollectionAdvancedOptionsFilled;
	}

	public get completedRequiredHints(): number {
		return this.hints?.required.filter((hint) => hint.completed).length;
	}

	public get mintPriceTruncated(): string {
		if (this.collection.chainSymbol === ChainSymbolEnum.sui) {
			return this.collection.mintPrice > 0 ? this.collection.mintPrice.toString() : 'Free';
		}

		return this.collection.mintPrice >= 1 ? Math.trunc(this.collection.mintPrice).toString() : 'Free';
	}

	public get currentRequiredHint(): {
		id: keyof ICollectionDetails | keyof CollectionIpfsMetadata,
		text: string,
		completed: boolean,
		icon: string,
		editData: ICollectionEditDto,
	} {
		for (const hint of this.hints.required) {
			if (!hint.completed) {
				return hint;
			}
		}

		return this.hints.required[this.hints.required.length - 1];
	}

	public get currentOptionalHint(): {
		id: keyof ICollectionDetails | keyof CollectionIpfsMetadata,
		text: string,
		completed: boolean,
		icon: string,
		editData: ICollectionEditDto,
	} {
		if (!this.isCollectionRequiredDataFilled || !this.nfts.length) {
			return;
		}

		for (const hint of this.hints.optional) {
			if (!hint.completed) {
				return hint;
			}
		}
	}

	public get nftsFromIPFSEditData(): ICollectionEditDto {
		return {
			id: 'tokensURI',
			type: 'text',
			label: 'Add NFTs from IPFS',
			isRequired: false,
		};
	}

	@Input() public collection: ICollectionDetails;
	@Input() public nfts: INFTDetails[];
	@Input() public onBackgroundImageChange$: BehaviorSubject<File>;
	@Input() public onSquareImageChange$: BehaviorSubject<File>;
	@Input() public onUpdated$: BehaviorSubject<void>;
	@Input() public collectionForm: FormGroup;
	@Output() public propertyEditStart = new EventEmitter();
	@Output() public advancedOptionsEditStart = new EventEmitter();
	@Output() public addNFTsStart = new EventEmitter();
	@Output() public removeNFT = new EventEmitter<number>();
	@Output() public nftEditStart = new EventEmitter<number>();
	@Output() public launchStart = new EventEmitter();
	public backgroundImageLoaded: boolean;
	public isImportedMetadataValid: boolean;
	public selectedTab: string;
	public dateFrom: string;
	public dateTo: string;
	public nftSourceType: 'file' | 'ipfs';
	public hints: {
		required: Array<{
			id: keyof ICollectionDetails | keyof CollectionIpfsMetadata,
			text: string,
			completed: boolean,
			icon: string,
			editData: ICollectionEditDto,
		}>;
		optional: Array<{
			id: keyof ICollectionDetails | keyof CollectionIpfsMetadata,
			text: string,
			completed: boolean,
			icon: string,
			editData: ICollectionEditDto,
		}>;
	};
	public userAddress: string;
	public isConnectedToWallet: boolean;
	public connectedChainSymbol: ChainSymbolEnum;
	public backgroundImagePreviewSrc: string | ArrayBuffer;
	public squareImagePreviewSrc: string | ArrayBuffer;
	public isCustomFirstTokenId: boolean;
	private subscriptions: Subscription;
	private backgroundImage: File;
	private squareImage: File;
	private totalSupply: number;
	private tokensURI: string;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly ipfsService: IpfsService,
		private readonly profileService: ProfileService,
		private readonly notificationsService: NotificationsService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.selectedTab = 'nfts';

		this.subscriptions.add(
			this.onBackgroundImageChange$.subscribe((file) => {
				this.backgroundImage = file;
				const reader = new FileReader();

				reader.onload = async (e) => {
					this.backgroundImagePreviewSrc = reader.result;
				};
				reader.readAsDataURL(file);
			}),
		);

		this.subscriptions.add(this.onSquareImageChange$.subscribe((file) => {
			this.squareImage = file;
			const reader = new FileReader();

			reader.onload = async (e) => {
				this.squareImagePreviewSrc = reader.result;
			};
			reader.readAsDataURL(file);
		}));

		this.subscriptions.add(
			this.onUpdated$
				.pipe(
					debounceTime(250),
				)
				.subscribe(async () => {
					this.updateHints();

					if (
						this.isImportedIPFSChanged()
						&& this.collection.nftSourceType === 'ipfs'
						&& (this.collection.tokensURI?.length === 46 || this.collection.tokensURI?.length === 59)
						&& this.collection.totalSupply > 0
					) {
						const firstTokenId: number = this.collection.isCustomFirstTokenId ? 1 : 0;
						const lastTokenId: number = firstTokenId === 0 ? this.collection.totalSupply - 1 : this.collection.totalSupply;

						this.nfts.length = 0;
						this.ipfsService.getNFTMetadataByCIDAndTokenId(
							this.collection.tokensURI,
							firstTokenId,
						).then(async (firstTokenMetadata: INFTMetadata) => {
							if (firstTokenMetadata?.image) {
								try {
									await this.ipfsService.getNFTMetadataByCIDAndTokenId(
										this.collection.tokensURI,
										lastTokenId,
									);

									this.isImportedMetadataValid = true;
									const nftsToPreview: number = this.collection.totalSupply > 10 ? 9 : this.collection.totalSupply;

									for (let i = 0; i < nftsToPreview; i++) {
										this.ipfsService.getNFTMetadataByCIDAndTokenId(
											this.collection.tokensURI,
											i + 1,
										).then((nftMetadata: INFTMetadata) => {
											this.nfts.push({
												tokenId: i + 1,
												fileURL: nftMetadata?.image?.length > 0
													? `https://omnisea.infura-ipfs.io/ipfs/${nftMetadata?.image.replace('ipfs://', '')}`
													: '',
												name: nftMetadata?.name || '',
											});
										});
									}

									return;
								} catch (e) {
									this.isImportedMetadataValid = false;
									this.notificationsService.error('Invalid Metadata URL', `Couldn't load metadata of last token at:
							            ${this.collection.tokensURI}/${lastTokenId}.json`);
								}
							}
							this.isImportedMetadataValid = false;
							this.notificationsService.error('Invalid Metadata URL', `Couldn't load metadata of first token at:
							 ${this.collection.tokensURI}/${firstTokenId}.json`);
						}).catch((e) => {
							this.isImportedMetadataValid = false;
							console.error(e);
							this.notificationsService.error('Invalid Metadata URL', `Couldn't load metadata of first token at:
							 ${this.collection.tokensURI}/${firstTokenId}.json`);
						});
					}
					this.nftSourceType = this.collection.nftSourceType;
					this.totalSupply = this.collection.totalSupply;
					this.tokensURI = this.collection.tokensURI;
					this.isCustomFirstTokenId = this.collection.isCustomFirstTokenId;
				}),
		);

		this.subscriptions.add(
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isWalletConnected();

					if (this.isConnectedToWallet) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

						this.connectedChainSymbol = this.isSupportedNetwork(connectedChainSymbol) ? connectedChainSymbol : ChainSymbolEnum.eth;

						if (!this.connectedChainSymbol) {
							await this.connectWallet();
						}

						try {
							if (this.collection?.creatorProfile) {
								return;
							}

							this.profileService.getProfile(await this.web3Service.getUserAddress()).then(async (profile: IProfile) => {
								this.collection.creatorProfile = profile;
							});
						} catch (e) {
							console.error(e);
						}
					}
				}, 250);
			}),
		);
	}

	public isImportedIPFSChanged(): boolean {
		return this.collection.tokensURI !== this.tokensURI
			|| this.collection.totalSupply !== this.totalSupply
			|| this.collection.nftSourceType !== this.nftSourceType
			|| this.collection.isCustomFirstTokenId !== this.isCustomFirstTokenId;
	}

	public getCreatorAlias(): string {
		if (!this.collection) {
			return;
		}

		if (this.collection.creatorProfile?.name) {
			return this.collection.creatorProfile.name;
		}
		const creatorAddress: string = this.userAddress || this.collection.creator;

		return `${creatorAddress.substring(0, 5)}...${creatorAddress.slice(-3)}`;
	}

	public onCollectionEdit(editData: ICollectionEditDto): void {
		this.propertyEditStart.emit(editData);
	}

	public onCollectionAdvancedOptionsEdit(): void {
		this.advancedOptionsEditStart.emit();
	}

	public onAddNFTs(): void {
		this.addNFTsStart.emit();
	}

	public onLaunch(): void {
		this.launchStart.emit();
	}

	public onRemove(index: number): void {
		this.removeNFT.emit(index);
	}

	public onEditNFT(index: number): void {
		if (this.nftSourceType !== 'file') {
			return;
		}

		this.nftEditStart.emit(index);
	}

	public getHintById(id: keyof ICollectionDetails | keyof CollectionIpfsMetadata): ICollectionEditDto {
		return this.hints.required.find((hint) => hint.id === id).editData;
	}

	public getOptionalHintById(id: keyof ICollectionDetails | keyof CollectionIpfsMetadata): ICollectionEditDto {
		return this.hints.optional.find((hint) => hint.id === id).editData;
	}

	public connectWallet(): void {
		this.web3Service.selectWalletType();
	}

	public getAttributesCount(nft: INFTDetails): number {
		if (!nft.attributes) {
			return 0;
		}

		return Object.keys(nft.attributes).length;
	}

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol);
	}

	private updateHints(): void {
		const now: number = moment().unix();

		if (this.collection?.dropFrom) {
			const secondsToStart: number = moment(this.collection.dropFrom).diff(now);

			this.dateFrom = secondsToStart > 0
				? `${moment.duration(secondsToStart, 'seconds').humanize(true)}`
				: 'Today';
		}

		if (this.collection?.dropTo) {
			const secondsToEnd: number = moment(this.collection.dropTo).diff(moment().unix());

			this.dateTo = moment.duration(secondsToEnd, 'seconds').humanize(true);
		}

		this.hints = {
			required: [
				{
					id: 'collectionName',
					text: 'Set collection\'s name',
					completed: !!this.collection.collectionName,
					icon: '🪁',
					editData: {
						id: 'collectionName',
						label: 'Collection name',
						isRequired: true,
						type: 'text',
					},
				},
				{
					id: 'fileURI',
					text: 'Set background image',
					completed: !!this.backgroundImage,
					icon: '🌄',
					editData: {
						id: 'fileURI',
						label: 'Background image',
						isRequired: true,
						type: 'file',
					},
				},
				{
					id: 'squareImageURI',
					text: 'Add collection logo',
					completed: !!this.squareImage,
					icon: '🖼️',
					editData: {
						id: 'squareImageURI',
						label: 'Collection logo',
						isRequired: true,
						type: 'file',
					},
				},
			],
			optional: [
				{
					id: 'mintPrice',
					text: 'Set mint price',
					completed: this.collection.mintPrice > 0,
					icon: '💸',
					editData: {
						id: 'mintPrice',
						label: 'Mint price',
						isRequired: false,
						type: 'number',
					},
				},
				{
					id: 'description',
					text: 'Set collection\'s description',
					completed: this.collection?.metadata?.description.length > 0,
					icon: '💡',
					editData: {
						id: 'description',
						label: 'Set collection\'s description',
						isRequired: false,
						type: 'textarea',
					},
				},
				{
					id: 'external_link',
					text: 'Set Website',
					completed: this.collection?.metadata?.external_link.length > 0,
					icon: '🔗',
					editData: {
						id: 'external_link',
						label: 'Set Website',
						isRequired: false,
						type: 'url',
					},
				},
				{
					id: 'dropFrom',
					text: 'Set minting dates',
					completed: this.collection.dropFrom > 0 || this.collection.dropTo > 0,
					icon: '📅',
					editData: {
						id: 'dropFrom',
						label: 'Set minting dates',
						isRequired: false,
						type: 'date',
					},
				},
				{
					id: 'dropTo',
					text: 'Set minting dates',
					completed: this.collection.dropFrom > 0 || this.collection.dropTo > 0,
					icon: '📅',
					editData: {
						id: 'dropTo',
						label: 'Set minting dates',
						isRequired: false,
						type: 'date',
					},
				},
			],
		};
	}
}
