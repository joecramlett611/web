import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, debounceTime, Observable, Subscription } from 'rxjs';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import IpfsService from '../../../ipfs/services/ipfs.service';
import Web3Service from '../../../web3/services/web3.service';
import { BigNumber, ethers } from 'ethers';
import { Web3Provider } from '@ethersproject/providers';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { StepperConfigType } from '../../../shared/types/stepper-config.type';
import { environment } from '../../../../../environments/environment';
import { AddResult } from 'ipfs-core-types/types/src/root';
import { IStepperStepConfig } from '../../../shared/interfaces/stepper-step-config.interface';
import { StepperStatusEnum } from '../../../shared/enums/stepper-status.enum';
import { CollectionMetadataDto } from '../../dto/collection-metadata.dto';
import { ContractReceipt, ContractTransaction } from '@ethersproject/contracts/src.ts/index';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import * as moment from 'moment';
import { Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import imageCompression from 'browser-image-compression';
import { ICompressImageOptions } from '../../../shared/interfaces/compress-image-options.interface';
import FirebaseStorageService from '../../../firebase/services/firebase-storage.service';
import FirebaseDatabaseService from '../../../firebase/services/firebase-database.service';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import { CollectionStatusEnum } from '../../../web3/enum/collection-status.enum';
import CollectionsService from '../../../chain-data/services/collections.service';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import FirestoreService from '../../../firebase/services/firestore.service';
import { ICreateCollectionTx } from '../../../collections/interfaces/create-collection-tx.interface';
import { UploadResult } from '@firebase/storage';
import { ContentTypeEnum } from '../../../collections/enums/content-type.enum';
import { ArtCategoryEnum } from '../../../collections/enums/art-category.enum';
import { MusicCategoryEnum } from '../../../collections/enums/music-category.enum';
import OmnichainService from '../../../omnichain/services/omnichain.service';
import { IEstimatedFees } from '../../../omnichain/interfaces/estimated-fees.interface';
import { OmnichainActionTypeEnum } from '../../../omnichain/enums/omnichain-action-type.enum';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';
import { LicenceTypeEnum } from '../../../collections/enums/licence-type.enum';
import { MetadataDisplayTypeEnum } from '../../enums/metadata-display-type.enum';
import { Title } from '@angular/platform-browser';
import { ICollectionEditDto } from '../../interfaces/collection-edit-dto.interface';
import { NftAttributes } from '../../../collections/types/nft-attributes.type';
import { WalletTypeEnum } from '../../../web3/enum/wallet-type.enum';
import { WalletProviderEnum } from '../../../web3/enum/wallet-provider.enum';
import AptosMartianWalletService from '../../../aptos/services/aptos-martian-wallet.service';
import { IAptosTransactionDetails } from '../../../aptos/interfaces/aptos-transaction-details.interface';
import { IsolatedAssetEnum } from '../../../web3/enum/isolated-asset.enum';
import AptosPetraWalletService from '../../../aptos/services/aptos-petra-wallet.service';
// import SuiEthosWalletService from '../../../sui/services/sui-ethos-wallet.service';
// import SuiSuietWalletService from '../../../sui/services/sui-suiet-wallet.service';
// import { SuiTransactionResponse } from '@mysten/sui.js/dist/types/transactions';
import AptosPontemWalletService from '../../../aptos/services/aptos-pontem-wallet.service';

@Component({
	selector: 'app-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit, OnDestroy {
	public get supportedNetworksAsText(): string {
		let supportedNetworksText: string = '';

		for (const chainName in this.chainNameToSymbolMap) {
			if (!this.chainNameToSymbolMap[chainName]) {
				continue;
			}

			supportedNetworksText += `${chainName} | `;
		}

		return supportedNetworksText;
	}

	public get supportedAssets(): string[] {
		return environment.supportedAssets;
	}

	public get attributesDisplayTypes(): MetadataDisplayTypeEnum[] {
		return [
			MetadataDisplayTypeEnum.ANY,
			MetadataDisplayTypeEnum.NUMBER,
			MetadataDisplayTypeEnum.BOOST_NUMBER,
			MetadataDisplayTypeEnum.BOOST_PERCENTAGE,
			MetadataDisplayTypeEnum.DATE,
		];
	}

	private get currentStep(): IStepperStepConfig {
		return this.stepper[this.activeStep];
	}

	public get nftFile(): File | undefined {
		return this.nftForm.get('file').value;
	}

	public get availableCategories(): ArtCategoryEnum[] | MusicCategoryEnum[] {
		switch (this.selectedContentType) {
			case ContentTypeEnum.MUSIC:
				return [
					MusicCategoryEnum.ROCK,
					MusicCategoryEnum.BLUES,
					MusicCategoryEnum.HIPHOP,
					MusicCategoryEnum.PUNK,
					MusicCategoryEnum.POP,
					MusicCategoryEnum.KPOP,
					MusicCategoryEnum.INDIE,
					MusicCategoryEnum.ELECTRONIC_DANCE,
					MusicCategoryEnum.RNB_SOUL,
					MusicCategoryEnum.REGGAE,
					MusicCategoryEnum.FUNK,
					MusicCategoryEnum.COUNTRY,
					MusicCategoryEnum.CLASSICAL,
					MusicCategoryEnum.GENERATIVE,
					MusicCategoryEnum.OTHERS,
				];
			case ContentTypeEnum.ART:
				return [
					ArtCategoryEnum.ANY,
					ArtCategoryEnum.AVATAR,
					ArtCategoryEnum.ABSTRACT,
					ArtCategoryEnum.GENERATIVE,
					ArtCategoryEnum.PHOTOGRAPHY,
					ArtCategoryEnum.GAMING,
					ArtCategoryEnum.PAINTING,
					ArtCategoryEnum.TEXTURE,
				];
			default:
				this.onArtCategoryChange(ArtCategoryEnum.ANY);
		}
	}

	public get isConnectedToIsolatedChain(): boolean {
		return this.isConnectedToWallet && environment.isolatedChains.includes(this.connectedChainSymbol);
	}

	public get attributesForm(): FormArray {
		return this.nftForm.get('attributes') as FormArray;
	}

	public get isMintPriceInteger(): boolean {
		const price: number = this.collectionForm.get('mintPrice').value;

		return !price || Number.isInteger(price);
	}

	public get isAdvancedOptionsFormValid(): boolean {
		return this.collectionForm.get('royalty').valid
			&& this.collectionForm.get('isPublic').valid
			&& this.collectionForm.get('chainSymbol').valid;
	}

	public get isFormValid(): boolean {
		return this.isAdvancedOptionsVisible ? this.isAdvancedOptionsFormValid : this.collectionForm.valid;
	}

	@ViewChild('createStepperCloseBtn') public createStepperCloseBtn: ElementRef<HTMLButtonElement>;
	@ViewChild('nftModalCloseBtn') public nftModalCloseBtn: ElementRef<HTMLButtonElement>;
	@ViewChild('editCollectionCloseBtn') public editCollectionCloseBtn: ElementRef<HTMLButtonElement>;
	@ViewChild('introModalCloseBtn') public introModalCloseBtn: ElementRef<HTMLButtonElement>;
	public subscriptions: Subscription;
	public collectionForm: FormGroup;
	public nftForm: FormGroup;
	public nftSourceType: 'file' | 'ipfs';
	public isConnectedToWallet: boolean;
	public userBalance: BigNumber;
	public selectedChainName: SupportedChainNameEnum;
	public selectedAssetName: SupportedAssetEnum | IsolatedAssetEnum;
	public readonly chainSymbols: ChainSymbolEnum[];
	public readonly chainNameToSymbolMap: Partial<ChainNameToSymbolMap>;
	public imagePreviewSrc: string | ArrayBuffer;
	public squareImagePreviewSrc: string | ArrayBuffer;
	public isFileLoading: boolean;
	public isFileLoadingError: boolean;
	public isFileInvalid: boolean;
	public isFileTooBig: boolean;
	public isNFTFileTooBig: boolean;
	public stepper: StepperConfigType;
	public activeStep: number;
	public isDrop: boolean;
	public today: any;
	public isAdvancedOptionsVisible: boolean;
	public file: File;
	public squareImageFile: File;
	public compressedFile: File;
	public nfts: INFTDetails[];
	public contentTypes: ContentTypeEnum[];
	public selectedContentType: ContentTypeEnum;
	public selectedCategory: ArtCategoryEnum | MusicCategoryEnum;
	public selectedLicence: LicenceTypeEnum;
	public licences: LicenceTypeEnum[];
	public collectionPreview: ICollectionDetails;
	public editCollectionData: ICollectionEditDto;
	public readonly onBackgroundImageChange$: BehaviorSubject<File> = new BehaviorSubject<File>(undefined);
	public readonly onSquareImageChange$: BehaviorSubject<File> = new BehaviorSubject<File>(undefined);
	public readonly onUpdatePreview$: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
	private readonly isCreatingCollectionCancelled$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	private id: number;
	private firestoreCollectionId: string;
	private isMetadata: boolean;
	private collectionMetadataIpfsCID: string;
	private backgroundImageIpfsHash: string;
	private squareImageIpfsHash: string;
	private smBackgroundImageIpfsHash: string;
	private smSquareImageIpfsHash: string;
	private collectionFactoryContractAddress: string;
	private readonly createParamsStruct: string = '(string memory dstChainName, string memory name, string memory uri, uint256 price, string memory assetName, uint256 from, uint256 to, string memory tokensURI, uint256 totalSupply, uint gas, uint256 redirectFee)';
	private readonly collectionFactoryContractAbi: string[] = [
		`function create(${this.createParamsStruct} params) public payable`,
		'function repository() public view returns (address)',
		'event Created(address collectionAddress)',
	];
	private createdCollectionAddress: string;
	private userAddress: string;
	public connectedChainSymbol: ChainSymbolEnum;
	private walletCheckOnMint: NodeJS.Timer;
	private createTx?: ContractTransaction;
	private createTxResult: { status: 'success' | 'error', txHash: string };
	private isSubmitted: boolean;
	private collectionFileStoragePath: string;
	private squareFileStoragePath: string;
	private nftsMetadata: INFTMetadata[];
	private tokensMetadataIpfsCID;
	private uploadedCount: number;
	private readonly maxNFTsFilesToUpload: number = 1000;
	private editedNFTIndex: number;
	private isNFTFileChanged: boolean;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly ipfsService: IpfsService,
		private readonly web3Service: Web3Service,
		private readonly firestoreService: FirestoreService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly collectionsService: CollectionsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly omnichainService: OmnichainService,
		private readonly aptosMartianWalletService: AptosMartianWalletService,
		private readonly aptosPontemWalletService: AptosPontemWalletService,
		private readonly aptosPetraWalletService: AptosPetraWalletService,
		// private readonly suiEthosWalletService: SuiEthosWalletService,
		// private readonly suiSuietWalletService: SuiSuietWalletService,
		private readonly titleService: Title,
	) {
		this.chainSymbols = environment.supportedChains;
		this.chainNameToSymbolMap = {};

		for (const chainName in environment.chainNameToSymbolMap) {
			if (!environment.chainNameToSymbolMap[chainName]) {
				continue;
			}
			const chainSymbol: ChainSymbolEnum = environment.chainNameToSymbolMap[chainName];

			if (
				!environment.chainNameToSymbolMap[chainName]
				|| !this.isSupportedNetwork(chainSymbol)
				|| this.isIsolatedChain(chainSymbol)
			) {
				continue;
			}
			this.chainNameToSymbolMap[chainName] = environment.chainNameToSymbolMap[chainName];
		}
	}

	public async ngOnInit(): Promise<void> {
		// Initial state:
		this.id = Number(`9${moment().unix()}${Math.floor(Math.random() * 90 + 10)}`);
		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');
		this.nftSourceType = 'file';
		this.uploadedCount = 0;
		this.stepper = [];
		this.subscriptions = new Subscription();
		this.selectedChainName = SupportedChainNameEnum.Ethereum;
		this.selectedAssetName = SupportedAssetEnum.USDC;
		this.today = moment().startOf('day').toDate();
		this.nfts = [];
		this.nftsMetadata = [];
		this.selectedContentType = ContentTypeEnum.ART;
		this.selectedCategory = ArtCategoryEnum.ANY;
		this.contentTypes = [ContentTypeEnum.ART, ContentTypeEnum.MUSIC];
		this.selectedLicence = LicenceTypeEnum.CREATOR;
		this.licences = [LicenceTypeEnum.CREATOR, LicenceTypeEnum.OWNER, LicenceTypeEnum.ANYONE];
		let assetName: SupportedAssetEnum | IsolatedAssetEnum;

		switch (this.connectedChainSymbol) {
			case ChainSymbolEnum.aptos:
				assetName = IsolatedAssetEnum.APT;
				break;
			// case ChainSymbolEnum.sui:
			// 	assetName = IsolatedAssetEnum.SUI;
			// 	break;
			default:
				assetName = SupportedAssetEnum.USDC;
		}

		this.collectionForm = this.formBuilder.group({
			collectionName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
			description: ['', [Validators.maxLength(1000)]],
			contentType: [ContentTypeEnum.ART, Validators.required],
			category: [ArtCategoryEnum.ANY, Validators.required],
			externalLink: ['', [Validators.maxLength(250)]],
			mintPrice: [0, [Validators.min(0)]],
			totalSupply: [0],
			isCustomFirstTokenId: [false],
			assetName: [assetName, Validators.required],
			dateFrom: [undefined],
			durationInDays: [undefined],
			chainSymbol: [ChainSymbolEnum.eth, [Validators.required, Validators.maxLength(50)]],
			isPublic: [true, Validators.required],
			licence: [this.selectedLicence],
			royalty: [0, [Validators.min(0), Validators.max(50)]],
			tokensURI: [undefined, [Validators.max(59)]],
		});
		this.updateCollectionPreview();

		this.subscriptions.add(
			this.collectionForm.valueChanges
				.pipe(debounceTime(500))
				.subscribe(() => {
					this.updateCollectionPreview();
				}),
			);

		this.nftForm = this.formBuilder.group({
			name: [undefined, Validators.maxLength(250)],
			file: [undefined, Validators.required],
			attributes: this.formBuilder.array([]),
		});

		this.subscriptions.add(
			this.collectionForm.get('mintPrice').valueChanges.subscribe((value: number) => {
				this.isDrop = Number(value) > 0;
			}),
		);

		this.subscriptions.add(
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isWalletConnected();
					if (this.isConnectedToWallet) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

						this.connectedChainSymbol = this.isSupportedNetwork(connectedChainSymbol) ? connectedChainSymbol : ChainSymbolEnum.eth;
						this.collectionPreview.srcChain = this.connectedChainSymbol;
						this.collectionPreview.chainSymbol = this.connectedChainSymbol;

						if (!this.connectedChainSymbol) {
							await this.connectWallet();
						}
						this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[this.connectedChainSymbol];
						const chainSymbolControl: AbstractControl = this.collectionForm.get('chainSymbol');

						if (this.connectedChainSymbol?.length > 0) {
							chainSymbolControl.setValue(this.connectedChainSymbol);
							this.selectedChainName = this.mapChainSymbolToName(this.connectedChainSymbol);

							switch (this.connectedChainSymbol) {
								case ChainSymbolEnum.aptos:
									this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.APT);
									this.selectedAssetName = IsolatedAssetEnum.APT;
									break;
								// case ChainSymbolEnum.sui:
								// 	this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.SUI);
								// 	this.selectedAssetName = IsolatedAssetEnum.SUI;
								// 	break;
								default:
									this.collectionForm.get('assetName').setValue(SupportedAssetEnum.USDC);
									this.selectedAssetName = SupportedAssetEnum.USDC;
							}
						}

						this.web3Service.getNativeAssetBalance(this.userAddress).then((userBalance: BigNumber) => {
							this.userBalance = userBalance;
						});
					}
				}, 250);
			}),
		);

		this.titleService.setTitle('Omnisea - Launch Omnichain collection');

		setTimeout(() => {
			this.openIntroModal();
		}, 1500);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
		this.closeIntroModal();
	}

	public onBackgroundFileChange(event: Event): void {
		this.isNFTFileChanged = true;
		this.isFileInvalid = false;
		this.isFileLoadingError = false;
		this.isFileTooBig = false;
		this.imagePreviewSrc = undefined;
		this.file = undefined;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (input?.files?.[0]) {
			this.file = input.files[0];
			const mimeType: string = this.file.type;

			if (!mimeType.match(/image\/*/)) {
				this.notificationsService.error('Not Image', 'Collection\'s cover has to be an image.');

				return;
			}

			if (this.isFileValid('background')) {
				this.compressCollectionImageAndSaveInIpfs('background');
			}
			const reader = new FileReader();

			reader.onloadstart = () => {
				this.isFileLoading = true;
			};
			reader.onload = async (e) => {
				this.isFileLoading = false;

				if (!this.isFileValid('background')) {
					this.isFileInvalid = true;
					this.file = undefined;
					return;
				}

				if (!mimeType.match(/image\/*/)) {
					return;
				}
				this.onBackgroundImageChange$.next(this.file);
				this.onUpdatePreview$.next();
				this.imagePreviewSrc = reader.result;
			};
			reader.onerror = (err) => {
				this.isFileLoading = false;
				this.isFileLoadingError = true;
				console.error(err);
			};
			reader.readAsDataURL(this.file);
		}
	}

	public onSquareFileChange(event: Event): void {
		this.isNFTFileChanged = true;
		this.squareImagePreviewSrc = undefined;
		this.squareImageFile = undefined;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (input?.files?.[0]) {
			this.squareImageFile = input.files[0];
			const mimeType: string = this.squareImageFile.type;

			if (!mimeType.match(/image\/*/)) {
				this.notificationsService.error('Not Image', 'File has to be an image.');

				return;
			}

			if (this.isFileValid('square')) {
				this.compressCollectionImageAndSaveInIpfs('square');
			}
			const reader = new FileReader();
			reader.onload = async () => {
				if (!this.isFileValid('square')) {
					this.squareImageFile = undefined;
					return;
				}

				if (!mimeType.match(/image\/*/)) {
					return;
				}
				this.onSquareImageChange$.next(this.squareImageFile);
				this.onUpdatePreview$.next();
				this.squareImagePreviewSrc = reader.result;
			};
			reader.onerror = (err) => {
				this.notificationsService.error('Could not upload square image');
				console.error(err);
			};
			reader.readAsDataURL(this.squareImageFile);
		}
	}

	public onAddNFTsFiles(event: Event): void {
		this.isNFTFileChanged = true;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (!input?.files?.[0]) {
			return;
		}
		const files: FileList = input.files;

		for (let i = 0; i < files.length; i++) {
			const file: File = files.item(i);
			const mimeType: string = file?.type;

			if (this.selectedContentType === ContentTypeEnum.MUSIC && !mimeType.match(/audio\/*/)) {
				this.notificationsService.error('Add Audio', 'Music type collection\'s NFTs have to be audio files');

				continue;
			}

			if (this.selectedContentType === ContentTypeEnum.ART && !mimeType.match(/image\/*/)) {
				this.notificationsService.error('Add Image', 'Art type collection\'s NFTs have to be image files');

				continue;
			}
			const reader = new FileReader();

			reader.onload = async () => {
				if (!this.isNFTFileValid(file)) {
					return;
				}
				const nftDetails: INFTDetails = {
					file,
					fileURL: reader.result,
					attributes: {},
				};

				this.nfts.push(nftDetails);
			};

			if (!mimeType.match(/image\/*/)) {
				continue;
			}

			reader.readAsDataURL(file);
		}
	}

	public async onSubmit(): Promise<void> {
		this.isSubmitted = true;
		this.uploadedCount = 0;
		this.isCreatingCollectionCancelled$.next(false);

		if (!this.isConnectedToWallet || !this.connectedChainSymbol) {
			await this.connectWallet();
		}

		if (this.collectionForm.get('collectionName').value.length < 3) {
			this.notificationsService.error('Invalid Name', 'Name has to contain minimum 3 characters.');

			return;
		}

		if (this.collectionForm.invalid) {
			this.notificationsService.error('Invalid Form', 'Check the form. Provide at least name and file.');

			return;
		}

		if (!this.isFileSupported()) {
			this.notificationsService.error('Upload File', 'Make sure to upload image or audio file.');
			return;
		}

		if (this.selectedContentType === 'MUSIC' && this.nfts?.length === 0) {
			this.notificationsService.error('Add NFTs', 'Music collection requires at least 1 audio NFT');
			return;
		}
		const externalLink: string = this.collectionForm.get('externalLink').value;

		if (externalLink?.length) {
			const reg: RegExp = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');

			if (!reg.test(externalLink)) {
				this.notificationsService.error('Invalid External Link', 'Provided link is not a valid URL');
				return;
			}
		}
		this.isMetadata = this.isMetadataProvided();
		this.buildSteps();

		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		this.subscriptions.add(this.createCollection().subscribe());
	}

	public isWalletConnected(): boolean {
		return this.web3Service.isWalletConnected();
	}

	public connectWallet(): void {
		this.web3Service.selectWalletType();
	}

	public onTargetChainChange(chainName: SupportedChainNameEnum): void {
		this.selectedChainName = chainName;
		this.collectionForm.get('chainSymbol').setValue(this.chainNameToSymbolMap[chainName]);
	}

	public onAssetChange(assetName: string): void {
		this.selectedAssetName = SupportedAssetEnum[assetName];
		this.collectionForm.get('assetName').setValue(this.selectedAssetName);
	}

	public onContentTypeChange(contentType: ContentTypeEnum): void {
		this.selectedContentType = this.mapContentTypeToEnum(contentType);
		this.collectionForm.get('contentType').setValue(this.selectedContentType);
		this.onCategoryChange();
	}

	public onCategoryChange(category?: ArtCategoryEnum | MusicCategoryEnum): void {
		switch (this.selectedContentType) {
			case ContentTypeEnum.MUSIC:
				this.onMusicCategoryChange(category as MusicCategoryEnum || MusicCategoryEnum.ROCK);
				break;
			case ContentTypeEnum.ART:
			default:
				this.onArtCategoryChange(category as ArtCategoryEnum || ArtCategoryEnum.ANY);
		}
		this.collectionForm.get('category').setValue(this.selectedCategory);
	}

	public onArtCategoryChange(category: ArtCategoryEnum): void {
		this.selectedCategory = category;
	}

	public onMusicCategoryChange(category: MusicCategoryEnum): void {
		this.selectedCategory = category;
	}

	public onLicenceTypeChange(licenceType: LicenceTypeEnum): void {
		this.selectedLicence = licenceType;
		this.collectionForm.get('licence').setValue(this.selectedLicence);
	}

	public mapChainNameToEnum(chainName: string): SupportedChainNameEnum {
		return chainName as SupportedChainNameEnum;
	}

	public mapContentTypeToEnum(contentType: string): ContentTypeEnum {
		return ContentTypeEnum[contentType.toUpperCase()];
	}

	public mapLicenceTypeToEnum(licenceType: string): LicenceTypeEnum {
		return LicenceTypeEnum[licenceType.toUpperCase()];
	}

	public isNFTFileSupported(): boolean {
		if (!this.nftFile) {
			return false;
		}

		return !!this.nftFile.type.match(/image\/*/)
			|| !!this.nftFile.type.match(/audio\/*/);
	}

	public isFileSupported(): boolean {
		if (!this.file) {
			return false;
		}

		return !!this.file.type.match(/image\/*/)
			|| !!this.file.type.match(/audio\/*/);
	}

	public isBackgroundFileImage(): boolean {
		if (!this.file) {
			return false;
		}

		return !!this.file.type.match(/image\/*/);
	}

	public isSquareFileImage(): boolean {
		if (!this.squareImageFile) {
			return false;
		}

		return !!this.squareImageFile.type.match(/image\/*/);
	}

	public stopCreatingCollection(): void {
		this.isCreatingCollectionCancelled$.next(true);
	}

	public isSupportedNetworkOrDisconnected(): boolean {
		const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		return !connectedChainSymbol || this.isSupportedNetwork(connectedChainSymbol);
	}

	public isIsolatedChain(chainSymbol: ChainSymbolEnum): boolean {
		return environment.isolatedChains.includes(chainSymbol);
	}

	public isCreateButtonDisabled(): boolean {
		return (this.isConnectedToWallet && this.collectionForm.invalid)
			|| !this.isSupportedNetworkOrDisconnected()
			|| (this.selectedContentType === 'MUSIC' && this.nfts?.length === 0);
	}

	public onEditNFT(): void {
		if (this.nftSourceType !== 'file') {
			return;
		}
		this.nfts[this.editedNFTIndex].name = this.nftForm.get('name').value || '';
		this.nfts[this.editedNFTIndex].attributes = this.attributesForm.value;
		this.closeNFTModal();
	}

	public closeNFTModal(): void {
		this.nftForm.reset();
		this.attributesForm.reset();
		this.attributesForm.clear();
		this.nftModalCloseBtn?.nativeElement?.click();
	}

	public getAttributesCount(nft: INFTDetails): number {
		if (!nft.attributes) {
			return 0;
		}

		return Object.keys(nft.attributes).length;
	}

	public onRemoveNFT(index: number): void {
		this.nfts.splice(index, 1);
	}

	public getCategoryName(category: MusicCategoryEnum | ArtCategoryEnum): string {
		switch (category) {
			case MusicCategoryEnum.ELECTRONIC_DANCE:
				return 'Electronic & Dance';
			case MusicCategoryEnum.KPOP:
				return 'K-pop';
			case MusicCategoryEnum.RNB_SOUL:
				return 'RNB & Soul';
			case MusicCategoryEnum.HIPHOP:
				return 'Hip-hop';
			default:
				return category.charAt(0) + category.slice(1).toLowerCase();
		}
	}

	public addAttribute(): void {
		this.attributesForm.push(
			this.formBuilder.group({
				trait_type: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(500)]],
				display_type: [MetadataDisplayTypeEnum.ANY],
				value: [undefined, [Validators.required]],
			}),
		);
	}

	public removeAttribute(attributeIndex: number): void {
		this.attributesForm.removeAt(attributeIndex);
	}

	public getAttributeDisplayType(attributeIndex: number): MetadataDisplayTypeEnum {
		return this.attributesForm.at(attributeIndex).get('display_type').value;
	}

	public onDisplayTypeChange(attributeIndex: number, displayType: MetadataDisplayTypeEnum): void {
		const attributeControl: AbstractControl = this.attributesForm.at(attributeIndex);
		const displayTypeControl: AbstractControl = attributeControl.get('display_type');
		const valueControl: AbstractControl = attributeControl.get('value');

		valueControl.clearValidators();
		valueControl.setValue(undefined);
		displayTypeControl.setValue(displayType);

		switch (displayType) {
			case MetadataDisplayTypeEnum.ANY:
				break;
			case MetadataDisplayTypeEnum.DATE:
				valueControl.setValue(moment().unix());
				valueControl.setValidators([Validators.required, Validators.min(0)]);
				break;
			case MetadataDisplayTypeEnum.BOOST_PERCENTAGE:
				valueControl.setValue(100);
				valueControl.setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
				break;
			default:
				valueControl.setValue(0);
				valueControl.setValidators([Validators.required, Validators.min(0)]);
		}

		valueControl.updateValueAndValidity({onlySelf: false, emitEvent: true});
	}

	public getDisplayTypeName(displayType: MetadataDisplayTypeEnum): string {
		switch (displayType) {
			case MetadataDisplayTypeEnum.ANY:
				return 'Any';
			case MetadataDisplayTypeEnum.BOOST_PERCENTAGE:
				return 'Percentage';
			case MetadataDisplayTypeEnum.BOOST_NUMBER:
				return 'Boost Number';
			case MetadataDisplayTypeEnum.NUMBER:
				return 'Numeric';
			case MetadataDisplayTypeEnum.DATE:
				return 'Timestamp';
			default:
				return 'Any';
		}
	}

	public closeIntroModal(): void {
		this.introModalCloseBtn?.nativeElement?.click();
	}

	public closeCollectionEditModal(): void {
		this.imagePreviewSrc = undefined;
		const filePicker: HTMLInputElement = document.querySelector<HTMLInputElement>('#filePicker');
		const squareFilePicker: HTMLInputElement = document.querySelector<HTMLInputElement>('#squareFilePicker');

		if (filePicker) {
			filePicker.value = '';
		}
		if (squareFilePicker) {
			squareFilePicker.value = '';
		}
		this.editCollectionCloseBtn?.nativeElement?.click();
	}

	public onCollectionEditStart(editData: ICollectionEditDto): void {
		this.isAdvancedOptionsVisible = false;
		this.editCollectionData = editData;

		if (this.editCollectionData.id === 'fileURI') {
			document.querySelector<HTMLButtonElement>('#backgroundImagePicker').click();

			return;
		}

		if (this.editCollectionData.id === 'squareImageURI') {
			document.querySelector<HTMLButtonElement>('#squareFilePicker').click();

			return;
		}

		if (this.editCollectionData.id === 'tokensURI') {
			if (this.nftSourceType !== 'ipfs') {
				this.nfts = [];
			}

			this.nftSourceType = 'ipfs';
		}

		this.openEditCollectionModal();
	}

	public onCollectionAdvancedOptionsEditStart(): void {
		this.isAdvancedOptionsVisible = true;
		this.editCollectionData = undefined;
		this.openEditCollectionModal();
	}

	public openAddNFTsModal(): void {
		if (this.nftSourceType === 'ipfs') {
			this.nfts = [];
			this.collectionForm.get('totalSupply').setValue(0);
		}
		this.nftSourceType = 'file';
		const triggerBtn: HTMLInputElement = document.querySelector<HTMLInputElement>('#nftFilePicker');

		triggerBtn.click();
	}

	public openEditNFTsModal(index: number): void {
		if (this.nftSourceType !== 'file') {
			this.notificationsService.error('Can\'t edit imported NFT');
			return;
		}
		this.editedNFTIndex = index;
		const nft: INFTDetails = this.nfts[this.editedNFTIndex];

		this.attributesForm.reset();
		this.attributesForm.clear();
		if (this.getAttributesCount(nft) > 0) {
			const attributes: NftAttributes[] = Array.isArray(nft.attributes) ? nft.attributes : [];

			for (const attribute of attributes) {
				this.attributesForm.push(this.formBuilder.group(attribute));
			}
		}
		this.nftForm.get('name').setValue(nft.name || '');
		const triggerBtn: HTMLInputElement = document.querySelector<HTMLInputElement>('#nftEditButton');

		triggerBtn.click();
	}

	private openIntroModal(): void {
		const triggerBtn: HTMLButtonElement = document.querySelector<HTMLButtonElement>('#introModalButton');

		triggerBtn.click();
	}

	private openEditCollectionModal(): void {
		const triggerBtn: HTMLButtonElement = document.querySelector<HTMLButtonElement>('#editCollectionButton');

		triggerBtn.click();
	}

	private async saveNFTs(): Promise<void> {
		return (new Promise(async (resolve, reject) => {
			let tokenId: number = 0;

			if (this.nfts.length > this.maxNFTsFilesToUpload) {
				this.notificationsService.error('IPFS limit', `Max files to upload on our free IPFS: ${this.maxNFTsFilesToUpload}. Please, import your own IPFS source.`);
				throw new Error(`Max files to upload on IPFS: ${this.maxNFTsFilesToUpload}!`);
			}
			const totalSupply: number = this.nfts.length;

			this.currentStep.subtitle = `Saving 1/${this.nfts.length} NFTs`;
			for (const nft of this.nfts) {
				if (!nft) {
					continue;
				}

				nft.tokenId = tokenId;
				tokenId++;
				this.saveNFT(nft).then(async () => {
					this.uploadedCount++;
					if (this.uploadedCount !== totalSupply) {
						this.currentStep.subtitle = `Saved ${this.uploadedCount}/${this.nfts.length} NFTs`;
						return;
					}
					const ipfsNFTsResult = await this.saveNFTsMetadataInIPFS();

					this.tokensMetadataIpfsCID = ipfsNFTsResult.cid.toString();
					this.isNFTFileChanged = false;
					resolve();
				}).catch((e) => {
					console.error(e);
					this.stopCreatingCollection();
					this.notificationsService.error('Error', `NFT #${nft.tokenId} failed to upload`);
					reject(`NFT #${nft.tokenId} failed to upload`);
				});
			}
		}));
	}

	private async saveNFT(nft: INFTDetails): Promise<void> {
		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		if (!nft) {
			throw new Error('No NFT data');
		}

		if (!nft.file) {
			throw new Error('No NFT file');
		}

		if (!nft.file.type.match(/image\/*/)
			&& !nft.file.type.match(/audio\/*/)) {
			throw new Error('Unsupported NFT file type');
		}

		if ((nft.file.size / 1024 / 1024) > 10) {
			throw new Error('NFT file > 10MB');
		}
		await this.compressNFTFileAndSaveInIpfs(nft);
		await this.saveNFTInDatabase(nft);
	}

	private createCollection(): Observable<void> {
		if (
			this.nftSourceType === 'ipfs'
			&& (this.collectionForm.get('tokensURI').value.length !== 46 && this.collectionForm.get('tokensURI').value.length !== 59)
		) {
			this.notificationsService.error('Invalid imported IPFS', 'Imported IPFS has to be 46 or 59 characters CID');
			throw new Error('Imported IPFS has to be 46 or 59 characters CID');
		}

		return new Observable<void>((subscriber) => {
			try {
				if (!!this.file) {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}
					if (!this.isFileValid('background') || !this.isFileValid('square')) {
						this.stepper[0] = {
							title: 'File is invalid',
							iconText: 'File upload',
							subtitle: 'Please, verify correctness and size of background and avatar files',
							status: StepperStatusEnum.ERROR,
							isActive: true,
						};
						this.activeStep = 0;

						this.notificationsService.error(
							'File Error',
							'Please, verify correctness and size of background and avatar files',
						);
						this.closeStepper();
						subscriber.complete();
						return;
					}

					this.activeStep = 0;
					this.sendCollectionImagesToIpfs();
				} else if (this.isMetadata) {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}

					this.stepper[0] = {
						title: `Saving ${this.nfts.length ? 'NFTs ' : ''}metadata in IPFS`,
						iconText: `Save ${this.nfts.length ? 'NFTs ' : ''}metadata`,
						status: StepperStatusEnum.PENDING,
						isActive: true,
					};
					this.activeStep = 0;
					this.sendMetadataToIpfs();
				} else {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}
					this.stepper[0] = {
						title: 'Almost ready',
						iconText: 'Confirm',
						status: StepperStatusEnum.PENDING,
						isActive: true,
					};
					this.activeStep = 0;

					this.create().then(() => {
						subscriber.complete();
					}, (e) => {
						subscriber.error(e);
					});
				}
			} catch (e) {
				console.error(e);
				subscriber.error(e);
			}
		});
	}

	private getDateFromTimestamp(): number {
		const dateFrom: Date = this.collectionForm.get('dateFrom').value;

		if (!dateFrom) {
			return 0;
		}

		return moment(dateFrom).unix();
	}

	private getDateToTimestamp(): number {
		const durationInDays: number = Number(this.collectionForm.get('durationInDays').value);

		if (!durationInDays || durationInDays < 1) {
			return 0;
		}
		const now: number = moment().unix();
		const dateFrom: number = this.getDateFromTimestamp();
		const startTimestamp: number = dateFrom || now;

		return moment(moment.unix(startTimestamp).toDate()).add(durationInDays, 'days').unix();
	}

	private isFileValid(type: 'background' | 'square'): boolean {
		if (type === 'background') {
			if (!this.file) {
				return false;
			}

			// File bigger than 10MB is too big
			if ((this.file.size / 1024 / 1024) > 10) {
				this.notificationsService.error('Background image has to be max. 10MB');
				this.isFileTooBig = true;

				return false;
			}
		} else if (type === 'square') {
			if (!this.squareImageFile) {
				return false;
			}

			// File bigger than 10MB is too big
			if ((this.squareImageFile.size / 1024 / 1024) > 10) {
				this.notificationsService.error('Square image has to be max. 10MB');

				return false;
			}
		}

		return true;
	}

	private isNFTFileValid(file: File): boolean {
		// File bigger than 10MB is too big
		return (file.size / 1024 / 1024) <= 10;
	}

	private isMetadataProvided(): boolean {
		return !!this.file
			|| this.collectionForm.get('collectionName').value?.length > 0
			|| this.collectionForm.get('description').value?.length > 0
			|| this.collectionForm.get('externalLink').value?.length > 0;
	}

	private async sendCollectionImagesToIpfs(): Promise<void> {
		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');

		this.ipfsService.sendFile(this.file).then((result: AddResult) => {
			this.ipfsService.sendFile(this.squareImageFile).then((squareImageIpfsResult: AddResult) => {
				this.squareImageIpfsHash = squareImageIpfsResult.path;
				this.backgroundImageIpfsHash = result.path;
				this.currentStep.title = 'File successfully uploaded to IPFS 🥳';
				this.currentStep.status = StepperStatusEnum.COMPLETE;

				setTimeout(() => {
					this.currentStep.isActive = false;
					this.stepper[1] = {
						title: this.nfts.length ? 'Saving NFTs metadata in IPFS' : 'Saving metadata in IPFS',
						iconText: this.nfts.length ? 'Save NFTs metadata' : 'Save metadata',
						status: StepperStatusEnum.PENDING,
						isActive: true,
					};
					this.activeStep = 1;
					this.sendMetadataToIpfs();
				}, 3000);
			});
		}).catch((e) => {
			console.error(e);
			this.currentStep.title = 'Couldn\'t upload file to IPFS';
			this.currentStep.subtitle = 'Verify your file correctness, try again, or contact us.';
			this.currentStep.status = StepperStatusEnum.ERROR;
			this.closeStepper(3000);
		}).finally(() => {
			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
		});
	}

	private async sendMetadataToIpfs(): Promise<void> {
		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}
		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');
		const metadata: CollectionMetadataDto = this.buildMetadata();
		const metadataResult: AddResult = await this.ipfsService.saveMetadata(JSON.stringify(metadata));

		this.collectionMetadataIpfsCID = metadataResult.cid.toString();
		const isUploadingFilesToIPFS: boolean = this.nftSourceType === 'file' && this.nfts.length > 0;

		if (isUploadingFilesToIPFS && this.isNFTFileChanged) {
			await this.saveNFTs();
		}
		this.currentStep.subtitle = isUploadingFilesToIPFS ? 'Saved all NFTs 🥳' : 'Saved Metadata 🥳';
		this.onMetadataSaved();
	}

	private onMetadataSaved(): void {
		setTimeout(() => {
			this.currentStep.isActive = false;
			const activeStep: number = 2;

			this.stepper[activeStep] = {
				title: `${this.nftSourceType === 'file' && this.nfts.length > 0 ? 'NFTs and ' : ''}Metadata successfully uploaded to IPFS 🥳`,
				iconText: 'Confirm',
				status: StepperStatusEnum.PENDING,
				isActive: true,
			};
			this.activeStep = activeStep;
			this.create();
		}, 3000);
	}

	private isDecimalPriceSupported(): boolean {
		const chainSymbol: ChainSymbolEnum = this.collectionForm.get('chainSymbol').value;

		return [ChainSymbolEnum.sui].includes(chainSymbol);
	}

	private getValidatedMintPrice(): number {
		const mintPrice: number = this.collectionForm.get('mintPrice').value;

		if (!mintPrice) {
			return 0;
		}

		if (this.isMintPriceInteger || this.isDecimalPriceSupported()) {
			return Number(mintPrice);
		}

		return Math.trunc(mintPrice);
	}

	private async saveCollectionInDatabase(status: CollectionStatusEnum): Promise<void> {
		if (status === CollectionStatusEnum.CREATING && this.createTxResult?.status !== 'success') {
			console.error(this.createTxResult);
		}

		const payload: ICollectionDetails = {
			id: this.id,
			firestoreId: this.firestoreCollectionId || null,
			status,
			collectionName: this.collectionForm.get('collectionName').value,
			mintPrice: this.getValidatedMintPrice(),
			assetName: this.selectedAssetName,
			totalSupply: this.nftSourceType === 'ipfs' ? (this.collectionForm.get('totalSupply').value || 0) : (this.nfts.length || 0),
			isCustomFirstTokenId: this.nftSourceType === 'ipfs' ? (this.collectionForm.get('isCustomFirstTokenId').value || false) : false,
			fileURI: this.isBackgroundFileImage() && this.smBackgroundImageIpfsHash?.length
				? `${this.smBackgroundImageIpfsHash}_${this.id}`
				: '',
			squareImageURI: (this.isSquareFileImage() && this.squareImageIpfsHash)
				? `https://omnisea.infura-ipfs.io/ipfs/${this.squareImageIpfsHash}`
				: '',
			fileStoragePath: this.collectionFileStoragePath,
			metadataURI: this.collectionMetadataIpfsCID,
			tokensURI: this.getTokensURI(),
			totalMinted: 0,
			createdAt: moment().unix(),
			srcChain: this.connectedChainSymbol,
			chainSymbol: this.collectionForm.get('chainSymbol').value,
			creator: this.userAddress,
			dropFrom: this.getDateFromTimestamp(),
			dropTo: this.getDateToTimestamp(),
			metadata: {
				name: this.collectionForm.get('collectionName').value,
				description: this.collectionForm.get('description').value || '',
				external_link: this.collectionForm.get('externalLink').value || '',
				media: this.isFileSupported() && this.backgroundImageIpfsHash?.length ? `${this.backgroundImageIpfsHash}_${this.id}` : '',
			},
			isPublic: this.collectionForm.get('isPublic').value,
			contentType: this.selectedContentType,
			category: this.selectedCategory,
			licence: this.collectionForm.get('licence').value,
			nftSourceType: this.nftSourceType,
			tokenFactoryVersion: this.getCurrentTokenFactoryVersion(),
		};

		if ([CollectionStatusEnum.CREATING, CollectionStatusEnum.ERROR].includes(status)) {
			payload.txHash = this.createTxResult?.txHash || null;
			payload.txStatus = this.createTxResult?.status || null;
		}

		try {
			if (status === CollectionStatusEnum.INITIATED) {
				const collectionFirestoreAddResult = await this.firestoreService.add(
					'collections',
					[],
					payload,
				);
				this.firestoreCollectionId = collectionFirestoreAddResult.id;
			} else {
				await this.firestoreService.set(
					'collections',
					this.firestoreCollectionId,
					payload,
				);
			}
		} catch (e) {
			console.error(e);
		}

		await this.collectionsService.saveCollectionInDatabase(
			this.userAddress,
			payload,
		);
	}

	private async saveNFTInDatabase(nft: INFTDetails): Promise<void> {
		try {
			this.firestoreService.add(
				'tokens',
				[],
				{
					collectionId: this.id,
					tokenId: nft.tokenId,
					fileIpfsHash: `${nft.fileIpfsHash}_${this.id}`,
					smFilePath: `sm_${nft.tokenId}_${this.id}`,
					fileStoragePath: nft.fileStoragePath,
				},
				undefined,
				async () => {
					// TODO: Uncomment and remove firebaseDatabase.save when ready to migrate
					// this.stopCreatingCollection();
					// this.notificationsService.error('Error', 'Cannot save your NFTs!');
					// throw new Error(`Cannot store NFT of user: ${this.userAddress} at: ${moment().unix()}`);
				}
			);
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.save<INFTDetails>(
			`/tokens/${this.id}/${nft.tokenId}`,
			{
				tokenId: nft.tokenId,
				fileIpfsHash: `${nft.fileIpfsHash}_${this.id}`,
				smFilePath: `sm_${nft.tokenId}_${this.id}`,
			},
			undefined,
			async () => {
				this.stopCreatingCollection();
				this.notificationsService.error('Error', 'Cannot save your NFTs!');
				throw new Error(`Cannot store NFT of user: ${this.userAddress} at: ${moment().unix()}`);
			}
		);
	}

	private async saveCreateTxInDatabase(): Promise<void> {
		const hash: string = this.createTx?.hash;

		if (!hash) {
			return;
		}

		try {
			// TODO: Uncomment and remove firebaseDatabase.save when ready to migrate
			const createCollectionTx: ICreateCollectionTx = {
				collectionId: this.id,
				hash,
			};
			await this.firestoreService.add(
				'transactions',
				[this.userAddress, 'createCollection'],
				createCollectionTx,
			);
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.save<Pick<ContractTransaction, 'hash'>>(
			`/transactions/${this.userAddress}/createCollection/${this.id}`,
			{hash},
		);
	}

	private buildMetadata(): CollectionMetadataDto {
		const metadata: CollectionMetadataDto = {
			name: this.collectionForm.get('collectionName').value || '',
			description: this.collectionForm.get('description').value || undefined,
			external_link: this.collectionForm.get('externalLink').value || undefined,
			image: this.isSquareFileImage() && this.squareImageIpfsHash?.length ? `https://omnisea.infura-ipfs.io/ipfs/${this.squareImageIpfsHash}` : '',
		};
		let royalty: number = this.collectionForm.get('royalty').value;

		if (royalty) {
			if (royalty > 50) {
				this.collectionForm.get('royalty').setValue(50);
				royalty = 50;
			}

			const basicPoints: number = 100;

			metadata.fee_recipient = this.userAddress;
			metadata.seller_fee_basis_points = Math.trunc(royalty * basicPoints);
		}

		return metadata;
	}

	private async create(): Promise<void> {
		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');
		const connectedWalletType: WalletTypeEnum = this.web3Service.getConnectedWalletType();
		const connectedWalletDetails = await this.web3Service.getConnectedWallet();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			return await this.createEVMCollection();
		}

		if (connectedWalletType === WalletTypeEnum.APTOS) {
			if (connectedWalletDetails.provider === WalletProviderEnum.APTOS_MARTIAN) {
				return await this.createAptosCollectionUsingMartian();
			}

			if (connectedWalletDetails.provider === WalletProviderEnum.APTOS_PETRA) {
				return await this.createAptosCollectionUsingPetra();
			}

			if (connectedWalletDetails.provider === WalletProviderEnum.APTOS_PONTEM) {
				return await this.createAptosCollectionUsingPontem();
			}
		}

		// if (connectedWalletType === WalletTypeEnum.SUI) {
		// 	if (connectedWalletDetails.provider === WalletProviderEnum.SUI_ETHOS) {
		// 		return await this.createSuiCollectionUsingEthos();
		// 	}
		//
		// 	if (connectedWalletDetails.provider === WalletProviderEnum.SUI_SUIET) {
		// 		return await this.createSuiCollectionUsingSuiet();
		// 	}
		// }
	}

	private async createAptosCollectionUsingPontem(): Promise<void> {
		const connectedWalletDetails = await this.web3Service.getConnectedWallet();
		const walletProvider: WalletProviderEnum = connectedWalletDetails.provider;

		this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.APT);
		this.selectedAssetName = IsolatedAssetEnum.APT;

		if (walletProvider !== WalletProviderEnum.APTOS_PONTEM) {
			this.notificationsService.error('Connect your Pontem wallet');
			throw new Error('Pontem wallet is not connected');
		}
		const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
		const chainSymbol: ChainSymbolEnum = chainControl.value;

		if (!chainSymbol) {
			chainControl.setValue(ChainSymbolEnum.aptos);
		}

		if (!this.collectionFactoryContractAddress) {
			this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos];
		}
		this.currentStep.title = 'Confirm';
		this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';

		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}
		const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;

		if (this.nftSourceType === 'file' && !areNFTsUploaded) {
			this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
			throw new Error('Not all NFTs were uploaded to IPFS');
		}

		const royalty: number = this.collectionForm.get('royalty').value;
		const basisPoints: number = 100;
		const isCustomFirstTokenId: boolean = this.nftSourceType === 'ipfs' && this.collectionForm.get('isCustomFirstTokenId').value;
		const payload = [
			this.collectionForm.get('collectionName').value,
			this.collectionForm.get('description').value,
			this.collectionMetadataIpfsCID || '',
			this.getTokensURI(),
			this.getValidatedMintPrice(),
			this.getDateFromTimestamp(),
			this.getDateToTimestamp(),
			this.getTokenSupply(),
			royalty ? (royalty * basisPoints) : 0,
			isCustomFirstTokenId ? 1 : 0,
			0, // Custom seed
		];

		try {
			const txHash: string = await this.aptosPontemWalletService.sendTransaction(connectedWalletDetails.address, {
				function: environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos].replace(
					'{moduleName}',
					environment.aptosSpecific.versionToModuleName[environment.currentTokenFactoryVersion[ChainSymbolEnum.aptos]],
				),
				arguments: payload,
				type_arguments: [],
			});

			this.saveCreateTxInDatabase();
			await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
			this.currentStep.title = 'Waiting for transaction confirmation';
			this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';

			try {
				const txResult: IAptosTransactionDetails = await this.aptosPontemWalletService.getTransaction(txHash);

				if (!txResult.success) {
					throw new Error('Transaction has failed');
				}
				this.createTxResult = {
					txHash,
					status: 'success',
				};
				// Save as CREATED / ERROR because Aptos is currently only Isolated Chain
				this.saveCollectionInDatabase(CollectionStatusEnum.CREATED);
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.currentStep.title = 'Success! 🥳';
				this.currentStep.subtitle = 'Collection will be visible in a moment.';
				this.currentStep.status = StepperStatusEnum.COMPLETE;
				this.notificationsService.success('Created', 'Collection will be visible in your profile');
				setTimeout(() => {
					this.closeStepper();
					setTimeout(() => {
						this.goToUserCollections(chainSymbol);
					}, 500);
				}, 500);
			} catch (e) {
				console.error(e);
				this.currentStep.title = 'Could not confirm the transaction';
				this.currentStep.subtitle = 'Please, check if your collection was created.';
				this.currentStep.status = StepperStatusEnum.ERROR;
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.closeStepper(3000);
			}
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Could not create the collection');
		}
	}

	private async createAptosCollectionUsingMartian(): Promise<void> {
		const connectedWalletDetails = await this.web3Service.getConnectedWallet();
		const walletProvider: WalletProviderEnum = connectedWalletDetails.provider;

		this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.APT);
		this.selectedAssetName = IsolatedAssetEnum.APT;

		if (walletProvider !== WalletProviderEnum.APTOS_MARTIAN) {
			this.notificationsService.error('Connect your Martian wallet');
			throw new Error('Martian wallet is not connected');
		}
		const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
		const chainSymbol: ChainSymbolEnum = chainControl.value;

		if (!chainSymbol) {
			chainControl.setValue(ChainSymbolEnum.aptos);
		}

		if (!this.collectionFactoryContractAddress) {
			this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos];
		}
		const mintPrice: number = Number(this.collectionForm.get('mintPrice').value);

		this.currentStep.title = 'Confirm';
		this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';

		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}
		const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;

		if (this.nftSourceType === 'file' && !areNFTsUploaded) {
			this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
			throw new Error('Not all NFTs were uploaded to IPFS');
		}

		const royalty: number = this.collectionForm.get('royalty').value;
		const basisPoints: number = 100;
		const isCustomFirstTokenId: boolean = this.nftSourceType === 'ipfs' && this.collectionForm.get('isCustomFirstTokenId').value;
		const payload = [
			this.collectionForm.get('collectionName').value,
			this.collectionForm.get('description').value,
			this.collectionMetadataIpfsCID || '',
			this.getTokensURI(),
			this.getValidatedMintPrice(),
			this.getDateFromTimestamp(),
			this.getDateToTimestamp(),
			this.getTokenSupply(),
			royalty ? (royalty * basisPoints) : 0,
			isCustomFirstTokenId ? 1 : 0,
			0, // Custom seed
		];

		try {
			const txHash: string = await this.aptosMartianWalletService.sendTransaction(connectedWalletDetails.address, {
				function: environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos].replace(
					'{moduleName}',
					environment.aptosSpecific.versionToModuleName[environment.currentTokenFactoryVersion[ChainSymbolEnum.aptos]],
				),
				arguments: payload,
				type_arguments: [],
			});

			this.saveCreateTxInDatabase();
			await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
			this.currentStep.title = 'Waiting for transaction confirmation';
			this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';

			try {
				const txResult: IAptosTransactionDetails = await this.aptosMartianWalletService.getTransaction(txHash);

				if (!txResult.success) {
					throw new Error('Transaction has failed');
				}
				this.createTxResult = {
					txHash,
					status: 'success',
				};
				// Save as CREATED / ERROR because Aptos is currently only Isolated Chain
				this.saveCollectionInDatabase(CollectionStatusEnum.CREATED);
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.currentStep.title = 'Success! 🥳';
				this.currentStep.subtitle = 'Collection will be visible in a moment.';
				this.currentStep.status = StepperStatusEnum.COMPLETE;
				this.notificationsService.success('Created', 'Collection will be visible in your profile');
				setTimeout(() => {
					this.closeStepper();
					setTimeout(() => {
						this.goToUserCollections(chainSymbol);
					}, 500);
				}, 500);
			} catch (e) {
				console.error(e);
				this.currentStep.title = 'Could not confirm the transaction';
				this.currentStep.subtitle = 'Please, check if your collection was created.';
				this.currentStep.status = StepperStatusEnum.ERROR;
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.closeStepper(3000);
			}
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Could not create the collection');
		}
	}

	private async createAptosCollectionUsingPetra(): Promise<void> {
		const connectedWalletDetails = await this.web3Service.getConnectedWallet();
		const walletProvider: WalletProviderEnum = connectedWalletDetails.provider;

		this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.APT);
		this.selectedAssetName = IsolatedAssetEnum.APT;

		if (walletProvider !== WalletProviderEnum.APTOS_PETRA) {
			this.notificationsService.error('Connect your Petra wallet');
			throw new Error('Petra wallet is not connected');
		}
		const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
		const chainSymbol: ChainSymbolEnum = chainControl.value;

		if (!chainSymbol) {
			chainControl.setValue(ChainSymbolEnum.aptos);
		}

		if (!this.collectionFactoryContractAddress) {
			this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos];
		}
		this.currentStep.title = 'Confirm';
		this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';

		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}
		const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;

		if (this.nftSourceType === 'file' && !areNFTsUploaded) {
			this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
			throw new Error('Not all NFTs were uploaded to IPFS');
		}
		const royalty: number = this.collectionForm.get('royalty').value;
		const basisPoints: number = 100;
		const isCustomFirstTokenId: boolean = this.nftSourceType === 'ipfs' && this.collectionForm.get('isCustomFirstTokenId').value;
		const payload = [
			this.collectionForm.get('collectionName').value,
			this.collectionForm.get('description').value,
			this.collectionMetadataIpfsCID || '',
			this.getTokensURI(),
			this.getValidatedMintPrice(),
			this.getDateFromTimestamp(),
			this.getDateToTimestamp(),
			this.getTokenSupply(),
			royalty ? (royalty * basisPoints) : 0,
			isCustomFirstTokenId ? 1 : 0,
			0, // Custom seed
		];

		try {
			const txDetails: Pick<IAptosTransactionDetails, 'hash' | 'success'> = await this.aptosPetraWalletService.sendTransaction({
				function: environment.collectionFactoryAddressesMap[ChainSymbolEnum.aptos].replace(
					'{moduleName}',
					environment.aptosSpecific.versionToModuleName[environment.currentTokenFactoryVersion[ChainSymbolEnum.aptos]],
				),
				arguments: payload,
				type_arguments: [],
			});
			const txHash: string = txDetails.hash;

			this.saveCreateTxInDatabase();
			await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
			this.currentStep.title = 'Waiting for transaction confirmation';
			this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';

			try {
				this.createTxResult = {
					txHash,
					status: 'success',
				};
				// Save as CREATED / ERROR because Aptos is currently only Isolated Chain
				this.saveCollectionInDatabase(CollectionStatusEnum.CREATED);
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.currentStep.title = 'Success! 🥳';
				this.currentStep.subtitle = 'Collection will be visible in a moment.';
				this.currentStep.status = StepperStatusEnum.COMPLETE;
				this.notificationsService.success('Created', 'Collection will be visible in your profile');
				setTimeout(() => {
					this.closeStepper();
					setTimeout(() => {
						this.goToUserCollections(chainSymbol);
					}, 500);
				}, 500);
			} catch (e) {
				console.error(e);
				this.currentStep.title = 'Could not confirm the transaction';
				this.currentStep.subtitle = 'Please, check if your collection was created.';
				this.currentStep.status = StepperStatusEnum.ERROR;
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.closeStepper(3000);
			}
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Could not create the collection');
		}
	}

	// private async createSuiCollectionUsingSuiet(): Promise<void> {
	// 	const connectedWalletDetails = await this.web3Service.getConnectedWallet();
	// 	const walletProvider: WalletProviderEnum = connectedWalletDetails.provider;
	//
	// 	this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.SUI);
	// 	this.selectedAssetName = IsolatedAssetEnum.SUI;
	//
	// 	if (walletProvider !== WalletProviderEnum.SUI_SUIET) {
	// 		this.notificationsService.error('Connect your Sui wallet');
	// 		throw new Error('Ethos wallet is not connected');
	// 	}
	// 	const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
	// 	const chainSymbol: ChainSymbolEnum = chainControl.value;
	//
	// 	if (!chainSymbol) {
	// 		chainControl.setValue(ChainSymbolEnum.sui);
	// 	}
	//
	// 	if (!this.collectionFactoryContractAddress) {
	// 		this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[ChainSymbolEnum.sui];
	// 	}
	// 	const mintPrice: number = this.getValidatedMintPrice();
	//
	// 	this.currentStep.title = 'Confirm';
	// 	this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';
	//
	// 	if (this.isCreatingCollectionCancelled$.getValue()) {
	// 		return;
	// 	}
	// 	const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;
	//
	// 	if (this.nftSourceType === 'file' && !areNFTsUploaded) {
	// 		this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
	// 		throw new Error('Not all NFTs were uploaded to IPFS');
	// 	}
	// 	const royalty: number = this.collectionForm.get('royalty').value;
	// 	const basisPoints: number = 100;
	// 	const isCustomFirstTokenId: boolean = this.nftSourceType === 'ipfs' && this.collectionForm.get('isCustomFirstTokenId').value;
	// 	const payload = [
	// 		environment.suiSpecific.objects.launchpadMetadata,
	// 		this.collectionForm.get('collectionName').value,
	// 		this.collectionForm.get('description').value,
	// 		this.collectionMetadataIpfsCID || '',
	// 		this.getTokensURI(),
	// 		mintPrice * (Math.pow(10, environment.suiSpecific.coins.SUI.decimals)),
	// 		this.getDateFromTimestamp(),
	// 		this.getDateToTimestamp(),
	// 		this.getTokenSupply(),
	// 		royalty ? (royalty * basisPoints) : 0,
	// 		isCustomFirstTokenId ? 1 : 0,
	// 	];
	//
	// 	try {
	// 		const txResponse: SuiTransactionResponse = await this.suiSuietWalletService.sendTransaction(
	// 			environment.suiSpecific.functions.createCollection,
	// 			payload,
	// 		);
	//
	// 		if (!txResponse?.certificate?.transactionDigest) {
	// 			throw new Error('Sui [Create] - No response');
	// 		}
	// 		const txHash: string = txResponse.certificate.transactionDigest;
	//
	// 		this.saveCreateTxInDatabase();
	// 		await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
	// 		this.currentStep.title = 'Waiting for transaction confirmation';
	// 		this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol
	// 			? 'Creating your collection...'
	// 			: 'Your collection will be created on the selected chain and visible in a few minutes';
	//
	// 		try {
	// 			if (txResponse?.effects?.status?.status !== 'success') {
	// 				this.notificationsService.error('Error', 'Transaction has failed');
	// 				this.saveCollectionInDatabase(CollectionStatusEnum.ERROR);
	// 				throw new Error('Transaction has failed');
	// 			}
	// 			this.createTxResult = {
	// 				txHash,
	// 				status: 'success',
	// 			};
	// 			this.saveCollectionInDatabase(CollectionStatusEnum.CREATED);
	// 			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	// 			this.currentStep.title = 'Success! 🥳';
	// 			this.currentStep.subtitle = 'Collection will be visible in a moment.';
	// 			this.currentStep.status = StepperStatusEnum.COMPLETE;
	// 			this.notificationsService.success('Created', 'Collection will be visible in your profile');
	// 			setTimeout(() => {
	// 				this.closeStepper();
	// 				setTimeout(() => {
	// 					this.goToUserCollections(chainSymbol);
	// 				}, 500);
	// 			}, 500);
	// 		} catch (e) {
	// 			console.error(e);
	// 			this.currentStep.title = 'Could not confirm the transaction';
	// 			this.currentStep.subtitle = 'Please, check if your collection was created.';
	// 			this.currentStep.status = StepperStatusEnum.ERROR;
	// 			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	// 			this.closeStepper(3000);
	// 		}
	// 	} catch (e) {
	// 		console.error(e);
	// 		this.notificationsService.error('Error', 'Could not create the collection');
	// 	}
	// }

	// private async createSuiCollectionUsingEthos(): Promise<void> {
	// 	const connectedWalletDetails = await this.web3Service.getConnectedWallet();
	// 	const walletProvider: WalletProviderEnum = connectedWalletDetails.provider;
	//
	// 	this.collectionForm.get('assetName').setValue(IsolatedAssetEnum.SUI);
	// 	this.selectedAssetName = IsolatedAssetEnum.SUI;
	//
	// 	if (walletProvider !== WalletProviderEnum.SUI_ETHOS) {
	// 		this.notificationsService.error('Connect your Sui Ethos wallet');
	// 		throw new Error('Ethos wallet is not connected');
	// 	}
	// 	const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
	// 	const chainSymbol: ChainSymbolEnum = chainControl.value;
	//
	// 	if (!chainSymbol) {
	// 		chainControl.setValue(ChainSymbolEnum.sui);
	// 	}
	//
	// 	if (!this.collectionFactoryContractAddress) {
	// 		this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[ChainSymbolEnum.sui];
	// 	}
	// 	const mintPrice: number = this.getValidatedMintPrice();
	//
	// 	this.currentStep.title = 'Confirm';
	// 	this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';
	//
	// 	if (this.isCreatingCollectionCancelled$.getValue()) {
	// 		return;
	// 	}
	// 	const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;
	//
	// 	if (this.nftSourceType === 'file' && !areNFTsUploaded) {
	// 		this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
	// 		throw new Error('Not all NFTs were uploaded to IPFS');
	// 	}
	//
	// 	const royalty: number = this.collectionForm.get('royalty').value;
	// 	const basisPoints: number = 100;
	// 	const isCustomFirstTokenId: boolean = this.nftSourceType === 'ipfs' && this.collectionForm.get('isCustomFirstTokenId').value;
	// 	const payload = [
	// 		environment.suiSpecific.objects.launchpadMetadata,
	// 		this.collectionForm.get('collectionName').value,
	// 		this.collectionForm.get('description').value,
	// 		this.collectionMetadataIpfsCID || '',
	// 		this.getTokensURI(),
	// 		mintPrice * (Math.pow(10, environment.suiSpecific.coins.SUI.decimals)),
	// 		this.getDateFromTimestamp(),
	// 		this.getDateToTimestamp(),
	// 		this.getTokenSupply(),
	// 		royalty ? (royalty * basisPoints) : 0,
	// 		isCustomFirstTokenId ? 1 : 0,
	// 	];
	//
	// 	try {
	// 		const txResponse: SuiTransactionResponse = await this.suiEthosWalletService.sendTransaction(
	// 			environment.suiSpecific.functions.createCollection,
	// 			payload,
	// 		);
	//
	// 		if (!txResponse?.certificate?.transactionDigest) {
	// 			throw new Error('Sui [Create] - No response');
	// 		}
	// 		const txHash: string = txResponse.certificate.transactionDigest;
	//
	// 		this.saveCreateTxInDatabase();
	// 		await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
	// 		this.currentStep.title = 'Waiting for transaction confirmation';
	// 		this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';
	//
	// 		try {
	// 			if (txResponse?.effects?.status?.status !== 'success') {
	// 				this.notificationsService.error('Error', 'Transaction has failed');
	// 				this.saveCollectionInDatabase(CollectionStatusEnum.ERROR);
	// 				throw new Error('Transaction has failed');
	// 			}
	// 			this.createTxResult = {
	// 				txHash,
	// 				status: 'success',
	// 			};
	// 			this.saveCollectionInDatabase(CollectionStatusEnum.CREATED);
	// 			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	// 			this.currentStep.title = 'Success! 🥳';
	// 			this.currentStep.subtitle = 'Collection will be visible in a moment.';
	// 			this.currentStep.status = StepperStatusEnum.COMPLETE;
	// 			this.notificationsService.success('Created', 'Collection will be visible in your profile');
	// 			setTimeout(() => {
	// 				this.closeStepper();
	// 				setTimeout(() => {
	// 					this.goToUserCollections(chainSymbol);
	// 				}, 500);
	// 			}, 500);
	// 		} catch (e) {
	// 			console.error(e);
	// 			this.currentStep.title = 'Could not confirm the transaction';
	// 			this.currentStep.subtitle = 'Please, check if your collection was created.';
	// 			this.currentStep.status = StepperStatusEnum.ERROR;
	// 			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	// 			this.closeStepper(3000);
	// 		}
	// 	} catch (e) {
	// 		console.error(e);
	// 		this.notificationsService.error('Error', 'Could not create the collection');
	// 	}
	// }

	private async createEVMCollection(): Promise<void> {
		let web3Provider: Web3Provider;

		try {
			if (!this.web3Service.isWalletConnected()) {
				await this.web3Service.connectEVMWalletWithModal();
			}

			if (this.walletCheckOnMint) {
				clearInterval(this.walletCheckOnMint);
			}

			web3Provider = this.web3Service.evmProvider$.getValue();
			console.log(typeof web3Provider, web3Provider instanceof Web3Provider);
		} catch (e) {
			this.notificationsService.error('Canceled', 'Please, connect your Web3 wallet');
			console.error(e);
			this.currentStep.title = 'Couldn\'t connect your wallet';
			this.currentStep.subtitle = 'Don\'t close this window! Connect to your wallet now. We\'ll check in 15 seconds';
			this.currentStep.status = StepperStatusEnum.ERROR;

			this.walletCheckOnMint = setInterval(() => {
				this.create();
			}, 15000);

			return;
		}
		const signer: JsonRpcSigner = web3Provider.getSigner();
		const chainControl: AbstractControl = this.collectionForm.get('chainSymbol');
		const chainSymbol: ChainSymbolEnum = chainControl.value;

		if (!chainSymbol) {
			chainControl.setValue(this.connectedChainSymbol || ChainSymbolEnum.eth);
		}

		if (!chainSymbol) {
			this.collectionForm.get('chainSymbol').setValue(this.connectedChainSymbol);
		}

		if (!this.collectionFactoryContractAddress) {
			this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[this.connectedChainSymbol];
		}

		const contract = new ethers.Contract(
			this.collectionFactoryContractAddress,
			this.collectionFactoryContractAbi,
			signer,
		);

		try {
			let estimatedFees: IEstimatedFees;

			if (chainSymbol !== this.connectedChainSymbol) {
				try {
					this.currentStep.subtitle = 'You\'ll be able to confirm transaction in a moment';
					const encodedFnData: string = this.web3Service.encodePayload(
						[
							chainSymbol,
							this.collectionForm.get('collectionName').value,
							this.collectionMetadataIpfsCID || '',
							this.getValidatedMintPrice(),
							this.selectedAssetName,
							this.getDateFromTimestamp(),
							this.getDateToTimestamp(),
							this.getTokensURI(),
							this.nfts.length,
							environment.chainSymbolToCreateCollectionGas[chainSymbol],
							0,
							this.userAddress,
						],
						['string', 'string', 'string', 'uint', 'string', 'uint', 'uint', 'string', 'uint', 'uint', 'uint', 'address'],
					);
					const encodedPayload: string = this.web3Service.encodePayload(
						[
							chainSymbol,
							this.userAddress || '0xMock',
							encodedFnData,
							environment.chainSymbolToCreateCollectionGas[chainSymbol],
							this.userAddress || '0xMock',
							chainSymbol,
							this.userAddress || '0xMock',
							0,
						],
						['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
					);

					estimatedFees = await this.omnichainService.getEstimatedFees(
						chainSymbol,
						encodedPayload,
						OmnichainActionTypeEnum.CREATE_COLLECTION,
					);

					if (this.userBalance !== undefined && this.userBalance.lt(estimatedFees.fee)) {
						const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
						const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
						const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

						this.notificationsService.error('Insufficient funds for transaction', tipMessage);
						this.currentStep.title = 'Insufficient funds for transaction';
						this.currentStep.subtitle = `${tipMessage}. Connected network can be congested now.`;
						this.currentStep.status = StepperStatusEnum.ERROR;

						this.closeStepper(10000);
						return;
					}
				} catch (e) {
					console.error(e);
					if (e?.message?.includes('aborted (timeout')) {
						// Ignore such error
					} else {
						this.notificationsService.error('Error', 'Cannot estimate transaction fee');
						this.closeStepper(3000);
					}
				}
			}
			this.currentStep.title = 'Confirm';
			this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';

			if (this.isCreatingCollectionCancelled$.getValue()) {
				return;
			}
			const areNFTsUploaded: boolean = this.nfts.length === this.uploadedCount || !this.isNFTFileChanged;

			if (this.nftSourceType === 'file' && !areNFTsUploaded) {
				this.notificationsService.error('Error', 'Not all NFTs were uploaded to IPFS');
				throw new Error('Not all NFTs were uploaded to IPFS');
			}

			if (chainSymbol === this.connectedChainSymbol) {
				this.createTx = await contract.create([
					chainSymbol,
					this.collectionForm.get('collectionName').value,
					this.collectionMetadataIpfsCID || '',
					this.getValidatedMintPrice(),
					this.selectedAssetName,
					this.getDateFromTimestamp(),
					this.getDateToTimestamp(),
					this.getTokensURI(),
					this.nfts.length,
					environment.chainSymbolToCreateCollectionGas[chainSymbol],
					0,
				]);
			} else {
				if (this.isIsolatedChain(this.connectedChainSymbol)) {
					this.notificationsService.error('Isolated chain cannot perform cross-chain transactions');
					throw new Error('Isolated chain cannot perform cross-chain transactions');
				}

				this.createTx = await contract.create(
					[
						chainSymbol,
						this.collectionForm.get('collectionName').value,
						this.collectionMetadataIpfsCID || '',
						this.getValidatedMintPrice(),
						this.selectedAssetName,
						this.getDateFromTimestamp(),
						this.getDateToTimestamp(),
						this.getTokensURI(),
						this.nfts.length,
						environment.chainSymbolToCreateCollectionGas[chainSymbol],
						estimatedFees.redirectFee,
					],
					{value: estimatedFees.fee},
				);
			}
			this.saveCreateTxInDatabase();
			await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
			this.currentStep.title = 'Waiting for transaction confirmation';
			this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';

			this.createTx.wait().then((txResult: ContractReceipt) => {
				this.createTxResult = {
					txHash: txResult.transactionHash,
					status: txResult.status === 1 ? 'success' : 'error',
				};
				this.saveCollectionInDatabase(CollectionStatusEnum.CREATING);
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.currentStep.title = 'Success! 🥳';
				this.currentStep.subtitle = 'Collection will be visible in a few minutes.';
				this.currentStep.status = StepperStatusEnum.COMPLETE;
				this.notificationsService.success('Created', 'Collection will be visible in a few minutes');
				setTimeout(() => {
					this.closeStepper();
					setTimeout(() => {
						this.goToUserCollections(chainSymbol);
					}, 500);
				}, 500);
			}).catch((e) => {
				console.error(e);
				this.currentStep.title = 'Could not confirm the transaction';
				this.currentStep.subtitle = 'Please, check if your collection was created.';
				this.currentStep.status = StepperStatusEnum.ERROR;
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.closeStepper(3000);
			});
		} catch (e) {
			if (e?.message?.includes('underlying network changed')) {
				console.error(e);
				this.notificationsService.error('Error', 'You\'ve changed network during the process');
				setTimeout(() => {
					this.closeStepper();
					window.location.reload();
				}, 2500);

				return;
			}

			if (e?.data?.message?.includes('insufficient funds for gas * price + value')) {
				console.error(e);
				this.notificationsService.error('Insufficient funds for transaction fees', 'Fund your wallet with more native asset for the tx fees');
				setTimeout(() => {
					this.closeStepper();
				}, 2500);

				return;
			}

			this.closeStepper();
			console.error(e);

			this.notificationsService.error('Transaction Error', 'Unable to create. Connected network can be congested');

			return;
		}
	}

	private getStepsCount(): number {
		return !!this.file ? 3 : (this.isMetadata ? 2 : 1);
	}

	private buildSteps(): void {
		const stepsCount: number = this.getStepsCount();

		if (stepsCount === 1) {
			this.stepper[0] = this.getFinalStepInitialConfig();
			this.stepper[0].isActive = true;
			return;
		}

		if (stepsCount === 2) {
			this.stepper[0] = this.getMetadataStepInitialConfig();
			this.stepper[0].isActive = true;
			this.stepper[1] = this.getFinalStepInitialConfig();
			return;
		}

		if (stepsCount === 3) {
			this.stepper[0] = this.getFileStepInitialConfig();
			this.stepper[1] = this.getMetadataStepInitialConfig();
			this.stepper[2] = this.getFinalStepInitialConfig();
			return;
		}
	}

	private getFileStepInitialConfig(): IStepperStepConfig {
		return {
			title: 'Uploading file to IPFS',
			status: StepperStatusEnum.PENDING,
			iconText: 'File upload',
			isActive: true,
		};
	}

	private getMetadataStepInitialConfig(): IStepperStepConfig {
		return {
			title: `Saving ${this.nfts.length ? 'NFTs and ' : ''}metadata in IPFS`,
			status: StepperStatusEnum.PENDING,
			iconText: `Save ${this.nfts.length ? 'NFTs ' : ''}metadata`,
			isActive: false,
		};
	}

	private getFinalStepInitialConfig(): IStepperStepConfig {
		return {
			title: 'Almost ready',
			status: StepperStatusEnum.PENDING,
			iconText: 'Confirm',
			isActive: false,
		};
	}

	private mapChainSymbolToName(chainSymbol: ChainSymbolEnum): SupportedChainNameEnum {
		switch (chainSymbol) {
			case ChainSymbolEnum.ftm:
				return SupportedChainNameEnum.Fantom;
			case ChainSymbolEnum.avax:
				return SupportedChainNameEnum.Avalanche;
			case ChainSymbolEnum.arb:
				return SupportedChainNameEnum.Arbitrum;
			case ChainSymbolEnum.bsc:
				return SupportedChainNameEnum.BSC;
			case ChainSymbolEnum.optimism:
				return SupportedChainNameEnum.Optimism;
			case ChainSymbolEnum.polygon:
				return SupportedChainNameEnum.Polygon;
			case ChainSymbolEnum.moonbeam:
				return SupportedChainNameEnum.Moonbeam;
			case ChainSymbolEnum.harmony:
				return SupportedChainNameEnum.Harmony;
			case ChainSymbolEnum.evmos:
				return SupportedChainNameEnum.Evmos;
			case ChainSymbolEnum.gate:
				return SupportedChainNameEnum.Gate;
			case ChainSymbolEnum.oasis:
				return SupportedChainNameEnum.Oasis;
			case ChainSymbolEnum.aptos:
				return SupportedChainNameEnum.Aptos;
			case ChainSymbolEnum.sui:
				return SupportedChainNameEnum.Sui;
			case ChainSymbolEnum.eth:
			default:
				return SupportedChainNameEnum.Ethereum;
		}
	}

	private closeStepper(timeout?: number): void {
		setTimeout(() => {
			const closeBtn: HTMLButtonElement = this.createStepperCloseBtn.nativeElement;

			if (closeBtn) {
				closeBtn.click();
			}
			document.querySelector('#confirmCreateStepper').classList.add('hidden');
		}, timeout || 0);
		this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	}

	private async goToUserCollections(chainSymbol: ChainSymbolEnum): Promise<void> {
		const queryParams: Params = {
			chainSymbol,
			metadataURI: this.collectionMetadataIpfsCID,
			userAddress: this.userAddress || await this.web3Service.getUserAddress(),
		};

		if (this.createdCollectionAddress) {
			queryParams.collectionAddress = this.createdCollectionAddress;
		}

		this.uiLoaderService.stopAll();
		this.closeStepper(0);
		this.router.navigate(
			['/user'],
			{queryParams}
		);
	}

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol) || this.isIsolatedChain(chainSymbol);
	}

	private compressCollectionImageAndSaveInIpfs(type: 'background' | 'square'): void {
		const options: ICompressImageOptions = {
			maxSizeMB: 1,
			maxWidthOrHeight: 2200,
			useWebWorker: true,
			alwaysKeepResolution: true,
		};

		imageCompression(this.file, options)
			.then((compressedFile) => {
				this.ipfsService.sendFile(compressedFile).then((result: AddResult) => {
					if (type === 'background') {
						this.smBackgroundImageIpfsHash = result.path;
						this.firebaseStorageService.uploadFile(`${this.smBackgroundImageIpfsHash}_${this.id}`, compressedFile).then((res) => {
							this.collectionFileStoragePath = res.ref.fullPath;
							this.closeCollectionEditModal();
						});
					} else if (type === 'square') {
						this.smSquareImageIpfsHash = result.path;
						this.firebaseStorageService.uploadFile(`${this.smSquareImageIpfsHash}_${this.id}`, compressedFile).then((res) => {
							this.squareFileStoragePath = res.ref.fullPath;
						});
					}
				});
			})
			.catch((error) => {
				console.error(error.message);
			});
	}

	private async compressNFTFileAndSaveInIpfs(nft: INFTDetails): Promise<void> {
		const options: ICompressImageOptions = {
			maxSizeMB: 1,
			maxWidthOrHeight: 1600,
			useWebWorker: true,
			alwaysKeepResolution: true,
		};
		const fileIpfsResult: AddResult = await this.ipfsService.sendFile(nft.file);

		nft.fileIpfsHash = fileIpfsResult.path;

		try {
			const nftFile: File = this.selectedContentType === 'ART' ? await imageCompression(nft.file, options) : nft.file;

			try {
				try {
					nft.smFilePath = `sm_${nft.tokenId}_${this.id}`;
					const res: UploadResult = await this.firebaseStorageService.uploadFile(`sm_${nft.tokenId}_${this.id}`, nftFile);

					nft.fileStoragePath = res.ref.fullPath;
				} catch (e) {
					console.error(`nft ${JSON.stringify(nft)} firebaseStorageService.uploadIpfsFile() error`);
					console.error(e);
				}
			} catch (e) {
				console.error(`nft ${JSON.stringify(nft)} ipfsService.sendFile() error`);
				console.error(e);
			}
		} catch (e) {
			console.error(`nft ${JSON.stringify(nft)} imageCompression() error`);
			console.error(e);
		}
	}

	private async saveNFTsMetadataInIPFS(): Promise<AddResult> {
		const collectionName: string = this.collectionForm.get('collectionName').value;
		const externalLink: string = this.collectionForm.get('externalLink').value;

		for (const savedNFT of this.nfts) {
			this.nftsMetadata.push({
				name: savedNFT.name || (collectionName?.length >= 3 ? `${collectionName} #${savedNFT.tokenId}` : `#${savedNFT.tokenId}`),
				image: savedNFT.file.type.match(/image\/*/) ? `https://omnisea.infura-ipfs.io/ipfs/${savedNFT.fileIpfsHash}` : '',
				fileURI: `https://omnisea.infura-ipfs.io/ipfs/${savedNFT.fileIpfsHash}`,
				animation_url: !savedNFT.file.type.match(/image\/*/) ? `https://omnisea.infura-ipfs.io/ipfs/${savedNFT.fileIpfsHash}` : '',
				external_link: externalLink || '',
				attributes: savedNFT.attributes || {}, // TODO: (must) check if that's correct - not an array
				description: this.collectionForm.get('description').value || '',
			});
		}

		return await this.ipfsService.saveAllMetadata<INFTMetadata>(this.nftsMetadata);
	}

	private updateCollectionPreview(): void {
		let assetName: SupportedAssetEnum | IsolatedAssetEnum;

		switch (this.connectedChainSymbol) {
			case ChainSymbolEnum.aptos:
				assetName = IsolatedAssetEnum.APT;
				break;
			case ChainSymbolEnum.sui:
				assetName = IsolatedAssetEnum.SUI;
				break;
			default:
				assetName = SupportedAssetEnum[this.collectionForm.get('assetName').value || SupportedAssetEnum.USDC];
		}

		this.collectionPreview = {
			collectionName: this.collectionForm.get('collectionName').value,
			createdAt: moment().unix(),
			totalMinted: 0,
			totalSupply: this.nftSourceType === 'ipfs' ? this.collectionForm.get('totalSupply').value : this.nfts.length,
			tokensURI: this.collectionForm.get('tokensURI').value,
			isCustomFirstTokenId: this.collectionForm.get('isCustomFirstTokenId').value,
			mintPrice: this.collectionForm.get('mintPrice').value,
			metadata: {
				name: this.collectionForm.get('collectionName').value,
				description: this.collectionForm.get('description').value,
				attributes: [],
				external_link: this.collectionForm.get('externalLink').value || '',
			},
			assetName,
			creator: this.userAddress || '',
			chainSymbol: this.collectionForm.get('chainSymbol').value,
			srcChain: this.connectedChainSymbol || ChainSymbolEnum.eth,
			contentType: ContentTypeEnum[this.collectionForm.get('contentType').value],
			dropFrom: this.getDateFromTimestamp(),
			dropTo: this.getDateToTimestamp(),
			licence: this.collectionForm.get('licence').value,
			isPublic: this.collectionForm.get('isPublic').value,
			royalties: this.collectionForm.get('royalty').value,
			nftSourceType: this.nftSourceType,
		};

		this.onUpdatePreview$.next();
	}

	private getTokensURI(): string {
		return this.nftSourceType === 'ipfs'
			? (this.collectionForm.get('tokensURI').value || '')
			: (this.tokensMetadataIpfsCID || '');
	}

	private getTokenSupply(): number {
		return this.nftSourceType === 'file' ? this.nfts.length : this.collectionForm.get('totalSupply').value;
	}

	private getCurrentTokenFactoryVersion(): number {
		return environment.currentTokenFactoryVersion[this.collectionForm.get('chainSymbol').value]
			|| environment.currentTokenFactoryVersion[ChainSymbolEnum.eth];
	}
}
