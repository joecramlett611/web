import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { TermsComponent } from './components/terms/terms.component';
import { TermsRoutingModule } from './terms-routing.module';

@NgModule({
	declarations: [
		TermsComponent,
	],
	imports: [
		TermsRoutingModule,
		CommonModule,
		SharedModule,
	],
	exports: [
		TermsComponent,
	]
})
export class TermsModule {
}
