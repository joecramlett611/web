import { Routes } from '@angular/router';

export const termsRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./terms.module').then(m => m.TermsModule),
		data: { preload: false },
	},
];
