import { Injectable } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { FirebaseApp, FirebaseOptions } from '@firebase/app';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import FirestoreService from './firestore.service';
import FirebaseDatabaseService from './firebase-database.service';
import CollectionsDataService from '../../chain-data/services/collections-data.service';
import UserService from '../../user/services/user.service';
import FirebaseStorageService from './firebase-storage.service';
const { initializeAppCheck, ReCaptchaV3Provider } = require('firebase/app-check');

@Injectable({
	providedIn: 'root',
})
export default class FirebaseService {
	public readonly firebaseApp$: BehaviorSubject<FirebaseApp> = new BehaviorSubject<FirebaseApp>(undefined);
	public readonly firebaseCommunityApp$: BehaviorSubject<FirebaseApp> = new BehaviorSubject<FirebaseApp>(undefined);

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly userService: UserService,
		private readonly firebaseStorageService: FirebaseStorageService,
	) {
		this.firebaseApp$.subscribe((firebaseApp: FirebaseApp) => {
			if (!firebaseApp) {
				return;
			}

			this.firebaseDatabaseService.database$.subscribe((db) => {
				if (db) {
					try {
						this.collectionsDataService.run();
						this.userService.run();
					} catch (e) {
						console.error(e);
					}
				}
			});

			this.firestoreService.setFirestore(firebaseApp);
			this.firebaseDatabaseService.setDatabase(firebaseApp);
			this.firebaseStorageService.setStorage(firebaseApp);
		});
	}

	public createFirebaseApp(): void {
		const firebaseConfig: FirebaseOptions = this.getFirebaseConfig();
		const firebaseApp: FirebaseApp = initializeApp(firebaseConfig);

		if (!environment.production) {
			// @ts-ignore
			self.FIREBASE_APPCHECK_DEBUG_TOKEN = environment.firebase.useDebugToken;
		}
		const appCheck = initializeAppCheck(firebaseApp, {
			provider: new ReCaptchaV3Provider(environment.firebase.appCheckToken),
			isTokenAutoRefreshEnabled: true,
		});

		this.firebaseApp$.next(appCheck.app);
	}

	public createFirebaseCommunityApp(): void {
		const firebaseConfig: FirebaseOptions = this.getFirebaseConfig();
		firebaseConfig.databaseURL = environment.firebase.communityDatabaseURL;
		firebaseConfig.storageBucket = undefined;
		const firebaseApp: FirebaseApp = initializeApp(firebaseConfig,  'community');

		if (!environment.production) {
			// @ts-ignore
			self.FIREBASE_APPCHECK_DEBUG_TOKEN = environment.firebase.useDebugToken;
		}
		const appCheck = initializeAppCheck(firebaseApp, {
			provider: new ReCaptchaV3Provider(environment.firebase.appCheckToken),
			isTokenAutoRefreshEnabled: true,
		});

		this.firebaseCommunityApp$.next(appCheck.app);
	}

	private getFirebaseConfig(): FirebaseOptions {
		return {
			apiKey: environment.firebase.apiKey,
			authDomain: environment.firebase.authDomain,
			projectId: environment.firebase.projectId,
			storageBucket: environment.firebase.storageBucket,
			databaseURL: environment.firebase.databaseURL,
			messagingSenderId: environment.firebase.messagingSenderId,
			appId: environment.firebase.appId,
			measurementId: environment.firebase.measurementId,
		};
	}
}
