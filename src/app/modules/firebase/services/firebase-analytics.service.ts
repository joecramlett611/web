import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FirebaseApp } from '@firebase/app';
import { getAnalytics, logEvent } from 'firebase/analytics';
import { Analytics, EventParams } from '@firebase/analytics';

@Injectable({
	providedIn: 'root',
})
export default class FirebaseAnalyticsService {
	public readonly analytics$: BehaviorSubject<Analytics> = new BehaviorSubject<Analytics>(undefined);

	public setAnalytics(firebaseApp: FirebaseApp): void {
		const analytics: Analytics = getAnalytics(firebaseApp);

		this.analytics$.next(analytics);
	}

	public logEvent(eventName: string, eventParams?: EventParams): void {
		const analytics: Analytics = this.analytics$.getValue();

		if (!analytics) {
			return;
		}
		logEvent(analytics, eventName, eventParams);
	}
}
