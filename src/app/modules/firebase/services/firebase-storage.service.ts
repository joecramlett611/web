import { Injectable } from '@angular/core';
import { getStorage } from 'firebase/storage';
import { BehaviorSubject } from 'rxjs';
import { FirebaseStorage, ref, StorageReference, uploadBytes, UploadResult, getDownloadURL, getMetadata } from '@firebase/storage';
import { FirebaseApp } from '@firebase/app';
import firebase from 'firebase/compat';
import FullMetadata = firebase.storage.FullMetadata;

@Injectable({
	providedIn: 'root',
})
export default class FirebaseStorageService {
	public readonly storage$: BehaviorSubject<FirebaseStorage> = new BehaviorSubject<FirebaseStorage>(undefined);

	public setStorage(firebaseApp: FirebaseApp): void {
		this.storage$.next(getStorage(firebaseApp));
	}

	public async uploadFile(hash: string, file: File): Promise<UploadResult> {
		const storage: FirebaseStorage = this.storage$.getValue();

		if (!storage) {
			throw new Error(`No FirebaseStorage on ${hash} upload`);
		}
		const storageRef: StorageReference = ref(storage, hash);

		return await uploadBytes(storageRef, file);
	}

	public async getIpfsFileURL(hash: string): Promise<string> {
		const storage: FirebaseStorage = this.storage$.getValue();

		if (!storage) {
			console.error(`No FirebaseStorage on: ${hash} getIpfsFileURL`);
			return `https://omnisea.infura-ipfs.io/ipfs/${hash.split('_')[0]}`;
		}

		try {
			return await getDownloadURL(ref(storage, hash));
		} catch (e) {
			return `https://omnisea.infura-ipfs.io/ipfs/${hash.split('_')[0]}`;
		}
	}

	public async getFileURL(fileName: string): Promise<string> {
		const storage: FirebaseStorage = this.storage$.getValue();

		if (!storage) {
			console.error(`No FirebaseStorage on: ${fileName} getDownloadURL`);
			return;
		}

		try {
			return await getDownloadURL(ref(storage, fileName));
		} catch (e) {
			console.error(`Error on: ${fileName} getDownloadURL`);
		}
	}

	public async getFileMetadata(fileName: string): Promise<FullMetadata> {
		const storage: FirebaseStorage = this.storage$.getValue();

		if (!storage) {
			console.error(`No FirebaseStorage on: ${fileName} getFileMetadata`);
			return;
		}

		try {
			return await getMetadata(ref(storage, fileName));
		} catch (e) {
			console.error(`Error on: ${fileName} getDownloadURL`);
		}
	}
}
