import { Injectable } from '@angular/core';
import { create } from 'ipfs-http-client';
import { BehaviorSubject } from 'rxjs';
import { IPFSHTTPClient } from 'ipfs-http-client/types/src/types';
import { AddResult } from 'ipfs-core-types/types/src/root';
import { environment } from '../../../../environments/environment';
import { ToFile } from 'ipfs-core-types/src/utils';
import { INFTMetadata } from '../../nfts/interfaces/nft-metadata.interface';

@Injectable({
	providedIn: 'root',
})
export default class IpfsService {
	public ipfsClient$: BehaviorSubject<IPFSHTTPClient> = new BehaviorSubject<IPFSHTTPClient>(undefined);
	private readonly ipfsRpcAuthHeader: string;

	public constructor() {
		this.ipfsRpcAuthHeader = this.composeIpfsAuthHeader();

		if (!this.ipfsClient$.getValue()) {
			this.connect();
		}
	}

	public connect(): void {
		this.ipfsClient$.next(create({
			host: environment.ipfsInfuraApiHost,
			port: environment.ipfsInfuraApiPort,
			protocol: environment.ipfsInfuraApiProtocol,
			headers: {
				authorization: this.ipfsRpcAuthHeader,
			},
		}));
	}

	public getClient(): IPFSHTTPClient {
		return this.ipfsClient$.getValue();
	}

	public sendFile(file: File): Promise<AddResult> {
		return this.getClient().add(
			file,
			{
				progress: (bytes, path) => {
					// console.log(bytes + ' / ' + file.size);
				},
			});
	}

	public saveMetadata(metadata: string): Promise<AddResult> {
		return this.getClient().add(metadata);
	}

	public async saveAllMetadata<T>(metadataDtos: Array<T>): Promise<AddResult> {
		const files: ToFile[] = [];

		for (let i = 0; i < metadataDtos.length; i++) {
			files.push({
				path: `/${i}.json`,
				content: JSON.stringify(metadataDtos[i]),
			});
		}
		const results: AddResult[] = [];

		for await (const result of this.getClient().addAll(files, {wrapWithDirectory: true})) {
			results.push(result);
		}

		return results[results.length - 1];
	}

	public get(path: string): AsyncIterable<Uint8Array> {
		return this.getClient().get(path);
	}

	public read(path: string): AsyncIterable<Uint8Array> {
		return this.getClient().cat(path);
	}

	public ls(path: string) {
		return this.getClient().ls(path);
	}

	public async getNFTMetadataByCIDAndTokenId(ipfsCID: string, tokenId: number): Promise<INFTMetadata> {
		const response = await fetch(`https://omnisea.infura-ipfs.io/ipfs/${ipfsCID}/${tokenId}.json`);

		return await response.json();
	}

	private composeIpfsAuthHeader(): string {
		return 'Basic '
		+ Buffer.from(environment.ipfsInfuraProjectId + ':' + environment.ipfsInfuraSecret)
			.toString('base64');
	}
}
