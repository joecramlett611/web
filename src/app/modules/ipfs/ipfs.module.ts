import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import IpfsService from './services/ipfs.service';

@NgModule({
	providers: [
		IpfsService,
	],
	imports: [
		CommonModule,
	],
})
export class IpfsModule {
}
