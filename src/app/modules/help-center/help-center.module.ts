import { NgModule } from '@angular/core';
import { HelpCenterComponent } from './components/help-center/help-center.component';
import { HelpCenterRoutingModule } from './help-center-routing.module';
import { HelpCenterCategoryComponent } from './components/help-center-category/help-center-category.component';
import HelpCenterService from './services/help-center.service';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HelpCenterTopicComponent } from './components/help-center-topic/help-center-topic.component';
import { SupportFormComponent } from './components/support-form/support-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		HelpCenterComponent,
		HelpCenterCategoryComponent,
		HelpCenterTopicComponent,
		SupportFormComponent,
	],
	imports: [
		HelpCenterRoutingModule,
		CommonModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
	],
	providers: [
		HelpCenterService,
	],
	exports: [
		HelpCenterComponent,
		HelpCenterCategoryComponent,
		HelpCenterTopicComponent,
		SupportFormComponent,
	]
})
export class HelpCenterModule {
}
