import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import FirestoreService from '../../../firebase/services/firestore.service';
import NotificationsService from '../../../shared/services/notifications.service';
import { Router } from '@angular/router';
import Web3Service from '../../../web3/services/web3.service';

@Component({
	selector: 'app-support-form',
	templateUrl: './support-form.component.html',
	styleUrls: ['./support-form.component.scss']
})
export class SupportFormComponent implements OnInit, OnDestroy {
	public subscriptions: Subscription;
	public form: FormGroup;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly firestoreService: FirestoreService,
		private readonly notificationsService: NotificationsService,
		private readonly router: Router,
		private readonly web3Service: Web3Service,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();

		this.form = this.formBuilder.group({
			topic: ['', [Validators.required, Validators.maxLength(500)]],
			message: ['', [Validators.required, Validators.maxLength(2000)]],
			email: ['', [Validators.required, Validators.maxLength(250)]],
		});
	}

	public async onSubmit(): Promise<void> {
		if (this.form.invalid) {
			this.notificationsService.error(
				'Error',
				'Please, check the form fields. Message: max 2000 characters, topic: max 500, contact: max 250',
			);

			return;
		}

		await this.firestoreService.add(
			`supportTickets`,
			[],
			{
				...this.form.value,
				userAddress: await this.web3Service.getUserAddress() || null,
			},
		);
		this.notificationsService.success('Success', 'We will process your request as soon as possible.');
		this.router.navigate(['/help-center']);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}
}
