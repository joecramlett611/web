import { Component } from '@angular/core';
import { Params, Router } from '@angular/router';
import { HelpCenterCategoryEnum } from '../../enums/help-center-category.enum';

@Component({
	selector: 'app-help-center',
	templateUrl: './help-center.component.html',
	styleUrls: ['./help-center.component.scss']
})
export class HelpCenterComponent {
	constructor(private readonly router: Router) {
	}

	public goToCategory(category: string): void {
		const queryParams: Params = {
			category,
		};

		this.router.navigate(['/help-center/category'], { queryParams });
	}

	public goToSupport(): void {
		const queryParams: Params = {
			category: HelpCenterCategoryEnum.BASICS,
			topic: 'Support',
		};

		this.router.navigate(['/help-center/topic'], { queryParams });
	}
}
