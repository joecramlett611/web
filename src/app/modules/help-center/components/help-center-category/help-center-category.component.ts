import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { of, Subscription, switchMap } from 'rxjs';
import { HelpCenterCategoryEnum } from '../../enums/help-center-category.enum';
import { IHelpCenterTopic } from '../../interfaces/help-center-topic.interface';
import HelpCenterService from '../../services/help-center.service';

@Component({
	selector: 'app-help-center-category',
	templateUrl: './help-center-category.component.html',
	styleUrls: ['./help-center-category.component.scss']
})
export class HelpCenterCategoryComponent implements OnInit, OnDestroy {
	public subscriptions: Subscription;
	public topics: IHelpCenterTopic[];
	public faq: Array<{
		question: string;
		answer: string;
	}>;
	public isLoaded: boolean;
	private category: HelpCenterCategoryEnum;

	constructor(
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly helpCenterService: HelpCenterService,
	) {
	}

	public ngOnInit(): void {
		this.topics = [];
		this.subscriptions = new Subscription();

		this.subscriptions.add(this.route.queryParams.pipe(
			switchMap((params: Params) => {
				if (!params?.category) {
					const queryParams: Params = {
						category: HelpCenterCategoryEnum.BASICS,
					};

					this.router.navigate(['/help-center/category'], {queryParams});

					return;
				}
				this.category = params.category;
				this.faq = this.helpCenterService.categoriesFAQ[this.category];
				this.topics = [...this.helpCenterService.categoriesTopics[this.category]];

				return this.helpCenterService.files$;
			}),
		).subscribe((files) => {
			if (!files) {
				return;
			}

			for (const topic of this.topics) {
				topic.imagePath = files[topic.imageName];
			}

			this.isLoaded = true;
		}));
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public goToTopic(topicName: string): void {
		const queryParams: Params = {
			category: this.category,
			topic: topicName,
		};

		this.router.navigate(['/help-center/topic'], {queryParams});
	}

	public redirectTo(url: string): void {
		window.open(url, '_blank');
	}
}
