import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription, switchMap } from 'rxjs';
import { HelpCenterCategoryEnum } from '../../enums/help-center-category.enum';
import { IHelpCenterTopic } from '../../interfaces/help-center-topic.interface';
import HelpCenterService from '../../services/help-center.service';

@Component({
	selector: 'app-help-center-topic',
	templateUrl: './help-center-topic.component.html',
	styleUrls: ['./help-center-topic.component.scss']
})
export class HelpCenterTopicComponent implements OnInit, OnDestroy {
	public subscriptions: Subscription;
	public topic: IHelpCenterTopic;
	public category: HelpCenterCategoryEnum;
	public isLoaded: boolean;

	constructor(
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly helpCenterService: HelpCenterService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();

		this.subscriptions.add(this.route.queryParams.pipe(
			switchMap((params: Params) => {
				if (!params?.topic || !params.category) {
					const queryParams: Params = {
						category: HelpCenterCategoryEnum.BASICS,
					};

					this.router.navigate(['/help-center/category'], {queryParams});

					return;
				}
				this.topic = this.helpCenterService.categoriesTopics[params.category].find((topic) => topic.name === params.topic);
				this.category = params.category;

				return this.helpCenterService.files$;
			}),
		).subscribe((files) => {
			if (!files) {
				return;
			}

			this.topic.imagePath = files[this.topic.imageName];

			this.isLoaded = true;
		}));
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public goToCategory(): void {
		const queryParams: Params = {
			category: this.category,
		};

		this.router.navigate(['/help-center/category'], {queryParams});
	}
}
