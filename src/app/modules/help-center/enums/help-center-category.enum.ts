export enum HelpCenterCategoryEnum {
	BASICS = 'basics',
	MINTING = 'minting',
	CREATING = 'creating',
	PAYMENTS = 'payments',
	RESOURCES = 'resources',
	DEVELOPERS = 'developers',
}
