import { Injectable } from '@angular/core';
import { HelpCenterCategoryEnum } from '../enums/help-center-category.enum';
import { IHelpCenterTopic } from '../interfaces/help-center-topic.interface';
import StaticFilesService from '../../shared/services/static-files.service';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export default class HelpCenterService {
	public readonly categoriesTopics: Record<HelpCenterCategoryEnum, IHelpCenterTopic[]>;
	public readonly categoriesFAQ: Record<HelpCenterCategoryEnum, Array<{
		question: string;
		answer: string;
	}>>;
	public readonly files$: BehaviorSubject<Record<string, string>> = new BehaviorSubject<Record<string, string>>(undefined);

	constructor(
		private readonly staticFilesService: StaticFilesService,
	) {
		this.staticFilesService.getFiles([
			'abstract-1.png',
			'abstract-2.png',
			'abstract-3.png',
			'abstract-4.png',
			'abstract-5.png',
			'abstract-6.png',
			'abstract-7.png',
			'abstract-8.png',
		]).then((filePaths) => {
			this.files$.next(filePaths);
		});

		// @ts-ignore
		this.categoriesTopics = {
			[HelpCenterCategoryEnum.BASICS]: [
				{
					name: 'The official Omnisea user guide',
					imageName: 'abstract-1.png',
					description: 'Get familiar the platform, terminology, how to start minting, and learn about our advantages.',
					content: `<p class="text-lg leading-normal">
                    When creating Omnisea, user experience has always been our guiding principle.
                </p>
                <p>
                    Applying the Omnichain approach to NFTs is not only about playing with new, hot technology. At Omnisea, we believe that the use of these solutions will allow us to achieve incomparably better experiences and simplicity of interaction with NFT, which, as a result of the creation of an increasing number of qualitative blockchains, have been broken down into many unique, yet isolated ecosystems.
                </p>
                <p>
                    The connection of NFT worlds along with covering this issue with the abstraction layer is primarily aimed at making the user not at all think about something like the chain to which it is connected or the transfer of funds between blockchains. Just like today's mainstream Web2 applications, that do not require the user to know the programming languages used.
                </p>
                <p>
                    The following instructions for using the Omnisea should start with an introduction - what can you do here?
                </p>
                <h2 class="text-xl">Launching Omnichain Collection</h2>
                <p>
                    Creating a collection by Omnisea is really easy. Just do the following:
                </p>
<div>✅ First of all, connect your wallet.</div>
<div class="mt-4">✅ Then click "Launch" or go to <a href="https://www.omnisea.org/create" target="_blank" class="text-accent">omnisea.org/create</a>. Fill out the form with all the details about your collection. The required data is only the name and photo (cover) of the collection. In addition, you can tag properties such as description, date range from when to when NFTs can be minted, price per mint, link to external collection page, etc.</div>
<div class="mt-4">✔️<span class="text-sm">(Optional)</span> Moreover, you can add all NFTs with their files included in the collection.</div>
<div class="mt-4">✔️<span class="text-sm">(Optional)</span> Roll out the "advanced options" if you want to decide on which blockchain collection should be created, or make your collection initially private (hidden).</div>
<div class="mt-4">✅ Click the "Confirm" button. Wait for the collection's and NFTs' files to be uploaded to IPFS, and confirm the transaction.</div>

                <h2 class="text-xl">Omnichain Minting</h2>
				<p>
                    Minting between chains is even easier. Learn more about the process in the "Minting" section of this Help Center. If you want to mint NFT on e.g. Ethereum from for example Avalanche, let's do these steps:
                </p>
<div>✅ Connect your wallet.</div>
<div>✅ Go to the collection you want to mint from. You can find collections for example on the <a href="https://www.omnisea.org/collections" target="_blank" class="text-accent">Explore</a> page</div>
<div class="mt-4">✔️<span class="text-sm">(Note)</span> If the collection's price per mint is specified, make sure to have enough funds in your wallet.</div>
<div>✅ Wait for the "Mint" button to become active. Click on it and accept the minting transaction.</div>
<div class="mt-4">✔️<span class="text-sm">(Note)</span> If the "Mint" button is inactive take a look at minting date range (from-to), and the remaining (if the supply is not unlimited) count.</div>

<p>Minting while being on the same chain as the collection is instant. NFT should be visible in your Profile -> NFTs. Omnichain mint can take a maximum few minutes to complete on the target blockchain.</p>

<h2 class="text-xl">Create Profile</h2>
<p>Creating your Profile is a great way to personalize your look, and, if you are a creator, being displayed in a much more professional manner with all relevant information linked.</p>

<div>✅ Connect to your wallet.</div>
<div>✅ Go to the My Profile page.</div>
<div>✅ Click on the Edit Profile button.</div>
<div>✅ Enter a unique name for the profile.</div>
<div>✔️<span class="text-sm">(Optional)</span> If you want, enter other optional data, such as avatar photos, backgrounds, and social links.</div>
<div>✅ Click Confirm.</div>`,
				},
				{
					name: 'Web3 Wallets',
					imageName: 'abstract-2.png',
					description: 'Learn how to install your wallet, which wallets are supported on Omnisea, and how to make various transactions.',
					content: `<p class="text-lg leading-normal">
Learn how to install your wallet, which wallets are supported on Omnisea, and how to make various transactions.
</p>

<h2 class="text-xl">Supported Wallets</h2>
<div>Users can connect to Omnisea using <span class="font-bold">MetaMask or Torus</span> wallets.</div>
<div class="mt-4"><span class="font-bold">MetaMask</span> is the most popular Web3 wallet and works as a browser extension. It allows you to start your adventure with decentralized applications in minutes. It offers the possibility of storing cryptocurrencies and NFTs without an intermediary, exchange of assets (e.g. ETH to USDC or OSEA used within the platform), or signing of transactions, such as the creation of NFT or mint collections.</div>
<div class="mt-4">Once connected, <span class="font-bold">Torus</span> works like MetaMask, but offers other possibilities for wallet creation and connection to applications. The main advantage of Torrus Wallet is the ability to create a wallet with your Google, Facebook, Twitter, Discord, and other social accounts. Torus Wallet, unlike MetaMask, does not work as a browser extension, but when clicked, it becomes an integral part of the Omnisea application and allows you to use the wallet from any device, e.g. phone or tablet.</div>

<p>You can install MetaMask from your browser's extensions store. After installment, we recommend pinning it for better accessibility. Next, create your new account, or import existing account from a seed phrase or using private key. Don't forget to always store your seed phrase for recovery possibility - we recommend doing it on paper, outside of device connected to Internet.</p>
<p>With Torus Wallet you don't have to install anything. Just click on our "Connect" icon or button, and choose Torus. Next, follow displayed instructions. Using Torus you can create new account using your Web2 accounts like Google, Facebook, Twitter, Discord, etc. You can also access every Omnisea's feature from mobile using this wallet.</p>

<h2 class="text-xl">Funding your wallet</h2>
<div>There are many simple ways to fund your wallet. First of all, the MetaMask and Torus wallets have built-in options for buying cryptocurrencies using, among others credit card. If possible, we recommend buying USDC, as it is one of the assets supported by our platform. If you have a different currency such as ETH, you can convert it to USDC or OSEA using MetaMask or decentralized exchanges such as Uniswap.</div>
<div class="mt-4">Another proven, common way is to buy your first cryptocurrencies from exchanges such as Binance, Coinbase, etc., and then send them to your wallet's public address.</div>

<h2 class="text-xl">Networks</h2>
<p>At Omnisea we don't want you to worry about choosing blockchains or switching between networks. That's why we'll continuously add more and more supported chains.</p>
<p>Currently (Testnet), we support Ethereum, BNB Chain (BSC), Polygon, Avalanche, Arbitrum, Optimism, and Fantom networks. In the near future, we will be able to connect to many other blockchains, such as Solana or chains from the Cosmos ecosystem.</p>
<p>Remember that our end goal is to provide ability to stay on your favourite network, and still be able to interact with NFTs and users from all ecosystems (blockchains).</p>`
				},
				{
					name: 'Terminology',
					imageName: 'abstract-3.png',
					description: '',
					content: `<p class="text-lg leading-normal">
The world of NFT, Web3, and cryptocurrency in general, is full of words and markings that are worth reading in order to use decentralized applications comfortably and with confidence. At this point, we would like to introduce the meaning of terminology, in particular related to NFTs.
</p>
<p class="font-bold">This article will be updated on a regular basis and expanded with new meanings of selected phrases.</p>
<p>
<p><span class="font-bold">NFT Collection</span> - Simply put, it is a set of unique NFTs. It is a blueprint for the included tokens. In a more advanced approach, it is a contract compliant with a standard specified for a given blockchain (e.g. ERC-721 for an EVM-compatible network). The collection groups the tokens included in it into one set. Individual NFTs can have multiple owners. A collection, as a contract from which its NFTs are created, may set them certain rules, such as maximum supply, behavior during creation (e.g. charging for a mint), limits, e.g. a date range when a new NFT may within this collection be created (minted).</p>
<p class="mt-4"><span class="font-bold">NFT</span> - Short for Non-fungible token. Since you are here, you must have heard of him already. Unlike fungible tokens, such as USDC, which can be traded 1: 1, NFTs are created to represent a unique, unique value. It can be an artistic work, a right such as ownership, copyright, any set of data whose purpose is to distinguish it from other creations, with no 1: 1 relationship to another asset. NFT, as a contract on the blockchain network, may contain a set of unique data identifying and characterizing it - e.g. Token ID (as part of a collection) or a link to a resource containing more detailed data specific to this token. This link is a property of the given NFT called metadata URI. The URI indicates the address where the read metadata is located. They are most often preserved off-chain, so the collector must pay special attention to the method of preserving this data - we recommend the use of IPFS (explanation below). At Omnisea, we consider the use of IPFS an obligation rather than an option in the collection and NFT creation process. Every NFT created through Omnisea has metadata stored on IPFS.</p>
<p class="mt-4"><span class="font-bold">Mint</span> - A process (blockchain transaction) of creating a new NFT in the collection to which it belongs. Often the term is confused with NFT purchasing. Purchase / sale can only take place on an already existing (minted) NFT. In addition, often the word "mint" also appears in the context of creating a collection. This is also technically wrong, because creating a collection consists of deploying a new contract on the blockchain, which will be a set and blueprint for the tokens included in a given collection. Mint thus only means creating a new NFT and it only happens once in a given token's lifecycle.</p>
<p class="mt-4"><span class="font-bold">IPFS</span> - An abbreviation of InterPlanetary File System. A protocol to create a permanent and decentralized method of storing and sharing files. IPFS is content-addressed (based on cryptographic hash functions) and works on a peer-to-peer model. The nodes of the IPFS network form a distributed file system. At Omnisea, we believe that IPFS is to files what Bitcoin is to money. For this reason, the data and files of every collection and NFT created through our platform are stored on IPFS. In this way, we make sure that the data of your tokens will never be compromised.</p>
</p>`
				},
				{
					name: 'Support',
					imageName: 'abstract-4.png',
					description: '',
					redirectTo: '',
				},
			],
			[HelpCenterCategoryEnum.CREATING]: [
				{
					name: 'How to create Omnichain Collection?',
					imageName: 'abstract-5.png',
					description: 'Everything you need to know about already a simple process of creating collection on Omnisea.',
					content: `<p class="text-lg leading-normal">Everything you need to know about already a simple process of creating collection on Omnisea. Don't worry - it's very simple.</p>
<div>✅ First of all, connect your wallet.</div>
<div class="mt-4">✅ Then click "Launch" or go to <a href="https://www.omnisea.org/create" target="_blank" class="text-accent">omnisea.org/create</a>. Fill out the form with all the details about your collection. The required data is only the name and photo (cover) of the collection. In addition, you can tag properties such as description, date range from when to when NFTs can be minted, price per mint, link to external collection page, etc.</div>
<div class="mt-4">✔️<span class="text-sm">(Optional)</span> Moreover, you can add all NFTs with their files included in the collection.</div>
<div class="mt-4">✔️<span class="text-sm">(Optional)</span> Roll out the "advanced options" if you want to decide on which blockchain collection should be created, or make your collection initially private (hidden).</div>
<div class="mt-4">✅ Click the "Confirm" button. Wait for the collection's and NFTs' files to be uploaded to IPFS, and confirm the transaction.</div>

<p>After creating your collection, you'll be automatically redirected to your Profile page, where you'll be able to see all your collections. Note, that you can always set your collection "Private" to hide it. It doesn't mean your collection is deleted on-chain. It means that your collection won't be accessible for other users.</p>`,
				},
				{
					name: 'Why? Advantages.',
					imageName: 'abstract-6.png',
					description: 'Why Omnichain collection is beneficial for Creators?',
					content: `<p class="text-lg leading-normal">Why Omnichain collection is beneficial for Creators?</p>

<div>✅ First and foremost, the Omnichain collection allows its NFTs to be minted to collectors from other blockchains. Users do not have to align with the creator's network and collection to mint - they do not have to transfer funds from their favorite ecosystem. They can mint, for example, from the collection that is on Ethereum, while remaining at the same time, for example, on the BNB Chain or Avalanche. This means that by creating the Omnichain collection, as a creator you significantly increase your audience.</div>
<div class="mt-4">✅ Secondly, your collection is becoming somewhat immune to the inevitable development of blockchain technology over time. Even in the event of new blockchains emerging, their users will still be able to mint NFT from your collection. Your ecosystem will only expand.</div>
<div class="mt-4">✅ Last but not least, remember that by making your collection chain-agnostic, you are significantly improving the UX of any product using your NFTs, as your collection operates at a higher level of abstraction and has far fewer end user requirements.</div>`
				},
				{
					name: 'IPFS. Always.',
					imageName: 'abstract-7.png',
					description: 'Why NFTs with metadata and related files must be stored using IPFS.',
					content: `<p class="text-lg leading-normal">Why NFTs with metadata and related files must be stored using IPFS?</p>
<p>At Omnisea, we consider the use of IPFS an obligation rather than an option in the collection and NFT creation process. Every NFT created through Omnisea has metadata stored on IPFS.</p>
<p>IPFS stands for InterPlanetary File System. It is a protocol to create a permanent and decentralized method of storing and sharing files. IPFS is content-addressed (based on cryptographic hash functions) and works on a peer-to-peer model. The nodes of the IPFS network form a distributed file system. At Omnisea, we think that IPFS is to files what Bitcoin is to money. For this reason, the data and files of every collection and NFT created through our platform are stored on IPFS. In this way, we make sure that the data of your tokens will never be compromised.</p>
<p>Some NFT marketplaces or launchpads allow users to so-called "Freeze" NFT data on IPFS if requested - optionally. We think that IPFS is now a crucial, fundamental part of the NFTs ecosystem.</p>`,
				},
				{
					name: 'Omnichain vs. Bridging',
					imageName: 'abstract-8.png',
					description: 'Why NFTs bridging not only doesn\'t make sense but also is harmful.',
					content: `<p class="text-lg leading-normal">Learn why NFTs bridging not only doesn't make sense but also is harmful.</p>
<p>So far, the problem of limiting collection to one blockchain was “solved” by projects providing so-called “NFT bridges”. What is in fact an NFT bridge and why it doesn’t make sense to use it?</p>
<p class="font-bold">How does the NFT bridge work?</p>
<p>Well, let’s just say it doesn’t even bridge an actual token. It mostly does the following:

1. Burns or locks NFT on the source chain.
2. Mints NFT with identical (sometimes even not exactly 1:1) attributes on the destination chain.
3. Transfers that NEW token to an owner.</p>

<p class="font-bold">What is the problem with that solution?</p>

<p>First of all, this method violates the fundamental NFT property which is non-fungibility. As you’ve probably noticed or already knew, the bridgoor doesn’t receive the same token. From a technical standpoint, he has lost his NFT and gained a new one, which is just a copy. It also lacks all on-chain value and history such as relation to deployer (contract creator), potentially link with metadata on IPFS, etc. In case of any internal logic used by some contract (on the source chain) with reference to that original token — it’s also gone.

If you’d have an amazing or very valuable NFT, would you let it burn in a bridging process, and let it inevitably lose all its unique and original attributes (and also receive a copy)?

Exactly. That’s why bridging NFTs doesn’t make any sense.</p>

<p class="font-bold">The real solution</p>
<p>Fortunately, thanks to the Omnichain approach with LayerZero at its forefront, we can now say we have the new and final solution for this problem. What we, at Omnisea, are doing is not bridging your NFTs at all. Your non-fungible token should never become fungible and should always stay on its home chain protecting its history, uniqueness, and authenticity! Otherwise, what are we even doing here?

All interactions with an Omnichain NFT (and collections) such as minting, buying, selling, using by external contracts, etc., can now be done from a different chain. For example, minting token on Avalanche from Ethereum or buying NFT on Fantom from BNB Chain.

Considering the pros and cons of the Omnichain approach and bridging, we have a clear winner and loser. That’s also why we won’t implement any functionality that works like a bridge. Once again, these violate the NFT’s intrinsic value, properties, and purpose.</p>`,
				},
			],
			[HelpCenterCategoryEnum.MINTING]: [
				{
					name: 'Omnichain Minting Guide',
					imageName: 'abstract-4.png',
					description: 'Learn how to mint between chains, and the possibilities for both Creator and Collector.',
					content: `<p class="text-lg leading-normal">Minting between chains is now very easy. Learn how to do it, and about the possibilities it offers for both Creator and Collector.</p>
<p>If you want to mint NFT on e.g. Ethereum from for example Avalanche, let's do these steps:</p>
<div>✅ Connect your wallet.</div>
<div>✅ Go to the collection you want to mint from. You can find collections for example on the <a href="https://www.omnisea.org/collections" target="_blank" class="text-accent">Explore</a> page</div>
<div class="mt-4">✔️<span class="text-sm">(Note)</span> If the collection's price per mint is specified, make sure to have enough funds in your wallet.</div>
<div>✅ Wait for the "Mint" button to become active. Click on it and accept the minting transaction.</div>
<div class="mt-4">✔️<span class="text-sm">(Note)</span> If the "Mint" button is inactive take a look at minting date range (from-to), and the remaining (if the supply is not unlimited) count.</div>
<p>Minting while being on the same chain as the collection is instant. NFT should be visible in your Profile -> NFTs. Omnichain mint can take a maximum few minutes to complete on the target blockchain.</p>`,
				},
				{
					name: 'Advantages',
					imageName: 'abstract-1.png',
					description: '',
					content: `<p class="text-lg leading-normal">Why Omnichain minting possibility is beneficial for Creators and Collectors?</p>

<div>✅ First and foremost, the Omnichain collection allows its NFTs to be minted to collectors from other blockchains. Users do not have to align with the creator's network and collection to mint - they do not have to transfer funds from their favorite ecosystem. They can mint, for example, from the collection that is on Ethereum, while remaining at the same time, for example, on the BNB Chain or Avalanche. This means that by creating the Omnichain collection, as a creator you significantly increase your audience.</div>
<div class="mt-4">✅ Secondly, a collection is becoming somewhat immune to the inevitable development of blockchain technology over time. Even in the event of new blockchains emerging, their users will still be able to mint NFT from a collection. Your ecosystem will only expand.</div>
<div class="mt-4">✅ Omnichain Minting is a revolutionary approach compared to NFT Bridging for preserving the value of the NFT itself from the perspective of multiple chains. Learn more here in the "Omnichain vs. Bridging" section.</div>`
				},
				{
					name: 'Costs',
					imageName: 'abstract-2.png',
					description: '',
					content: `<p class="text-lg leading-normal">Learn everything about minting transaction fees and costs (platform fees) present on the Omnisea.</p>
<h2 class="text-xl">When user pays?</h2>
<p>There are two types of fees in the minting process - the blockchain transaction fee and the mint fee. The former is always present and its value is dynamic, depending on the given chain and its current activity. The mint fee is set by the creator of the collection when it is created. The developer may set the price for the mint in a unified currency for all supported blockchains - USDC (and soon also OSEA), or not set the price for the mint (Free mint).</p>
<p><span class="font-bold">Important!</span> During an Omnichain transaction, the transaction fee is calculated in relation to the target blockchain, not the source blockchain. This means that if you are minting e.g. on Ethereum from Fantom, you will likely find that the fee (in FTM) is higher than usual. In the event that you do not have enough native currency, the transaction will not be sent, and you will be informed in an error window about the required amount (of e.g. FTM).</p>
<h2 class="text-xl">Platform fee</h2>
<p>We charge a small 2% commission on each mint, unless it was of course Free Mint. The amount of this commission is included in the price per mint set by the creator. This means that if the price for a mint is 10 USDC, the creator will earn 9.8 USDC and we will charge 0.2 USDC. The earned funds will allow us to cover the current operating costs and support the Ecosystem Fund.</p>
<h2 class="text-xl">Approval transactions</h2>
<p>It is worth noting that if you are paying for a minting transaction for the first time using USDC, you will be asked to accept this expense by confirming the so-called Approval transaction. This type of transaction usually, even on blockchains with higher transaction fees, involves a very modest transaction fee and is charged once.</p>`,
				},
				{
					name: 'Omnichain vs. Bridging',
					imageName: 'abstract-3.png',
					description: '',
					content: `<p class="text-lg leading-normal">Learn why NFTs bridging not only doesn't make sense but also is harmful.</p>
<p>So far, the problem of limiting collection to one blockchain was “solved” by projects providing so-called “NFT bridges”. What is in fact an NFT bridge and why it doesn’t make sense to use it?</p>
<p class="font-bold">How does the NFT bridge work?</p>
<p>Well, let’s just say it doesn’t even bridge an actual token. It mostly does the following:

1. Burns or locks NFT on the source chain.
2. Mints NFT with identical (sometimes even not exactly 1:1) attributes on the destination chain.
3. Transfers that NEW token to an owner.</p>

<p class="font-bold">What is the problem with that solution?</p>

<p>First of all, this method violates the fundamental NFT property which is non-fungibility. As you’ve probably noticed or already knew, the bridgoor doesn’t receive the same token. From a technical standpoint, he has lost his NFT and gained a new one, which is just a copy. It also lacks all on-chain value and history such as relation to deployer (contract creator), potentially link with metadata on IPFS, etc. In case of any internal logic used by some contract (on the source chain) with reference to that original token — it’s also gone.

If you’d have an amazing or very valuable NFT, would you let it burn in a bridging process, and let it inevitably lose all its unique and original attributes (and also receive a copy)?

Exactly. That’s why bridging NFTs doesn’t make any sense.</p>

<p class="font-bold">The real solution</p>
<p>Fortunately, thanks to the Omnichain approach with LayerZero at its forefront, we can now say we have the new and final solution for this problem. What we, at Omnisea, are doing is not bridging your NFTs at all. Your non-fungible token should never become fungible and should always stay on its home chain protecting its history, uniqueness, and authenticity! Otherwise, what are we even doing here?

All interactions with an Omnichain NFT (and collections) such as minting, buying, selling, using by external contracts, etc., can now be done from a different chain. For example, minting token on Avalanche from Ethereum or buying NFT on Fantom from BNB Chain.

Considering the pros and cons of the Omnichain approach and bridging, we have a clear winner and loser. That’s also why we won’t implement any functionality that works like a bridge. Once again, these violate the NFT’s intrinsic value, properties, and purpose.</p>`,
				},
			],
			[HelpCenterCategoryEnum.PAYMENTS]: [
				{
					name: 'Omnisea Payments 101',
					imageName: 'abstract-7.png',
					description: 'Check supported assets, when you\'ll use them, and payment types inside the platform.',
					content: `<p class="text-lg leading-normal">Here's a quick summary of all payments inside Omnisea</p>
<h2 class="text-2xl font-bold font-display">Transaction and platform fees</h2>
<p>There are two types of fees in the minting process - the blockchain transaction fee and the mint fee. The former is always present and its value is dynamic, depending on the given chain and its current activity. The mint fee is set by the creator of the collection when it is created. The developer may set the price for the mint in a unified currency for all supported blockchains - USDC (and soon also OSEA), or not set the price for the mint (Free mint).</p>
<p><span class="font-bold">Important!</span> During an Omnichain transaction, the transaction fee is calculated in relation to the target blockchain, not the source blockchain. This means that if you are minting e.g. on Ethereum from Fantom, you will likely find that the fee (in FTM) is higher than usual. In the event that you do not have enough native currency, the transaction will not be sent, and you will be informed in an error window about the required amount (of e.g. FTM).</p>
<h2 class="text-xl">Platform fee</h2>
<p>We charge a small 2% commission on each mint, unless it was of course Free Mint. The amount of this commission is included in the price per mint set by the creator. This means that if the price for a mint is 10 USDC, the creator will earn 9.8 USDC and we will charge 0.2 USDC. The earned funds will allow us to cover the current operating costs and support the Ecosystem Fund.</p>
<h2 class="text-xl">Approval transactions</h2>
<p>It is worth noting that if you are paying for a minting transaction for the first time using USDC, you will be asked to accept this expense by confirming the so-called Approval transaction. This type of transaction usually, even on blockchains with higher transaction fees, involves a very modest transaction fee and is charged once.</p>

<h2 class="text-2xl font-bold font-display">Other payments within the platform</h2>
<p><span class="font-bold">Withdrawing Refunds</span> - In case of Omnichain minting transaction, if you've paid for the NFT mint using e.g. USDC, but your transaction failed on the target chain, you will be entitled to a refund. The option to withdraw USDC will then appear on the collection page of this NFT in the "Refunds" tab. Read more in the "Payments" section of this Help Center.</p>
<p><span class="font-bold">Withdrawing Earnings</span> - If you are a creator of a collection with set price per mint, and someone minted NFT from your collection (e.g. using USDC), you'll be able to withdraw your earnings using the "Claim" button while being connected to the network they are on (Go to the page of your collection and check the "Earned" tab).</p>
`,
				},
				{
					name: 'Refunds',
					imageName: 'abstract-6.png',
					description: '',
					content: `<p class="text-lg leading-normal">In case of a minting transaction, if you've paid for the NFT mint using e.g. USDC, but your transaction failed on the target chain, you will be entitled to a refund. Learn how to claim it.</p>
<p>
<p>1. Make sure that transaction on the source chain (the one you were connected to) was successful and the funds has been debited from your wallet (e.g. USDC - do not confuse this fee with a non-refundable transaction fee).</p>
<p>2. On Mainnet, any Omnichain transaction should be processed by an Omnichain provider (protocol), such as LayerZero, within minutes. To be sure, however, we recommend waiting longer and checking if the NFT has not appeared on your Profile.</p>
<p>3. If the NFT did not show up, check if you are eligible for a refund. You can do this by going to the collection page, and then wait for the "Refunds" tab to load. If so, check the amount to be refunded in the table displayed there and press the "Claim" button.</p>
<p>4. Note that at the moment the only anticipated situation in which you are entitled to a refund is when on the target chain, at the time of NFT minting, it was found that the maximum supply of the NFT collection had already been reached and therefore it was not possible to make a mint. In another case, the reason was probably independent of us, e.g. an error on the provider's side or even due to the current state of the blockchain itself.</p>
</p>`,
				},
				{
					name: 'Earnings',
					imageName: 'abstract-8.png',
					description: '',
					content: `<p class="text-lg leading-normal">If you are a creator of a collection with set price per mint, and someone minted NFT from your collection (e.g. using USDC), you'll be able to withdraw your earnings. See how to do it.</p>
<p>
<p>1. Go to your collection's page.</p>
<p>2. Wait for the "Earned" tab to load.</p>
<p>3. Open the tab, and check your earnings table.</p>
<p>4. Users probably paid for minting on multiple blockchains. That's why the table is categorized per chain. You're able to collect earnings from each chain separately. The funds will be sent to your address on the indicated network.</p>
</p>`,
				},
				{
					name: 'OSEA or USDC?',
					imageName: 'abstract-5.png',
					description: '',
					content: `<p class="text-lg leading-normal"><span class="font-bold">USDC (and soon OSEA)</span> are the supported assets inside our platform. In both cases, we were guided by the possibility of setting a unified NFT prices on all blockchains, and the greatest possible availability of the selected cryptocurrency on many chains. In the case of OSEA, we have direct confidence in the future existence of the token on each supported network.</p>
<p>
The two main reasons for the existence of OSEA are:
1. Make sure it will always be possible to set the mint price in a unified currency for all chains. This is especially true when all payments within our platform are transferred to the Omnichain system, including refunds and payouts (earnings withdraw).
2. Introduction of a unified asset to the governance of the Omnichain Router on all supported blockchains. Read more about it in the section dedicated to Omnichain Router.
</p>`,
				},
			],
			[HelpCenterCategoryEnum.RESOURCES]: [
				{
					name: 'Omnichain? New hype or real innovation?',
					imageName: 'abstract-3.png',
					description: '',
					content: `<p class="text-lg leading-normal">Omnichain is an evolution of the multi-chain approach that enables interaction between blockchains in an unprecedented way.</p>
<p>The future is multi-chain. You may have heard this phrase. This is true. Tribalism can still be found in circles devoted to specific cryptocurrency ecosystems. However, this does not change the fact that the idea of a single monolithic blockchain that will handle all use cases and transactions while maintaining a high level of scalability and decentralization has passed.</p>
<p>In recent years, we have seen a huge influx of new blockchains. Chains such as BSC first, then incl. Polygon, Fantom, Solana and Avalanche allowed us to make transactions for a fraction of what we paid on Ethereum.</p>
<p>With the development of so many different ecosystems, the problem of their fragmentation has arisen. Yes, thanks to the multi-chain approach, we are able to mint and trade NFT on multiple blockchains side by side. The key point, however, is that these NFTs, and the entire ecosystems they are comprised of, cannot communicate with each other, and do not increase each other's value. To solve this problem, solution designers competed in ideas for the best bridges. In the context of DeFi, this method was not as flawed as it was with the NFTs. Read more about this in the dedicated article - "Omnichain vs. Bridging".</p>
<p>The Omnichain approach and technologies provide a solution to the problem described in that article. Above all, however, they solve the problem of fragmentation of NFT ecosystems. Thanks to protocols like Omnisea, users will now be able to open their NFT collections to other worlds - other blockchains. In the case of the Omnisea platform, this obviously applies to Omnichain collection creation and Omnichain Minting. This year, the Omnichain Marketplace functionalities will also be added (buying, selling, bidding between chains).</p>
<p>Learn more about specific benefits coming from Omnichain technology in relation to NFTs from this Help Center's sections dedicated "Creating" and "Minting".</p>`,
				},
				{
					name: 'Contracts',
					imageName: 'abstract-4.png',
					description: '',
					redirectTo: 'https://medium.com/@omnisea/transparency-omnisea-contracts-and-development-update-a141367f0be9',
				},
				{
					name: 'Whitepaper',
					imageName: 'abstract-1.png',
					description: '',
					redirectTo: 'https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io',
				},
				{
					name: 'Omnichain Router',
					imageName: 'abstract-2.png',
					description: '',
					content: `<p class="text-lg leading-normal">Omnichain Router is an abstraction layer for the expanding range of Omnichain solutions such as LayerZero or Axelar.</p>
<p>In our opinion, Omnichain Router is the biggest puzzle of our protocol. Its use cases go beyond NFTs. It can be used by the Omnisea, and soon also by other projects, to initiate and receive any Omnichain tasks, and even route them between the endpoints of many unrelated Omnichain providers such as LayerZero or Axelar, and others in the future.</p>
<p>Omnichain Router maps a given action (e.g. mint or creating a collection) on the basis of the target chain to the provider that supports this chain, and then delegates this action to it for processing on the indicated target blockchain. The user pays a transaction fee on "his" source chain without transferring funds to the target chain.</p>
<p>From a technical standpoint, Omnichain Router is our contract deployed on each supported blockchain. The Omnichain Router SDK will be released to allow teams to easily build Omnichain projects. The router will be responsible for receiving the job with the encoded data and mapping the job to the correct provider and its endpoint based on the name of the target chain itself. The Omnichain Router will be upgraded in order to adapt it to the newest solutions, in accordance with the principles of decentralization. The address of the latest Router implementation is chosen by the community voting using the OSEA token. In order to save applications and protocols from having to update the Router address, the newest selected address is updated in the Omnichain Controller contract by voting. The protocols therefore refer indirectly to the Router (although they can do it by selecting a specific version), but to the Controller, which is responsible for updating the Router and transferring tasks to its latest instance.</p>
<p>Currently, regarding Omnichain providers, we support LayerZero's protocol with plans to soon integrate Axelar into the Router.</p>`,
				},
			],
			[HelpCenterCategoryEnum.DEVELOPERS]: [
				{
					name: 'Documentation',
					imageName: 'abstract-6.png',
					description: 'Learn more about our protocols, contracts, and how to use them programmatically.',
					redirectTo: 'https://docs.omnisea.org',
				},
				{
					name: 'Omnichain Router',
					imageName: 'abstract-5.png',
					description: '',
					content: `<p class="text-lg leading-normal">Omnichain Router is an abstraction layer for the expanding range of Omnichain solutions such as LayerZero or Axelar.</p>
<p>In our opinion, Omnichain Router is the biggest puzzle of our protocol. Its use cases go beyond NFTs. It can be used by the Omnisea, and soon also by other projects, to initiate and receive any Omnichain tasks, and even route them between the endpoints of many unrelated Omnichain providers such as LayerZero or Axelar, and others in the future.</p>
<p>Omnichain Router maps a given action (e.g. mint or creating a collection) on the basis of the target chain to the provider that supports this chain, and then delegates this action to it for processing on the indicated target blockchain. The user pays a transaction fee on "his" source chain without transferring funds to the target chain.</p>
<p>From a technical standpoint, Omnichain Router is our contract deployed on each supported blockchain. The Omnichain Router SDK will be released to allow teams to easily build Omnichain projects. The router will be responsible for receiving the job with the encoded data and mapping the job to the correct provider and its endpoint based on the name of the target chain itself. The Omnichain Router will be upgraded in order to adapt it to the newest solutions, in accordance with the principles of decentralization. The address of the latest Router implementation is chosen by the community voting using the OSEA token. In order to save applications and protocols from having to update the Router address, the newest selected address is updated in the Omnichain Controller contract by voting. The protocols therefore refer indirectly to the Router (although they can do it by selecting a specific version), but to the Controller, which is responsible for updating the Router and transferring tasks to its latest instance.</p>
<p>Currently, regarding Omnichain providers, we support LayerZero's protocol with plans to soon integrate Axelar into the Router.</p>`,
				},
				{
					name: 'Contracts',
					imageName: 'abstract-8.png',
					description: '',
					redirectTo: 'https://medium.com/@omnisea/transparency-omnisea-contracts-and-development-update-a141367f0be9',
				},
				{
					name: 'Whitepaper',
					imageName: 'abstract-7.png',
					description: '',
					redirectTo: 'https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io',
				},
			],
		};

		this.categoriesFAQ = {
			[HelpCenterCategoryEnum.BASICS]: [
				{
					question: 'What wallets are supported?',
					answer: `<div>Users can connect to Omnisea using <span class="font-bold">MetaMask or Torus</span> wallets.</div>
<div class="mt-4"><span class="font-bold">MetaMask</span> is the most popular Web3 wallet and works as a browser extension. It allows you to start your adventure with decentralized applications in minutes. It offers the possibility of storing cryptocurrencies and NFTs without an intermediary, exchange of assets (e.g. ETH to USDC or OSEA used within the platform), or signing of transactions, such as the creation of NFT or mint collections.</div>
<div class="mt-4">Once connected, <span class="font-bold">Torus</span> works like MetaMask, but offers other possibilities for wallet creation and connection to applications. The main advantage of Torrus Wallet is the ability to create a wallet with your Google, Facebook, Twitter, Discord, and other social accounts. Torus Wallet, unlike MetaMask, does not work as a browser extension, but when clicked, it becomes an integral part of the Omnisea application and allows you to use the wallet from any device, e.g. phone or tablet.</div>`,
				},
				{
					question: 'What if I can\'t connect my wallet?',
					answer: `<div>1. First, refresh the page. Some wallets, especially those running as browser extensions, have a known connection problem if the application is launched as the first page in a newly opened browser (e.g. via a link).</div>
<div>2. If that didn't help, please clear your browser's cache and try again.</div>
<div>3. If possible, try connecting using a different browser or a different wallet.</div>
<div>4. You can always reach our support. We'll be happy to help.</div>`,
				},
				{
					question: 'Can I connect and use Omnisea from mobile?',
					answer: `<span class="font-bold">Yes</span>, you can take full advantage of Omnisea with a mobile device, e.g. a phone, using the Torus wallet. In the future, we will also enable connection using other mobile wallets, but at the moment it is the best, safe and recommended solution.`,
				},
				{
					question: 'What blockchains do we support?',
					answer: `At present (Testnet) we support Ethereum, BNB Chain (BSC), Polygon, Avalanche, Arbitrum, Optimism, and Fantom networks. In the near future, we will be able to connect to many other blockchains, such as Solana or chains from the Cosmos ecosystem.`,
				},
				{
					question: 'How do I fund my wallet?',
					answer: `<div>There are many simple ways to fund your wallet. First of all, the MetaMask and Torus wallets have built-in options for buying cryptocurrencies using, among others credit card. If possible, we recommend buying USDC, as it is one of the assets supported by our platform. If you have a different currency such as ETH, you can convert it to USDC or OSEA using MetaMask or decentralized exchanges such as Uniswap.</div>
<div class="mt-4">Another proven, common way is to buy your first cryptocurrencies from exchanges such as Binance, Coinbase, etc., and then send them to your wallet's public address.</div>`,
				},
				{
					question: 'What assets are supported on Omnisea, and how to get them?',
					answer: `<div><span class="font-bold">USDC (and soon OSEA)</span> are the supported assets inside our platform. In both cases, we were guided by the possibility of setting a unified NFT prices on all blockchains, as well as the greatest possible availability of the selected cryptocurrency on many chains. In the case of OSEA, we have direct confidence in the future existence of the token on each supported network.</div>`,
				},
				{
					question: 'How to create and use my Profile?',
					answer: `<div>1. Connect to your wallet.</div>
<div>1. Connect to your wallet.</div>
<div>2. Go to the My Profile page.</div>
<div>3. Click on the Edit Profile button.</div>
<div>4. Enter a unique name for the profile.</div>
<div>5. If you want, enter other optional data, such as avatar photos, backgrounds, and social links.</div>
<div>6. Click Confirm.</div>`,
				},
				{
					question: 'How long does the Omnichain transaction take to complete?',
					answer: `You may have noticed that on the Testnet network, operations from one chain to another may take longer than actions on the same blockchain. Such a situation would not take place on Mainnet - Omnichain operations will take up to a few minutes.`,
				},
				{
					question: 'What if my transaction has failed?',
					answer: `<div>Depends on what the reason for this was. This failure may be due to a stagnation in the RPC (network) to which you are connected - especially on test networks. You can check RPC status on Chainlist.org and potentially (for advanced users) change RPC in your wallet settings.</div>
<div class="mt-4">If the reason is different, you may have allocated too little transaction fee on the selected blockchain.</div>
<div class="mt-4">If it was a minting transaction and you paid for the NFT using eg USDC, you will be entitled to a refund. The option to withdraw USDC will then appear on the collection page of this NFT in the "Refunds" tab. Read more in the "Payments" section of this Help Center.</div>
<div class="mt-4">In case of further problem, please do not hesitate to contact our support team.</div>`,
				},
			],
			[HelpCenterCategoryEnum.CREATING]: [
				{
					question: 'What do I need to do to create a collection?',
					answer: `<div>To create a collection, you first need to connect your wallet.</div>
<div class="mt-4">Then click "Launch" or go to omnisea.org/create. Fill out the form with all the details about your collection. The required data is only the name and photo (cover) of the collection. In addition, you can tag properties such as description, date range from when to when NFTs can be minted, price per mint, link to external collection page, etc.</div>
<div class="mt-4">Moreover, you can add all NFTs with their files included in the collection.</div>`,
				},
				{
					question: 'How can I add NFT to my collection?',
					answer: `<div>You can add NFT to a collection only at the stage of creating a collection. All you have to do is click "Add NFT" in the "NFTs" section of the form and upload the file related to the NFT.</div>
<div class="mt-4">Based on the amount of NFT added, the value of the total supply of the collection will be automatically determined. In the absence of an added NFT, the supply will be infinite and the file associated with each minted NFT will be the collection cover.</div>`,
				},
				{
					question: 'What if no NFTs are added to the collection?',
					answer: `In the absence of an added NFT, the supply will be infinite and the file associated with each minted NFT will be the collection cover.`,
				},
				{
					question: 'What if no NFTs are added to the collection?',
					answer: `In the absence of an added NFT, the supply will be infinite and the file associated with each minted NFT will be the collection cover.`,
				},
				{
					question: 'What NFT standard do we use when creating a collection?',
					answer: `For EVM-compatible blockchains, we use the ERC-721 standard. We will update this notice as appropriate when support is rolled out to other chains.`,
				},
				{
					question: 'What does the Omnichain Collection mean? What are its advantages?',
					answer: `<div>First and foremost, the Omnichain collection allows its NFTs to be minted to collectors from other blockchains. Users do not have to align with the creator's network and collection to mint - they do not have to transfer funds from their favorite ecosystem. They can mint, for example, from the collection that is on Ethereum, while remaining at the same time, for example, on the BNB Chain or Avalanche. This means that by creating the Omnichain collection, as a creator you significantly increase your audience.</div>
<div class="mt-4">Secondly, your collection is becoming somewhat immune to the inevitable development of blockchain technology over time. Even in the event of new blockchains emerging, their users will still be able to mint NFT from your collection. Your ecosystem will only expand.</div>
<div class="mt-4">Last but not least, remember that by making your collection chain-agnostic, you are significantly improving the UX of any product using your NFTs, as your collection operates at a higher level of abstraction and has far fewer end user requirements.</div>`,
				},
				{
					question: 'Can I create a collection on a different chain than I\'m connected to?',
					answer: `Yes, when creating a collection, expand the advanced settings and select the target blockchain. For example, you can create a collection on Ethereum while currently on Polygon, etc. Your collection's NFTs will be minted on the target chain (Polygon in this example) by users from any supported chain - no burning, wrapping, no bridging.`,
				},
			],
			[HelpCenterCategoryEnum.MINTING]: [
				{
					question: 'What does Omnichain Mint mean?',
					answer: `This means NFT minting, which exists on blockchain A, being connected to blockchain B. We enable users to mint NFT, which belongs to a collection that is e.g. on Ethereum, without leaving their network, e.g. BNB Chain, Avalanche, etc.`,
				},
				{
					question: 'How can I add NFT to my collection?',
					answer: `<div>You can add NFT to a collection only at the stage of creating a collection. All you have to do is click "Add NFT" in the "NFTs" section of the form and upload the file related to the NFT.</div>
<div class="mt-4">Based on the amount of NFT added, the value of the total supply of the collection will be automatically determined. In the absence of an added NFT, the supply will be infinite and the file associated with each minted NFT will be the collection cover.</div>`,
				},
				{
					question: 'Can I sell my NFT?',
					answer: `Yes, your NFT is fully compatible with the ERC-721 standard. You can list and sell it on existing NFT Marketplaces. Omnisea will introduce Omnichain Marketplace functionalities later this year.`,
				},
				{
					question: 'I made a successful mint transaction and my NFT still hasn\'t shown up.',
					answer: `NFT minted on the same chain (non-Omnichain) should appear immediately. Omnichain Mint takes a few minutes for the transaction to occur on the target chain. If you are using the current Testnet, the normal situation is even significant delays for reasons beyond Omnisea's control, which are on the Omnichain provider's side. <span class="font-bold">On Mainnet, a similar situation would not take place, and the NFT would be visible in a maximum of a few minutes.</span>`,
				},
			],
			[HelpCenterCategoryEnum.PAYMENTS]: [
				{
					question: 'I paid for mint and transaction has failed on the target blockchain',
					answer: `In that case, you will be entitled to a refund. The option to withdraw USDC (asset in which you've made payment) will then appear on the collection page in the "Refunds" tab.`,
				},
				{
					question: 'Where can I see and claim my refunds?',
					answer: `Right now we display refunds on page of each collection from which the mint transaction has failed to complete. Go to the collection page and wait for your refunds to load in the "Refunds" tab. Use the "Claim" button to get the refund.`,
				},
				{
					question: 'How can I collect the earned funds?',
					answer: `Go to the page of your collection and see the funds you are entitled to. Withdraw them using the "Claim" button while connected to the network they are on (all displayed in the table).`,
				},
				{
					question: 'How can I get assets to pay for minting?',
					answer: `<div>First of all, fund your wallet. MetaMask and Torus wallets have built-in options for buying cryptocurrencies using, among others credit card. Buy USDC or OSEA - assets supported by our platform. If you have a different currency such as ETH, you can convert it to USDC or OSEA using MetaMask or decentralized exchanges such as Uniswap. Learn more in the "Getting Started" section.</div>`,
				},
				{
					question: 'What assets are supported on Omnisea, and how to get them?',
					answer: `<div><span class="font-bold">USDC (and soon OSEA)</span> are the currently supported assets inside our platform. In both cases, we were guided by the possibility of setting a unified NFT prices on all blockchains, as well as the greatest possible availability of the selected cryptocurrency on many chains. In the case of OSEA, we have direct confidence in the future existence of the token on each supported network.</div>`,
				},
			],
			[HelpCenterCategoryEnum.RESOURCES]: [
				{
					question: 'Where can I find the Whitepaper?',
					answer: `We've uploaded our Whitepaper on IPFS. It's available at: https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io`,
				},
				{
					question: 'How it works?',
					answer: `Omnisea uses Omnichain solution providers such as LayerZero. We recommend checking the technical issues of a given solution in the Whitepaper of a given provider. Omnisea, with the help of the created Omnichain Router, maps a given action (e.g. mint or creating a collection) on the basis of the target chain to the provider that supports this chain, and then delegates this action to it for processing on the indicated target blockchain. The user pays a transaction fee on "his" source chain without transferring funds to the target chain. More information in our Whitepaper.`,
				},
				{
					question: 'What is Omnichain Router?',
					answer: `Omnichain Router is our contract deployed for each supported blockchain. It is an abstraction layer for the expanding range of Omnichain solutions such as LayerZero or Axelar. The Omnichain Router SDK will be released to allow teams to easily build Omnichain projects. The router is responsible for receiving the job with the encoded data and mapping the job to the correct provider and its endpoint based on the name of the target chain itself. The Omnichain Router will be upgraded in order to adapt it to the newest solutions, in accordance with the principles of decentralization. The address of the latest Router implementation is chosen by the community voting using the OSEA token. In order to save applications and protocols from having to update the Router address, the newest selected address is updated in the Omnichain Controller contract by voting. The protocols therefore refer indirectly to the Router (although they can do it by selecting a specific version), but to the Controller, which is responsible for updating the Router and transferring tasks to its latest instance.`,
				},
				{
					question: 'What Omnichain solutions (providers) are we currently using?',
					answer: `We currently only use LayerZero. After introducing and testing the Omnichain Router in v4 of our Testnet, we will also implement Axelar as a second provider. The above will enable integration with, among others Moonbeam (Polkadot) or blockchains from the Cosmos ecosystem.`,
				},
			],
			[HelpCenterCategoryEnum.DEVELOPERS]: [
				{
					question: 'Where can I find the documentation and code?',
					answer: `Documentation, tutorials, code, examples will be published after Mainnet goes live. In the meantime, we encourage anyone willing to contact us. We are open to sharing our materials and discussing it :)`,
				},
				{
					question: 'What is Omnichain Router?',
					answer: `Omnichain Router is our contract deployed for each supported blockchain. It is an abstraction layer for the expanding range of Omnichain solutions such as LayerZero or Axelar. The Omnichain Router SDK will be released to allow teams to easily build Omnichain projects. The router is responsible for receiving the job with the encoded data and mapping the job to the correct provider and its endpoint based on the name of the target chain itself. The Omnichain Router will be upgraded in order to adapt it to the newest solutions, in accordance with the principles of decentralization. The address of the latest Router implementation is chosen by the community voting using the OSEA token. In order to save applications and protocols from having to update the Router address, the newest selected address is updated in the Omnichain Controller contract by voting. The protocols therefore refer indirectly to the Router (although they can do it by selecting a specific version), but to the Controller, which is responsible for updating the Router and transferring tasks to its latest instance.`,
				},
				{
					question: 'Where can I find the Whitepaper?',
					answer: `We've uploaded our Whitepaper on IPFS. It's available at: <a href="https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io" target="_blank" class="text-accent">https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io</a>`,
				},
				{
					question: 'Will Omnisea contracts be open to third-party protocols?',
					answer: `Yes, not only the Omnichain Router (general use contract for Omnichain tasks), but also our contracts with direct Omnichain collection and polling functions will be made available to other teams. In our documentation, we will create sections for protocols (e.g. integrations using Solidity) and dApps (Web - JavaScript).`,
				},
			],
		};
	}
}
