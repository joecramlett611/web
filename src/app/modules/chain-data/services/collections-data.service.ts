import { Injectable } from '@angular/core';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import { BehaviorSubject } from 'rxjs';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { environment } from '../../../../environments/environment';
import CollectionsService from './collections.service';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';
import * as moment from 'moment';
import { ContractReceipt } from 'ethers';
import { CollectionStatusEnum } from '../../web3/enum/collection-status.enum';
import firebase from 'firebase/compat';
import Web3Service from '../../web3/services/web3.service';
import { CollectionBasicChainDataType } from '../types/collection-basic-chain-data.type';
import { Contract } from '@ethersproject/contracts';
import FirestoreService from '../../firebase/services/firestore.service';
import {
	collection as colRef,
	Firestore,
	getDocs,
	onSnapshot,
	orderBy,
	query,
	QueryConstraint,
	where
} from '@firebase/firestore';
import Unsubscribe = firebase.Unsubscribe;

@Injectable()
export default class CollectionsDataService {
	public readonly userCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	public readonly selectedUserCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	public readonly createdCollectionsFromChain$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	private connectedUserCollectionsListener: Promise<Unsubscribe>;
	private selectedUserCollectionsListener: Promise<Unsubscribe>;

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly web3Service: Web3Service,
		private readonly collectionsService: CollectionsService,
	) {
	}

	/**
	 * @dev Invoked on the application start
	 */
	public async run(): Promise<void> {
		setTimeout(() => {
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					if (this.web3Service.isWalletConnected()) {
						const userAddress: string = await this.web3Service.getUserAddress();

						if (userAddress) {
							clearInterval(walletInterval);
							this.listenToConnectedUserCollectionsLegacy(userAddress);
						}
					}
				}, 250);
			});

			this.userCollections$.subscribe((userCollections) => {
				if (!userCollections?.length) {
					return;
				}

				this.listenToUserCollectionsState();
			});
		}, 250);
	}

	/**
	 * @dev Currently NOT used Firestore function to get collections created by SELECTED user
	 * @param userAddress string
	 */
	public async listenToUserCollections(userAddress: string): Promise<void> {
		const supportedChains: ChainSymbolEnum[] = [...environment.supportedChains, ...environment.isolatedChains];
		const firestore: Firestore = this.firestoreService.firestore$.getValue();

		if (!firestore) {
			throw new Error('No Firestore at listenToUserCollections()');
		}

		if (this.connectedUserCollectionsListener) {
			return;
		}

		const qb = query(
			colRef(firestore, 'collections'),
			where('creator', '==', userAddress),
			where('isPublic', '==', true),
			where('isVisible', '==', true),
			where('chainSymbol', 'in', supportedChains),
			orderBy('createdAt', 'desc'),
		);

		// @ts-ignore
		this.connectedUserCollectionsListener = onSnapshot<ICollectionDetails>(qb, async (querySnapshot) => {
			const userCollections: ICollectionDetails[] = [];

			querySnapshot.forEach((doc) => {
				userCollections.push(doc.data());
			});

			if (userCollections?.length) {
				this.userCollections$.next(userCollections);

				for (const collection of this.userCollections$.getValue()) {
					if (collection.imageURL || collection.isImageLoading) {
						continue;
					}
					collection.isImageLoading = true;

					this.firebaseStorageService.getIpfsFileURL(collection.fileStoragePath).then((url) => {
						collection.imageURL = url;
					});
				}

				return;
			}
			this.userCollections$.next([]);
		});
	}

	/**
	 * @dev Currently used RealtimeDB function to get (listen) collections created by CONNECTED user
	 * @param userAddress string
	 */
	public async listenToConnectedUserCollectionsLegacy(userAddress: string): Promise<void> {
		if (!this.firebaseDatabaseService.database$.getValue()) {
			throw new Error('No DB at getUserCollections()');
		}

		if (this.connectedUserCollectionsListener) {
			await this.firebaseDatabaseService.unsubscribe(this.connectedUserCollectionsListener);
		}

		this.connectedUserCollectionsListener = this.firebaseDatabaseService.get<Record<string, ICollectionDetails>>(
			'/collections/',
			{
				orderBy: 'creator',
				equalTo: userAddress,
			},
			async (creatingCollections) => {
				if (creatingCollections) {
					const supportedChains: ChainSymbolEnum[] = [...environment.supportedChains, ...environment.isolatedChains];
					const userCollections: ICollectionDetails[] = Object.values(creatingCollections)
						.reverse()
						.filter((collection) => supportedChains.includes(collection.chainSymbol));

					this.userCollections$.next(Object.values(userCollections));

					for (const collection of this.userCollections$.getValue()) {
						if (collection.imageURL || collection.isImageLoading) {
							return;
						}
						collection.isImageLoading = true;

						this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
							collection.imageURL = url;
						});
					}

					return;
				}

				this.userCollections$.next([]);
			},
			async () => {
				console.error(`Could not getUserCollections of user: ${userAddress}`);
			},
		);
	}

	public async listenToUserCollectionsState(): Promise<void> {
		if (!this.userCollections$?.getValue()) {
			return;
		}

		this.checkUserCollectionsStateSavedInDatabase();
	}

	/**
	 * @dev Currently used RealtimeDB function to get (listen) SELECTED user collections
	 * @param userAddress string
	 */
	public async listenToSelectedUserCollections(userAddress: string): Promise<void> {
		if (!this.firebaseDatabaseService.database$.getValue()) {
			throw new Error('No DB at listenToSelectedUserCollections()');
		}

		if (this.selectedUserCollectionsListener) {
			await this.firebaseDatabaseService.unsubscribe(this.selectedUserCollectionsListener);
		}

		this.selectedUserCollectionsListener = this.firebaseDatabaseService.get<Record<string, ICollectionDetails>>(
			'/collections/',
			{
				orderBy: 'creator',
				equalTo: userAddress,
			},
			async (creatingCollections) => {
				if (creatingCollections) {
					const supportedChains: ChainSymbolEnum[] = [...environment.supportedChains, ...environment.isolatedChains];
					const userCollections: ICollectionDetails[] = Object.values(creatingCollections)
						.reverse()
						.filter((collection) =>
							supportedChains.includes(collection.chainSymbol) && collection.isPublic, // && collection.isVisible,
						);

					this.selectedUserCollections$.next(Object.values(userCollections));

					for (const collection of this.selectedUserCollections$.getValue()) {
						if (collection.imageURL || collection.isImageLoading) {
							return;
						}
						collection.isImageLoading = true;

						this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
							collection.imageURL = url;
						});
					}

					return;
				}

				this.selectedUserCollections$.next([]);
			},
			async () => {
				console.error(`Could not listenToSelectedUserCollections of user: ${userAddress}`);
			},
		);
	}

	public async getCollectionBySlug(slug: string): Promise<ICollectionDetails> {
		const firestore: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getCollectionBySlug()');
		}
		const queryConstraints: QueryConstraint[] = [
			where('slug', '==', slug),
		];

		const qb = query(
			colRef(firestore, 'collections'),
			...queryConstraints,
		);
		// @ts-ignore
		const querySnapshot = await getDocs<ICollectionDetails>(qb);
		const results = [];
		// @ts-ignore
		querySnapshot.forEach((doc) => {
			results.push(doc.data());
		});

		return results[0];
	}

	public async checkIsCollectionCreated(collection: ICollectionDetails): Promise<boolean> {
		if (collection?.onChainLoaders?.isCheckingIfCreated) {
			return;
		}
		collection.onChainLoaders = {
			isCheckingIfCreated: true,
		};
		const supportedChainsSymbols: ChainSymbolEnum[] = [...environment.supportedChains, ...environment.isolatedChains];

		if (!supportedChainsSymbols.includes(collection.chainSymbol)) {
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		const repository: Contract = await this.collectionsService.getRepositoryContract(collection.chainSymbol);

		if (!repository) {
			console.error(`checkIsCollectionCreated() - No ${collection.chainSymbol} repository`);
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		const userCollectionsAddresses: string[] = await repository.getAllByUser(collection.creator);
		let createdCollection: CollectionBasicChainDataType;

		for (const collectionAddress of [...userCollectionsAddresses].reverse()) {
			try {
				const collectionFromChain: CollectionBasicChainDataType = await this.collectionsService
					.getCollectionStateFromChain(collectionAddress, collection.chainSymbol);

				if (collectionFromChain.metadataURI !== collection.metadataURI) {
					continue;
				}
				createdCollection = collectionFromChain;
				break;
			} catch (e) {
				console.error(e);
			}
		}

		if (!createdCollection) {
			if (collection.status === CollectionStatusEnum.INITIATED) {
				const txHash: string | null = await this.collectionsService.getTxHashFromDatabase(`/transactions/${collection.creator}/createCollection/${collection.id}`);

				if (!txHash) {
					if (collection.reason !== 'incomplete') {
						this.setCollectionStatus(collection, CollectionStatusEnum.INITIATED, 'incomplete');
					}
					collection.onChainLoaders.isCheckingIfCreated = false;

					return;
				}
				const creatingCollectionTxResult: ContractReceipt = await this.collectionsService.getTransactionResult(
					txHash,
					collection.chainSymbol,
				);

				if (creatingCollectionTxResult.status === 1) {
					collection.onChainLoaders.isCheckingIfCreated = false;

					return;
				}
				console.error('Status set to ERROR because tx status !== 1');
				this.setCollectionStatus(collection, CollectionStatusEnum.ERROR, 'txError');
			}

			// // Handling Omnichain collection create with success tx on srcChain, but in fact not created on dstChain (e.g. error or LZ issue):
			// if (collection.status === CollectionStatusEnum.CREATING
			// 	&& moment(collection.createdAt).isBefore(moment().subtract(24, 'hours'))
			// ) {
			// 	this.setCollectionStatus(collection, CollectionStatusEnum.ERROR, 'dstChainNoCollection');
			// }
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		collection.status = CollectionStatusEnum.CREATED;
		collection.collectionAddress = createdCollection.collectionAddress;
		collection.totalMinted = createdCollection.totalMinted;

		try {
			await this.setCollectionStatus(collection, CollectionStatusEnum.CREATED);
			this.createdCollectionsFromChain$.next([
				...(this.createdCollectionsFromChain$.getValue() || []),
				collection,
			]);
			collection.onChainLoaders.isCheckingIfCreated = false;

			return true;
		} catch (e) {
			console.error(e);
		}
		collection.onChainLoaders.isCheckingIfCreated = false;

		return;
	}

	public async setCollectionPublic(collectionDetails: ICollectionDetails): Promise<void> {
		if (collectionDetails.isPublic) {
			return;
		}

		try {
			await this.firestoreService.set(
				'collections',
				collectionDetails.firestoreId,
				{
					isPublic: true,
				},
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb and add here: collectionDetails.isPublic = true;
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'isPublic'>>(
			`/collections/${collectionDetails.id}`,
			{
				isPublic: true,
			},
			async () => {
				collectionDetails.isPublic = true;
			},
			async () => {
				console.error(`Couldn\'t set creating collection: ${collectionDetails.collectionName} | ${collectionDetails.collectionAddress} to public`);
			}
		);
	}

	public async setCollectionPrivate(collectionDetails: ICollectionDetails): Promise<void> {
		if (!collectionDetails.isPublic) {
			return;
		}

		try {
			await this.firestoreService.set(
				'collections',
				collectionDetails.firestoreId,
				{
					isPublic: false,
				},
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb and add here: collectionDetails.isPublic = false;
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'isPublic'>>(
			`/collections/${collectionDetails.id}`,
			{isPublic: false},
			async () => {
				collectionDetails.isPublic = false;
			},
			async () => {
				console.error(`Couldn\'t set creating collection: ${collectionDetails.collectionName} | ${collectionDetails.collectionAddress} to private`);
			}
		);
	}

	public async setCollectionStatus(collection: ICollectionDetails, status: CollectionStatusEnum, reason?: string): Promise<void> {
		try {
			await this.firestoreService.set(
				'collections',
				collection.firestoreId,
				{status, collectionAddress: collection?.collectionAddress || '', reason: reason || ''}
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'status' | 'collectionAddress' | 'reason'>>(
			`/collections/${collection.id}`,
			{status, collectionAddress: collection?.collectionAddress || '', reason: reason || ''},
			undefined,
			async () => {
				console.error(`Couldn\'t set collection: ${collection.collectionName} | ${collection?.collectionAddress || null} status to ${status}`);
			}
		);
	}

	public async getCollectionDetails(
		id: number,
		onSuccess: (result: ICollectionDetails) => Promise<void>,
		onError: () => Promise<void>,
	): Promise<void> {
		await this.firebaseDatabaseService.getById(
			`/collections/${id}`,
			onSuccess,
			onError,
		);

		// mklmlkn.
	}

	public async getUserCollectionsCreateData(userAddress: string, isConnectedUser?: boolean): Promise<ICollectionDetails[]> {
		if (!userAddress) {
			return;
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getUserCollectionsCreateData()');
		}
		const queryConstraints: QueryConstraint[] = [
			where('creator', '==', userAddress),
		];

		if (!isConnectedUser) {
			queryConstraints.push(
				where('isPublic', '==', true),
			);
		}
		const qb = query(
			colRef(db, 'collections'),
			...queryConstraints,
		);

		// @ts-ignore
		const querySnapshot = await getDocs<ICollectionDetails>(qb);
		const collectionsCreateData: ICollectionDetails[] = [];

		querySnapshot.forEach((doc) => {
			collectionsCreateData.push(doc.data());
		});

		return collectionsCreateData;
	}

	private async isCollectionActive(collection: ICollectionDetails): Promise<boolean> {
		const from: number = collection.dropFrom;
		const to: number = collection.dropTo;
		const isOngoing: boolean = (!from || (moment().unix() >= from && moment().unix() <= to));

		if (!collection.totalSupply) {
			return isOngoing;
		}

		// TODO: Query via Firestore after implementing mechanism for storing current totalMinted
		return collection.totalSupply > collection.totalMinted;
	}

	private checkUserCollectionsStateSavedInDatabase(): void {
		for (const userCollection of this.userCollections$.getValue()) {
			if (![CollectionStatusEnum.INITIATED, CollectionStatusEnum.CREATING].includes(userCollection.status)) {
				continue;
			}
			this.checkIsCollectionCreated(userCollection).then((isCreated) => {
				if (isCreated) {
					return;
				}

				const collectionStatusInterval: NodeJS.Timer = setInterval(() => {
					this.checkIsCollectionCreated(userCollection).then((isFinallyCreated) => {
						if (!isFinallyCreated) {
							return;
						}

						clearInterval(collectionStatusInterval);
					});
				}, 30000);
			});
		}
	}
}
