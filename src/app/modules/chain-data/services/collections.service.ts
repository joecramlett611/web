import { Injectable } from '@angular/core';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, ContractReceipt, ContractTransaction, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { Contract } from '@ethersproject/contracts';
import { environment } from '../../../../environments/environment';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import * as moment from 'moment';
import { CollectionEarningListType } from '../../collections/types/collection-earning-list.type';
import { CollectionRefundsListType } from '../../collections/types/collection-refunds-list.type';
import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';
import { IMintTx } from '../../collections/interfaces/mint-tx.interface';
import { CollectionBasicChainDataType } from '../types/collection-basic-chain-data.type';
import FirestoreService from '../../firebase/services/firestore.service';
import { doc, Firestore, getDoc } from '@firebase/firestore';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import { OmnichainActionTypeEnum } from '../../omnichain/enums/omnichain-action-type.enum';
import OmnichainService from '../../omnichain/services/omnichain.service';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { AptosClient } from 'aptos';
import { IMintRules } from '../../aptos/interfaces/mint-rules.interface';
import { IAllowlist } from '../../aptos/interfaces/allowlist.interface';

@Injectable()
export default class CollectionsService {
	public readonly collectionFactoryContractAbi: string[] = [
		'function create(string memory chSym, string memory collName, string memory collURI, string memory fileURI, uint256 price, uint256 from, uint256 to, string[] memory URIs, uint gas) public payable',
		'function estimateFees(string memory collName, string memory chSym, string memory URI, string memory fileURI, string[] memory URIs, uint gas) external view returns (uint)',
		'function repository() public view returns (address)',
		'event Created(address collectionAddress)',
	];
	public readonly omniNftAbi: string[] = [
		'function createdAt() external view returns (uint256)',
		'function creator() external view returns (address)',
		'function getMintedBy(address addr) public view returns (uint256[] memory)',
		'function collectionName() external view returns (string memory)',
		'function hidden() external view returns (bool)',
		'function dropFrom() external view returns (uint256)',
		'function dropTo() external view returns (uint256)',
		'function collectionURI() external view returns (string memory)',
		'function mintPrice() external view returns (uint256)',
		'function totalSupply() external view returns (uint256)',
		'function totalMinted() external view returns (uint256)',
		'function tokenIds() external view returns (uint256)',
		'function tokenURI(uint256 tokenId) external view returns (string memory)',
		'function addMetadataURIs(string[] memory _newMetadataURIs) external',
		'function setAllowlist(bytes calldata addresses, uint256 maxPerAddress, uint256 maxPerAddressPublic, uint256 publicFrom, bool isEnabled) external',
		'function allowlist() external view returns (uint256 maxPerAddress, uint256 maxPerAddressPublic, uint256 publicFrom, bool isEnabled)',
		'function isAllowlisted(address account) external view returns (bool)',
	];
	public readonly collectionsRepositoryContractAbi: string[] = [
		'function getById(uint256 _tokenId) public view returns (IOmniERC721 collection)',
		'function getAllByUser(address user) external view returns (address[] memory)',
	];
	public readonly tokenFactoryContractAbi: string[] = [
		'function refunds(address coll, string memory dstChainName, address spender) public view returns (uint256)',
		'function mints(address coll, string memory dstChainName) public view returns (uint256)',
		'function getEarned(address coll, string memory _dstChainName, uint256 gas, uint256 redirectFee) external payable',
		'function refund(address coll, string memory _dstChainName, uint256 gas, uint256 redirectFee) external payable',
		'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
	];
	private mintTxFirestoreId: string;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firestoreService: FirestoreService,
		private readonly omnichainService: OmnichainService,
	) {
	}

	public async getEVMCollectionMintedCount(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		tokenFactoryVersion?: number,
	): Promise<number> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);

		if (tokenFactoryVersion >= 3) {
			return (await contract.totalMinted()).toNumber();
		}

		return (await contract.tokenIds()).toNumber();
	}

	public async getCollectionStateFromChain(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<CollectionBasicChainDataType> {
		fallbackId = fallbackId || 0;
		try {
			const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol, fallbackId);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const metadataURI: string = await contract.collectionURI();
			const totalMinted: number = (await contract.totalMinted()).toNumber();

			return {
				collectionAddress,
				metadataURI,
				totalMinted,
			};
		} catch (e) {
			console.error(e);
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getCollectionStateFromChain(
						collectionAddress,
						chainSymbol,
						fallbackId,
					);
				}
			}

			throw new Error(e);
		}
	}

	public async isCollectionCreated(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
	): Promise<boolean> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
		const createdAt: BigNumber | undefined = await contract.createdAt();

		return createdAt && (createdAt.toNumber() > 0);
	}

	public async getCollectionContractOnSelectedNetwork(
		contractAddress: string,
		chainSymbol: ChainSymbolEnum,
	): Promise<Contract> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);

		return new ethers.Contract(contractAddress, this.omniNftAbi, provider);
	}

	public chainSymbolToCollectionFactoryAddressMap(chainSymbol: ChainSymbolEnum): string {
		return environment.collectionFactoryAddressesMap[chainSymbol];
	}

	public async getCollectionFactoryContractOnSelectedNetwork(
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<Contract> {
		try {
			const contractAddress: string = environment.collectionFactoryAddressesMap[chainSymbol];
			const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol, fallbackId);

			return new ethers.Contract(contractAddress, this.collectionFactoryContractAbi, provider);
		} catch (e) {
			console.error(e);
		}
	}

	public async getRepositoryContract(chainSymbol: ChainSymbolEnum, fallbackId?: number): Promise<Contract> {
		fallbackId = fallbackId || 0;

		try {
			const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol, fallbackId);
			const collectionFactoryAddress: string = environment.collectionFactoryAddressesMap[chainSymbol];
			if (!collectionFactoryAddress) {
				return;
			}
			const factoryContract: Contract = new ethers.Contract(collectionFactoryAddress, this.collectionFactoryContractAbi, provider);
			const repositoryAddress: string = await factoryContract.repository();

			return new ethers.Contract(repositoryAddress, this.collectionsRepositoryContractAbi, provider);
		} catch (e) {
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getRepositoryContract(chainSymbol, fallbackId);
				}
			}

			console.error(e);
			return;
		}
	}

	public async getTokenFactoryContract(
		chainSymbol: ChainSymbolEnum,
		tokenFactoryVersion: number,
		fallbackId?: number,
		isWrite?: boolean,
	): Promise<Contract> {
		fallbackId = fallbackId || 0;

		try {
			const tokenFactoryAddress: string =
				environment.tokenFactoryMap[tokenFactoryVersion || 1][chainSymbol];

			if (!tokenFactoryAddress) {
				return;
			}
			return new ethers.Contract(
				tokenFactoryAddress,
				this.tokenFactoryContractAbi,
				isWrite ? this.web3Service.getEVMProvider().getSigner() : this.web3Service.getEVMNetworkProvider(chainSymbol, fallbackId),
			);
		} catch (e) {
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getTokenFactoryContract(chainSymbol, tokenFactoryVersion, fallbackId);
				}
			}

			console.error(e);
			return;
		}
	}

	public async getCollectionById(collectionId: string): Promise<ICollectionDetails> {
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const ref = doc(db, `collections/${collectionId}`);
		// @ts-ignore
		const docSnap = await getDoc<ICollectionDetails>(ref);

		return docSnap.exists() ? docSnap.data() : undefined;
	}

	public async saveCollectionInDatabase(creatorAddress: string, collectionDetails: ICollectionDetails): Promise<void> {
		const timestamp: number = moment().unix();

		await this.firebaseDatabaseService.save<ICollectionDetails>(
			`/collections/${collectionDetails.id}`,
			collectionDetails,
			undefined,
			async () => {
				throw new Error(`Cannot store collection of user: ${creatorAddress} at: ${timestamp}`);
			}
		);
	}

	public async getRefundsList(
		collection: ICollectionDetails,
		spenderAddress: string,
	): Promise<Partial<CollectionRefundsListType>> {
		const refundsList = {};
		const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, collection.tokenFactoryVersion);

		if (!contract) {
			return refundsList;
		}

		for (const chainSymbol of environment.supportedChains) {
			if (chainSymbol === collection.chainSymbol) {
				continue;
			}

			const chainRefund: BigNumber | undefined = await contract.refunds(
				collection.collectionAddress,
				chainSymbol,
				spenderAddress,
			);


			refundsList[chainSymbol] = chainRefund.gt(0) ? Number(ethers.utils.formatUnits(chainRefund, 6)) : 0;
		}

		return refundsList;
	}

	public async getEarningsList(
		collection: ICollectionDetails,
		mintPrice: number,
	): Promise<Partial<CollectionEarningListType>> {
		const earningsList = {};
		const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, collection.tokenFactoryVersion);

		if (!contract) {
			return earningsList;
		}

		for (const chainSymbol of environment.supportedChains) {
			if (chainSymbol === collection.chainSymbol) {
				continue;
			}

			const chainMintsCount: BigNumber | undefined = await contract.mints(
				collection.collectionAddress,
				chainSymbol,
			);

			earningsList[chainSymbol] = (chainMintsCount?.toNumber() || 0) * mintPrice;
		}

		return earningsList;
	}

	public async claimEarned(collection: ICollectionDetails, dstChainSymbol: ChainSymbolEnum): Promise<boolean> {
		try {
			const userAddress: string = await this.web3Service.getEVMProvider().getSigner().getAddress();
			const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, collection.tokenFactoryVersion, 0, true);
			const encodedFnData: string = this.web3Service.encodePayload(
				[
					2,
					collection.collectionAddress,
					true,
					collection.mintPrice,
					userAddress,
					collection.creator,
					collection.assetName,
				],
				['uint', 'address', 'bool', 'uint', 'address', 'address', 'string'],
			);
			const encodedPayload: string = this.web3Service.encodePayload(
				[
					dstChainSymbol,
					userAddress || '0xMock',
					encodedFnData,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for CLAIM_EARNED action
					userAddress || '0xMock',
					dstChainSymbol,
					userAddress || '0xMock',
					0,
				],
				['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
			);
			const estimatedFees = await this.omnichainService.getEstimatedFees(
				dstChainSymbol,
				encodedPayload,
				OmnichainActionTypeEnum.MINT_TOKEN, // TODO: measure and set gas used for CLAIM_EARNED action
			);
			const tx: ContractTransaction = await contract.getEarned(
				collection.collectionAddress,
				dstChainSymbol,
				environment.chainSymbolToMintTokenGas[dstChainSymbol],
				estimatedFees.redirectFee,
				{value: estimatedFees.fee},
			);

			const srcChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();
			const at: number = moment().unix();
			let claimTxData: IClaimEarnedTx = {
				collectionId: collection.id,
				collectionName: collection.collectionName,
				srcChainSymbol,
				dstChainSymbol,
				price: collection.mintPrice,
				assetName: collection.assetName,
				at,
				txHash: tx.hash,
			};
			let claimTxFirestoreId: string;

			try {
				const res = await this.firestoreService.add<IClaimEarnedTx>(
					'transactions',
					[userAddress, 'claimEarned'],
					claimTxData,
				);

				claimTxFirestoreId = res.id;
			} catch (e) {
				console.error(e);
			}

			const txResult: ContractReceipt = await tx.wait();
			claimTxData = {
				collectionId: collection.id,
				collectionName: collection.collectionName,
				srcChainSymbol,
				dstChainSymbol,
				at,
				price: collection.mintPrice,
				assetName: collection.assetName,
				txHash: tx.hash,
				status: txResult?.status || null,
			};

			try {
				this.firestoreService.set<IClaimEarnedTx>(
					`transactions/${userAddress}/claimEarned`,
					claimTxFirestoreId,
					claimTxData,
				);
			} catch (e) {
				console.error(e);
			}

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	public async refund(collection: ICollectionDetails, dstChainSymbol: ChainSymbolEnum): Promise<boolean> {
		try {
			const userAddress: string = await this.web3Service.getEVMProvider().getSigner().getAddress();
			const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, collection.tokenFactoryVersion, 0, true);
			const encodedFnData: string = this.web3Service.encodePayload(
				[
					2,
					collection.collectionAddress,
					false,
					collection.mintPrice,
					userAddress,
					userAddress,
					collection.assetName,
				],
				['uint', 'address', 'bool', 'uint', 'address', 'address', 'string'],
			);
			const encodedPayload: string = this.web3Service.encodePayload(
				[
					dstChainSymbol,
					userAddress || '0xMock',
					encodedFnData,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: (must) measure and set gas used for REFUND action
					userAddress || '0xMock',
					dstChainSymbol,
					userAddress || '0xMock',
					0,
				],
				['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
			);
			const estimatedFees = await this.omnichainService.getEstimatedFees(
				dstChainSymbol,
				encodedPayload,
				OmnichainActionTypeEnum.MINT_TOKEN, // TODO: measure and set gas used for REFUND action
			);
			const tx: ContractTransaction = await contract.refund(
				collection.collectionAddress,
				dstChainSymbol,
				environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for REFUND action
				estimatedFees.redirectFee,
				{value: estimatedFees.fee},
			);
			const srcChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();
			const at: number = moment().unix();
			let claimRefundTxData: IClaimRefundTx = {
				collectionId: collection.id,
				collectionName: collection.collectionName,
				srcChainSymbol,
				dstChainSymbol,
				at,
				price: collection.mintPrice,
				assetName: collection.assetName,
				txHash: tx.hash,
			};
			let txFirestoreId: string;

			try {
				const res = await this.firestoreService.add<IClaimEarnedTx>(
					'transactions',
					[userAddress, 'refund'],
					claimRefundTxData,
				);

				txFirestoreId = res.id;
			} catch (e) {
				console.error(e);
			}

			const txResult: ContractReceipt = await tx.wait();

			claimRefundTxData = {
				collectionId: collection.id,
				collectionName: collection.collectionName,
				srcChainSymbol,
				dstChainSymbol,
				at,
				price: collection.mintPrice,
				assetName: collection.assetName,
				status: txResult?.status || null,
				txHash: tx.hash,
			};
			try {
				this.firestoreService.set<IClaimEarnedTx>(
					`transactions/${userAddress}/refund`,
					txFirestoreId,
					claimRefundTxData,
				);
			} catch (e) {
				console.error(e);
			}

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	public async getTransactionResult(txHash: string, chainSymbol: ChainSymbolEnum): Promise<ContractReceipt> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);

		return await provider.getTransactionReceipt(txHash);
	}

	public getTxHashFromDatabase(path: string): Promise<string | null> {
		return new Promise((resolve, reject) => {
			this.firebaseDatabaseService.getById<Pick<ContractTransaction, 'hash'>>(
				path,
				async (tx: Pick<ContractTransaction, 'hash'>) => {
					resolve(tx?.hash || null);
					return;
				},
				async () => {
					console.error('Could not getTxHashFromDatabase() for path: ', path);
					reject();
				}
			);
		});
	}

	public async saveMintTx(
		collection: ICollectionDetails,
		userAddress: string,
		txHash: string,
		status?: number,
	): Promise<void> {
		const mintTxData: IMintTx = {
			collectionId: collection.id,
			srcChainSymbol: this.web3Service.getConnectedChainSymbol(),
			dstChainSymbol: collection.chainSymbol,
			txHash,
			at: moment().unix(),
			status: status || null,
		};

		try {
			if (!this.mintTxFirestoreId) {
				const res = await this.firestoreService.add<IMintTx>(
					'transactions',
					[userAddress, 'mint'],
					mintTxData,
				);

				this.mintTxFirestoreId = res.id;
			} else {
				this.firestoreService.set<IMintTx>(
					`transactions/${userAddress}/mint`,
					this.mintTxFirestoreId,
					mintTxData,
				);
			}

		} catch (e) {
			console.error(e);
		}

		try {
			this.firebaseDatabaseService.save<IMintTx>(
				`/transactions/${userAddress}/mint/${collection.collectionAddress || collection.collectionName}/${txHash}`,
				mintTxData,
			);
		} catch (e) {
			console.error(e);
		}
	}

	public async incrementCollectionMintsCount(
		collection: ICollectionDetails,
		userAddress: string,
		mintPrice: number,
		txHash: string,
	): Promise<void> {
		try {
			this.firebaseDatabaseService.increment(`/stats/collections/${collection.id}/mint`);
		} catch (e) {
			console.error(e);
		}

		try {
			await this.firestoreService.add<IMintData>(
				'mints',
				[],
				{
					collectionId: collection.firestoreId,
					collectionName: collection.collectionName,
					collectionFileURI: collection.fileURI,
					userAddress,
					at: moment().unix(),
					price: mintPrice,
					txHash,
				},
			);
		} catch (e) {
			console.error(e);
		}
	}

	public async getCollectionMintsCountFromDatabase(
		collectionId: number,
		onSuccess: (mintCount: number) => Promise<void>,
	): Promise<void> {
		try {
			this.firebaseDatabaseService.getById<number>(
				`/stats/collections/${collectionId}/mint`,
				async (mintCount: number) => {
					onSuccess(mintCount);
				},
			);
		} catch (e) {
			console.error(e);
		}
	}

	public async setWhitelistForEVMCollection(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		addresses: string[],
		maxPerAddress: number,
		maxPerAddressPublic: number,
		publicFrom: number,
	): Promise<ContractReceipt> {
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider().getSigner();
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, signer);

		const abiCoder = ethers.utils.defaultAbiCoder;
		const addressesAsBytes = abiCoder.encode(
			['address[]'],
			[addresses]); // array to encode

		const tx: ContractTransaction = await contract.setAllowlist(
			addressesAsBytes,
			maxPerAddress,
			maxPerAddressPublic,
			publicFrom,
			true,
		);

		return await tx.wait();
	}

	public async isAllowlistedInEVMCollection(collectionAddress: string, chainSymbol: ChainSymbolEnum): Promise<boolean> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
		const userAddress: string = await this.web3Service.getUserAddress();

		return await contract.isAllowlisted(userAddress);
	}

	public async getEVMAllowlist(collectionAddress: string, chainSymbol: ChainSymbolEnum): Promise<IAllowlist> {
		const provider: BaseProvider = this.web3Service.getEVMNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
		const allowlistData: {
			isEnabled: boolean,
			[0]: BigNumber,
			[1]: BigNumber,
			[2]: BigNumber,
		} = await contract.allowlist();

		if (!allowlistData?.isEnabled) {
			return {
				addresses: [],
				isEnabled: false,
				maxPerAddress: 0,
				publicMaxPerAddress: 0,
				publicFrom: 0,
			};
		}

		return {
			addresses: [],
			isEnabled: allowlistData.isEnabled,
			maxPerAddress: allowlistData[0].toNumber(),
			publicMaxPerAddress: allowlistData[1].toNumber(),
			publicFrom: allowlistData[2].toNumber(),
		};
	}

	public async getAptosAllowlist(creator: string, collectionName: string, moduleVersion: number): Promise<IAllowlist> {
		const aptosClient = new AptosClient(environment.aptosSpecific.api.uri);
		const resources = await aptosClient.getAccountResources(environment.aptosSpecific.deployer);
		const launchpadMetadataResource = resources.find(
			(resource) => resource.type === environment.aptosSpecific.resources.launchpadMetadata.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
		);
		// @ts-ignore
		const { handle } = launchpadMetadataResource.data.collection_mint_rules;
		const getAllowlistTableItemRequest = {
			key_type: environment.aptosSpecific.types.collectionIdentifier.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
			value_type: environment.aptosSpecific.types.allowlist.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
			key: {
				creator,
				name: collectionName,
			},
		};
		const mintRules = await aptosClient.getTableItem(handle, getAllowlistTableItemRequest);

		if (!mintRules.allowlist?.is_enabled) {
			return {
				addresses: [],
				isEnabled: false,
				isAllowlisted: false,
				publicMaxPerAddress: 0,
				maxPerAddress: 0,
				publicFrom: 0,
			};
		}

		return {
			addresses: mintRules.allowlist.addresses,
			isEnabled: mintRules.allowlist.is_enabled,
			maxPerAddress: mintRules.allowlist.max_per_address,
			publicMaxPerAddress: mintRules.allowlist.public_max_per_address,
			publicFrom: mintRules.allowlist.public_from,
		};
	}
}
