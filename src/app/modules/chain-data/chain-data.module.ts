import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import CollectionsService from './services/collections.service';
import CollectionsDataService from './services/collections-data.service';

@NgModule({
	providers: [
		CollectionsService,
		CollectionsDataService,
	],
	imports: [
		CommonModule,
	],
})
export class ChainDataModule {
}
