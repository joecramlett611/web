import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';

export type CollectionBasicChainDataType = Pick<ICollectionDetails, 'metadataURI' | 'totalMinted' | 'fileURI' | 'collectionAddress'>;
