export interface AcceleratorApplicationDto {
	fullName: string;
	email: string;
	experience?: string;
	mainCollectionURL: string;
	userAddress: string;
}
