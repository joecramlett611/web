import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import FirebaseService from '../../../firebase/services/firebase.service';
import { Subscription } from 'rxjs';
import NotificationsService from '../../../shared/services/notifications.service';
import { AcceleratorApplicationDto } from '../../dto/accelerator-application.dto';
import Web3Service from '../../../web3/services/web3.service';
import ProfileService from '../../../user/services/profile.service';
import { IProfile } from '../../../user/interfaces/profile.interface';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import FirestoreService from '../../../firebase/services/firestore.service';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';

@Component({
	selector: 'app-accelerator-form',
	templateUrl: './accelerator-form.component.html',
	styleUrls: ['./accelerator-form.component.scss']
})
export class AcceleratorFormComponent implements OnInit, OnDestroy {
	public form: FormGroup;
	public isConnectedToWallet: boolean;
	public profile: IProfile;
	public isSubmitted: boolean;
	public isSubmitting: boolean;
	public hasRequiredCollection: boolean;
	private application: AcceleratorApplicationDto;
	private subscriptions: Subscription;
	private userAddress: string;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly firebaseService: FirebaseService,
		private readonly firestoreService: FirestoreService,
		private readonly notificationsService: NotificationsService,
		private readonly web3Service: Web3Service,
		private readonly profileService: ProfileService,
		private readonly collectionsDataService: CollectionsDataService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.form = this.formBuilder.group({
			fullName: ['', [Validators.required, Validators.maxLength(250)]],
			email: ['', [Validators.required, Validators.email, Validators.maxLength(250)]],
			experience: ['', [Validators.maxLength(2000)]],
			mainCollectionURL: ['', [Validators.required, Validators.maxLength(250)]],
			terms: [false, Validators.requiredTrue],
		});

		this.subscriptions.add(
			this.web3Service.connectedWallet$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isWalletConnected();

					if (this.isConnectedToWallet) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						this.profileService.getProfile(this.userAddress).then((profile) => {
							this.profile = profile;
						});
						this.subscriptions.add(
							this.collectionsDataService.userCollections$.subscribe((userCollections: ICollectionDetails[]) => {
								if (userCollections === undefined) {
									return;
								}
								const minLengthCollection: ICollectionDetails = userCollections.find((collection) => {
									return collection.totalSupply >= 50
										&& (
											(collection.assetName === SupportedAssetEnum.USDC && collection.mintPrice >= 5)
											|| (collection.assetName === SupportedAssetEnum.OSEA && collection.mintPrice >= 5000)
										);
								});

								this.hasRequiredCollection = !!minLengthCollection;
							}),
						);
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public onSubmit(): void {
		if (!this.firestoreService.firestore$.getValue()) {
			return;
		}

		if (this.form.invalid) {
			return;
		}
		this.isSubmitting = true;

		this.application = this.getApplicationData();
		this.firestoreService.add<AcceleratorApplicationDto>(
			'accelerator',
			[],
			this.application,
			async (result) => {
				this.isSubmitting = false;
				this.notificationsService.success('Thank you 😊', 'Your application has been sent');
				this.isSubmitted = true;
			},
			async () => {
				this.isSubmitting = false;
				this.notificationsService.error('Sorry', 'We couldn\'t send your application. Please, try again');
			},
		);
	}

	public scrollToForm() {
		document.getElementById('acceleratorForm').scrollIntoView({behavior: 'smooth'});
	}

	public connectWallet(): void {
		this.web3Service.selectWalletType();
	}

	private getApplicationData(): AcceleratorApplicationDto {
		return {
			email: this.form.get('email').value,
			fullName: this.form.get('fullName').value,
			experience: this.form.get('experience').value,
			mainCollectionURL: this.form.get('mainCollectionURL').value,
			userAddress: this.userAddress,
		};
	}
}
