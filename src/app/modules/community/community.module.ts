import { NgModule } from '@angular/core';
import { CommunityRoutingModule } from './community-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AcceleratorFormComponent } from './components/accelerator-form/accelerator-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		AcceleratorFormComponent,
	],
	imports: [
		CommunityRoutingModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
	],
	exports: [
		AcceleratorFormComponent,
	],
})
export class CommunityModule {
}
