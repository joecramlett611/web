import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceleratorFormComponent } from './components/accelerator-form/accelerator-form.component';

const routes: Routes = [
	{
		path: '',
		component: AcceleratorFormComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CommunityRoutingModule {
}
