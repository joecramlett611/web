import { Routes } from '@angular/router';

export const communityRoutes: Routes = [
	{
		path: 'accelerator',
		loadChildren: () => import('./community.module').then(m => m.CommunityModule),
		data: { preload: false },
	},
];
