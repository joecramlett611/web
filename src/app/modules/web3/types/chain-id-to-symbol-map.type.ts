import { ChainSymbolEnum } from '../enum/chain-symbol.enum';

export type ChainIdToSymbolMap = Record<number, ChainSymbolEnum>;
