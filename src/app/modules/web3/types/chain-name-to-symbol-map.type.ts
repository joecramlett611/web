import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../enum/supported-chain-name.enum';

export type ChainNameToSymbolMap = Record<SupportedChainNameEnum, ChainSymbolEnum>;
