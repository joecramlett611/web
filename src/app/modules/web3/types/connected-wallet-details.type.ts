import { WalletTypeEnum } from '../enum/wallet-type.enum';
import { WalletProviderEnum } from '../enum/wallet-provider.enum';

export type ConnectedWalletDetails = {
	type: WalletTypeEnum,
	provider: WalletProviderEnum,
	isConnected: boolean,
	address: string,
};
