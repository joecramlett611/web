import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { Network } from '@ethersproject/networks';

export type ChainSymbolToNetworkMap = Record<ChainSymbolEnum, Network[]>;
