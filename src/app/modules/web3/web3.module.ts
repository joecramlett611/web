import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Web3ModalModule } from '@mindsorg/web3modal-angular';
import NetworksService from './services/networks.service';
import { AptosModule } from '../aptos/aptos.module';
import { SuiModule } from '../sui/sui.module';

@NgModule({
	providers: [
		NetworksService,
	],
	imports: [
		CommonModule,
		Web3ModalModule,
		AptosModule,
		SuiModule,
	],
	exports: [
		Web3ModalModule,
		AptosModule,
		SuiModule,
	],
})
export class Web3Module {
}
