export interface INetworkTaskState {
	status?: 'completed' | 'error' | 'pending';
	reason?: string;
	onRetry?: (onRetrySuccess?: (data) => Promise<void>) => Promise<void>;
	progress?: number;
}
