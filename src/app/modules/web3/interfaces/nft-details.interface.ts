import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';
import { NftAttributes } from '../../collections/types/nft-attributes.type';

export interface INFTDetails {
	tokenId?: number;
	name?: string;
	description?: string;
	file?: File;
	fileIpfsHash?: string;
	smFilePath?: string;
	smFileIpfsHash?: string; // DEPRECATED
	fileStoragePath?: string;
	attributes?: NftAttributes;
	fileURL?: string | ArrayBuffer;
	contentType?: ContentTypeEnum;
	isFileLoaded?: boolean;
	collectionId?: number;
	collectionName?: string;
	collectionAddress?: string;
	collectionMetadataURI?: string;
	chainSymbol?: ChainSymbolEnum;
}

