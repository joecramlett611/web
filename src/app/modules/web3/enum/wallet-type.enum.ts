export enum WalletTypeEnum {
	EVM = 'EVM',
	APTOS = 'APTOS',
	SUI = 'SUI',
}
