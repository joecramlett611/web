export enum TxTypeEnum {
	createCollection = 'createCollection',
	mint = 'mint',
	claimEarned = 'claimEarned',
	refund = 'refund',
}
