export enum CollectionStatusEnum {
	INITIATED = 'INITIATED',
	CREATING = 'CREATING',
	CREATED = 'CREATED',
	DELETED = 'DELETED',
	ERROR = 'ERROR',
}
