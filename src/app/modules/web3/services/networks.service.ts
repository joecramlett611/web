import { Injectable } from '@angular/core';
import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { environment } from '../../../../environments/environment';
import { ChainSymbolToNetworkMap } from '../types/chain-symbol-to-network-map.type';
import { Network } from '@ethersproject/networks';

@Injectable()
export default class NetworksService {
	public get chainSymbolToNetworkMap(): ChainSymbolToNetworkMap {
		return environment.chainSymbolToNetworkMap;
	}

	public getNetworkByChainSymbol(chainSymbol: ChainSymbolEnum, fallbackId?: number): Network {
		return this.chainSymbolToNetworkMap[chainSymbol][fallbackId || 0];
	}

	public getMAINNETNetworkByChainSymbol(chainSymbol: ChainSymbolEnum, fallbackId?: number): Network {
		return environment.MAINNETChainSymbolToNetworkMap[chainSymbol][fallbackId || 0];
	}
}
