import { Injectable } from '@angular/core';
import { Web3ModalService } from '@mindsorg/web3modal-angular';
import { Web3Provider } from '@ethersproject/providers';
import { BehaviorSubject } from 'rxjs';
import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { environment } from '../../../../environments/environment';
import { ChainIdToSymbolMap } from '../types/chain-id-to-symbol-map.type';
import NetworksService from './networks.service';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, ethers } from 'ethers';
import { Contract } from '@ethersproject/contracts';
import { ExternalProvider } from '@ethersproject/providers/src.ts/web3-provider';
import { WalletTypeEnum } from '../enum/wallet-type.enum';
import AptosMartianWalletService from '../../aptos/services/aptos-martian-wallet.service';
import { WalletProviderEnum } from '../enum/wallet-provider.enum';
import { ConnectedWalletDetails } from '../types/connected-wallet-details.type';
import GlobalStateService from '../../shared/global-state.service';
import { GlobalActionTypeEnum } from '../../shared/enums/global-action-type.enum';
import AptosPetraWalletService from '../../aptos/services/aptos-petra-wallet.service';
// import SuiSuietWalletService from '../../sui/services/sui-suiet-wallet.service';
// import SuiEthosWalletService from '../../sui/services/sui-ethos-wallet.service';
// import { SuiEthosWalletConnection } from '../../sui/interfaces/sui-ethos-wallet-connection.interface';
// import SuiNetworkService from '../../sui/services/sui-network.service';
// import { SuietWalletConnection } from '../../../react/components/react-container/react-container.component';
import AptosPontemWalletService from '../../aptos/services/aptos-pontem-wallet.service';

@Injectable()
export default class Web3Service {
	public get chainIdToSymbolMap(): ChainIdToSymbolMap {
		return environment.chainIdToSymbolMap;
	}

	public networksProviders$: Partial<Record<ChainSymbolEnum, BehaviorSubject<BaseProvider>>>;
	public MAINNETNetworksProviders$: Partial<Record<ChainSymbolEnum, BehaviorSubject<BaseProvider>>>;
	public readonly evmProvider$: BehaviorSubject<Web3Provider> = new BehaviorSubject<Web3Provider>(undefined);
	public readonly connectedWallet$: BehaviorSubject<WalletTypeEnum> = new BehaviorSubject<WalletTypeEnum>(undefined);
	public readonly connectedProvider$: BehaviorSubject<WalletProviderEnum> = new BehaviorSubject<WalletProviderEnum>(undefined);
	public readonly networkId$: BehaviorSubject<number> = new BehaviorSubject<number>(null);
	private readonly erc20Abi: string[] = [
		'function balanceOf(address account) view returns (uint256)',
	];

	constructor(
		private readonly globalStateService: GlobalStateService,
		private readonly web3ModalService: Web3ModalService,
		private readonly networksService: NetworksService,
		private readonly aptosMartianWalletService: AptosMartianWalletService,
		private readonly aptosPetraWalletService: AptosPetraWalletService,
		private readonly aptosPontemWalletService: AptosPontemWalletService,
		// private readonly suiSuietWalletService: SuiSuietWalletService,
		// private readonly suiEthosWalletService: SuiEthosWalletService,
		// private readonly suiNetworkService: SuiNetworkService,
	) {
		this.networksProviders$ = {
			[ChainSymbolEnum.eth]: undefined,
			[ChainSymbolEnum.bsc]: undefined,
			[ChainSymbolEnum.avax]: undefined,
			[ChainSymbolEnum.polygon]: undefined,
			[ChainSymbolEnum.arb]: undefined,
			[ChainSymbolEnum.optimism]: undefined,
			[ChainSymbolEnum.ftm]: undefined,
			[ChainSymbolEnum.moonbeam]: undefined,
			[ChainSymbolEnum.harmony]: undefined,
			[ChainSymbolEnum.evmos]: undefined,
			[ChainSymbolEnum.gate]: undefined,
			[ChainSymbolEnum.oasis]: undefined,
		};
		this.MAINNETNetworksProviders$ = {
			[ChainSymbolEnum.eth]: undefined,
			[ChainSymbolEnum.bsc]: undefined,
			[ChainSymbolEnum.avax]: undefined,
			[ChainSymbolEnum.polygon]: undefined,
			[ChainSymbolEnum.arb]: undefined,
			[ChainSymbolEnum.optimism]: undefined,
			[ChainSymbolEnum.ftm]: undefined,
			[ChainSymbolEnum.moonbeam]: undefined,
			[ChainSymbolEnum.harmony]: undefined,
			[ChainSymbolEnum.evmos]: undefined,
			[ChainSymbolEnum.gate]: undefined,
			[ChainSymbolEnum.oasis]: undefined,
		};
		const supportedChains: ChainSymbolEnum[] = [...environment.supportedChains, ...environment.isolatedChains];

		for (const chain of supportedChains) {
			this.networksProviders$[chain] = new BehaviorSubject<BaseProvider>(undefined);
			this.MAINNETNetworksProviders$[chain] = new BehaviorSubject<BaseProvider>(undefined);
		}

		this.evmProvider$.subscribe((provider) => {
			if (!provider) {
				return;
			}

			provider.on('network', (newNetwork, oldNetwork) => {
				if (oldNetwork && !this.isProviderTorus()) {
					this.connectedWallet$.next(WalletTypeEnum.EVM);
					// window.location.reload();
				}
			});

			setInterval(() => {
				if (this.evmProvider$.getValue() && !this.isWalletUnlocked()) {
					this.evmProvider$.next(undefined);
				}
			}, 500);
		});

		// this.suiEthosWalletService.connection$.subscribe((connection) => {
		// 	if (!connection) {
		// 		return;
		// 	}
		//
		// 	this.connectedProvider$.next(WalletProviderEnum.SUI_ETHOS);
		// 	this.connectedWallet$.next(WalletTypeEnum.SUI);
		// });
		//
		// this.suiSuietWalletService.connection$.subscribe((connection) => {
		// 	if (!connection) {
		// 		return;
		// 	}
		//
		// 	this.connectedProvider$.next(WalletProviderEnum.SUI_SUIET);
		// 	this.connectedWallet$.next(WalletTypeEnum.SUI);
		// });

		this.aptosMartianWalletService.connection$.subscribe((connection) => {
			if (!connection) {
				return;
			}

			this.connectedProvider$.next(WalletProviderEnum.APTOS_MARTIAN);
			this.connectedWallet$.next(WalletTypeEnum.APTOS);
		});

		this.aptosPetraWalletService.connection$.subscribe((connection) => {
			if (!connection) {
				return;
			}

			this.connectedProvider$.next(WalletProviderEnum.APTOS_PETRA);
			this.connectedWallet$.next(WalletTypeEnum.APTOS);
		});

		this.aptosPontemWalletService.connection$.subscribe((connection) => {
			if (!connection) {
				return;
			}

			this.connectedProvider$.next(WalletProviderEnum.APTOS_PONTEM);
			this.connectedWallet$.next(WalletTypeEnum.APTOS);
		});

		this.evmProvider$.subscribe((connection) => {
			if (!connection) {
				return;
			}

			this.connectedProvider$.next(this.isProviderTorus() ? WalletProviderEnum.TORUS : WalletProviderEnum.METAMASK);
			this.connectedWallet$.next(WalletTypeEnum.EVM);
		});
	}

	public async connect(walletType: WalletTypeEnum | undefined, walletProvider: WalletProviderEnum | undefined): Promise<void> {
		switch (walletType) {
			case WalletTypeEnum.EVM:
			default:
				return this.connectEVMWalletWithModal();
			case WalletTypeEnum.APTOS:
				if (walletProvider === WalletProviderEnum.APTOS_MARTIAN) {
					return this.aptosMartianWalletService.connect();
				}
				if (walletProvider === WalletProviderEnum.APTOS_PETRA) {
					return this.aptosPetraWalletService.connect();
				}
				if (walletProvider === WalletProviderEnum.APTOS_PONTEM) {
					return this.aptosPontemWalletService.connect();
				}
				break;
		}
	}

	public async selectWalletType(): Promise<void> {
		this.globalStateService.actions[GlobalActionTypeEnum.SELECT_WALLET_TYPE].next(true);
	}

	public async connectAptosMartianWallet(): Promise<void> {
		return this.aptosMartianWalletService.connect();
	}

	public async connectAptosPontemWallet(): Promise<void> {
		return this.aptosPontemWalletService.connect();
	}

	public async connectAptosPetraWallet(): Promise<void> {
		return this.aptosPetraWalletService.connect();
	}

	// public async connectSuiSuietWallet(): Promise<void> {
	// 	return this.suiSuietWalletService.connect();
	// }
	//
	// public async setSuiEthosWalletConnection(connection: SuiEthosWalletConnection): Promise<void> {
	// 	return this.suiEthosWalletService.connection$.next(connection);
	// }
	//
	// public async setSuiSuietWalletConnection(connection: SuietWalletConnection): Promise<void> {
	// 	return this.suiSuietWalletService.connection$.next(connection);
	// }

	public async connectEVMWalletWithModal(): Promise<void> {
		try {
			const providerConnection = await this.web3ModalService.open();
			const provider: Web3Provider = new Web3Provider(providerConnection, 'any');

			// for (const key in provider.provider) {
			// 	if (key === 'networkVersion' && Number(provider.provider[key]) === 280) {
			// 		console.log('ZK TIMEEEEE!');
			//
			//
			// 		// @ts-ignore
			// 		this.evmProvider$.next((new ZkProvider(providerConnection, 'any')));
			// 		this.connectedWallet$.next(WalletTypeEnum.EVM);
			//
			// 		return;
			// 	}
			// }

			this.evmProvider$.next(provider);
			this.connectedWallet$.next(WalletTypeEnum.EVM);
		} catch (e) {
			console.error(e);
			this.evmProvider$.next(undefined);
			// TODO: Notification "Couldn't connect to wallet"
		}
	}

	public async getConnectedWallet(): Promise<ConnectedWalletDetails> {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		switch (connectedWalletType) {
			case WalletTypeEnum.EVM:
			default:
				return {
					type: connectedWalletType,
					provider: this.isProviderTorus() ? WalletProviderEnum.TORUS : WalletProviderEnum.METAMASK,
					isConnected: this.isWalletConnected(),
					address: await this.getUserAddress(),
				};
			case WalletTypeEnum.APTOS:
				const provider: WalletProviderEnum = this.connectedProvider$.getValue();
				let isAptosWalletConnected: boolean;

				switch (provider) {
					case WalletProviderEnum.APTOS_MARTIAN:
						isAptosWalletConnected = await this.aptosMartianWalletService.isConnected();
						break;
					case WalletProviderEnum.APTOS_PETRA:
						isAptosWalletConnected = await this.aptosPetraWalletService.isConnected();
						break;
					case WalletProviderEnum.APTOS_PONTEM:
						isAptosWalletConnected = await this.aptosPontemWalletService.isConnected();
						break;
				}

				return {
					type: connectedWalletType,
					provider,
					isConnected: isAptosWalletConnected,
					address: await this.getUserAddress(),
				};
			// case WalletTypeEnum.SUI:
			// 	const suiProvider: WalletProviderEnum = this.connectedProvider$.getValue();
			// 	let isConnected: boolean = false;
			//
			// 	if (suiProvider === WalletProviderEnum.SUI_ETHOS) {
			// 		isConnected = this.suiEthosWalletService.isConnected();
			// 	}
			// 	if (suiProvider === WalletProviderEnum.SUI_SUIET) {
			// 		isConnected = this.suiSuietWalletService.isConnected();
			// 	}
			//
			// 	return {
			// 		type: connectedWalletType,
			// 		provider: suiProvider,
			// 		isConnected,
			// 		address: await this.getUserAddress(),
			// 	};
		}
	}

	public getConnectedWalletType(): WalletTypeEnum {
		return this.connectedWallet$.getValue();
	}

	public getEVMProvider(): Web3Provider {
		return this.evmProvider$.getValue();
	}

	public isWalletConnected(): boolean {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			return !!this.getEVMProvider() && this.isWalletUnlocked();
		}
		const connectedWalletProvider: WalletProviderEnum = this.connectedProvider$.getValue();

		if (connectedWalletType === WalletTypeEnum.APTOS) {
			if (connectedWalletProvider === WalletProviderEnum.APTOS_MARTIAN) {
				return this.aptosMartianWalletService.connection$?.getValue()?.address?.length > 0;
			}

			if (connectedWalletProvider === WalletProviderEnum.APTOS_PONTEM) {
				return this.aptosPontemWalletService.connection$?.getValue()?.address?.length > 0;
			}

			return this.aptosPetraWalletService.connection$?.getValue()?.address?.length > 0;
		}

		// if (connectedWalletType === WalletTypeEnum.SUI) {
		// 	if (connectedWalletProvider === WalletProviderEnum.SUI_ETHOS) {
		// 		return this.suiEthosWalletService.connection$?.getValue()?.isConnected;
		// 	}
		// 	if (connectedWalletProvider === WalletProviderEnum.SUI_SUIET) {
		// 		return this.suiSuietWalletService.connection$?.getValue()?.isConnected;
		// 	}
		// }
	}

	public isEVMProviderCached(): boolean {
		// @ts-ignore
		return this.web3ModalService?.web3WalletConnector?.cachedProvider === 'injected';
			// @ts-ignore
			// || this.web3ModalService?.web3WalletConnector?.providerController?.cachedProvider === 'walletconnect';
	}

	public getCachedEVMProvider(): ExternalProvider {
		// @ts-ignore
		if (this.web3ModalService?.web3WalletConnector?.cachedProvider === 'injected') {
			return window.ethereum;
		}
	}

	public async getUserAddress(): Promise<string | undefined> {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		switch (connectedWalletType) {
			case WalletTypeEnum.EVM:
			default:
				return await this.getEVMProvider()?.getSigner()?.getAddress() || undefined;
			case WalletTypeEnum.APTOS:
				const connectedWalletProvider: WalletProviderEnum = this.connectedProvider$.getValue();

				switch (connectedWalletProvider) {
					case WalletProviderEnum.APTOS_MARTIAN:
						return this.aptosMartianWalletService.connection$?.getValue()?.address;
					case WalletProviderEnum.APTOS_PETRA:
						return this.aptosPetraWalletService.connection$?.getValue()?.address;
					case WalletProviderEnum.APTOS_PONTEM:
						return this.aptosPontemWalletService.connection$?.getValue()?.address;
				}
				break;
			// case WalletTypeEnum.SUI:
			// 	const connectedSuiWalletProvider: WalletProviderEnum = this.connectedProvider$.getValue();
			//
			// 	if (connectedSuiWalletProvider === WalletProviderEnum.SUI_ETHOS) {
			// 		return this.suiEthosWalletService.connection$?.getValue()?.address;
			// 	}
			//
			// 	if (connectedSuiWalletProvider === WalletProviderEnum.SUI_SUIET) {
			// 		return this.suiSuietWalletService.connection$?.getValue()?.address;
			// 	}
			// 	break;
		}
	}

	public isWalletUnlocked(): boolean {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			if (this.isProviderTorus()) {
				// @ts-ignore
				return this.getEVMProvider()?.provider?.selectedAddress?.length > 0;
			}

			if (this.isEVMProviderWalletConnect()) {
				// @ts-ignore
				return this.getEVMProvider().provider.connected;
			}

			return window.ethereum?.selectedAddress?.length > 0;
		}
	}

	public showTorus(): void {
		// @ts-ignore
		this.evmProvider$.getValue().provider.torus.showTorusButton();
	}

	private isProviderTorus(): boolean {
		// @ts-ignore
		return this.evmProvider$.getValue()?.provider?.isTorus;
	}

	private isEVMProviderWalletConnect(): boolean {
		// @ts-ignore
		return this.evmProvider$.getValue()?.provider?.wc;
	}

	public getConnectedChainSymbol(): ChainSymbolEnum | undefined {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			const chainId: number = this.getEVMProvider()?.network?.chainId;

			return chainId > 0 ? this.chainIdToSymbolMap[chainId] : undefined;
		}

		if (connectedWalletType === WalletTypeEnum.APTOS) {
			return ChainSymbolEnum.aptos;
		}

		if (connectedWalletType === WalletTypeEnum.SUI) {
			return ChainSymbolEnum.sui;
		}
	}

	public goToAddressOnExplorer(address: string, chainSymbol: ChainSymbolEnum, page?: string, params?: string): void {
		const chainSymbolToExplorerUrlMap: Record<ChainSymbolEnum, string> = environment.chainSymbolToExplorerUrlMap;

		window.open(`${chainSymbolToExplorerUrlMap[chainSymbol]}/${page || 'address'}/${address}/${params || ''}`, '_blank');
	}

	public getEVMNetworkProvider(chainSymbol: ChainSymbolEnum, fallbackId?: number): BaseProvider {
		const provider: BaseProvider = this.networksProviders$[chainSymbol].getValue();

		if (!provider || fallbackId > 0) {
			this.networksProviders$[chainSymbol].next(
				ethers.getDefaultProvider(this.networksService.getNetworkByChainSymbol(chainSymbol, fallbackId || 0)),
			);
		}

		return this.networksProviders$[chainSymbol].getValue();
	}

	public getMainnetEVMNetworkProvider(chainSymbol: ChainSymbolEnum, fallbackId?: number): BaseProvider {
		const provider: BaseProvider = this.MAINNETNetworksProviders$[chainSymbol].getValue();

		if (!provider || fallbackId > 0) {
			this.MAINNETNetworksProviders$[chainSymbol].next(
				ethers.getDefaultProvider(this.networksService.getMAINNETNetworkByChainSymbol(chainSymbol, fallbackId || 0)),
			);
		}

		return this.MAINNETNetworksProviders$[chainSymbol].getValue();
	}


	public async getBalance(tokenAddress: string): Promise<BigNumber> {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			const provider: Web3Provider = this.getEVMProvider();
			const userAddress: string = await provider?.getSigner()?.getAddress();

			if (!userAddress) {
				return BigNumber.from(0);
			}
			const tokenContract: Contract = new ethers.Contract(tokenAddress, this.erc20Abi, provider.getSigner());

			return await tokenContract.balanceOf(userAddress);
		}
	}

	public async getNativeAssetBalance(address: string): Promise<BigNumber> {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			const provider: Web3Provider = this.getEVMProvider();
			const userAddress: string = address || await provider?.getSigner()?.getAddress();

			if (!userAddress) {
				return BigNumber.from(0);
			}

			return await provider.getBalance(userAddress);
		}
	}

	public async getFormattedNativeAssetBalance(address: string): Promise<number> {
		const connectedWalletType: WalletTypeEnum = this.getConnectedWalletType();

		if (connectedWalletType === WalletTypeEnum.EVM) {
			return Number(ethers.utils.formatEther(await this.getNativeAssetBalance(address)));
		}
	}

	public encodePayload(params: unknown[], paramsTypes: string[]): string {
		return ethers.utils.AbiCoder.prototype.encode(
			paramsTypes,
			params,
		);
	}

	// public async getSuiCoinBalanceAndObjectId(address: string): Promise<{
	// 	balance: number,
	// 	coinObjectId: string,
	// 	biggestCoinBalance: number,
	// }> {
	// 	return await this.suiNetworkService.getSuiCoinAndBalance(address);
	// }
}
