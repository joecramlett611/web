import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import { IMintTx } from '../../collections/interfaces/mint-tx.interface';
import { ContractTransaction } from '@ethersproject/contracts/src.ts/index';

export interface IUserTransactionsList {
	refund: Record<string, Record<string, IClaimRefundTx>>;
	claimEarned: Record<string, Record<string, IClaimEarnedTx>>;
	mint: Record<string, Record<string, IMintTx>>;
	createCollection: Record<string, Pick<ContractTransaction, 'hash'>>;
}
