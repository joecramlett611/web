import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import UserService from './services/user.service';
import ProfileService from './services/profile.service';
import UserActivityService from './services/user-activity-service';

@NgModule({
	providers: [
		UserService,
		ProfileService,
		UserActivityService,
	],
	imports: [
		SharedModule,
	],
})
export class UserModule {
}
