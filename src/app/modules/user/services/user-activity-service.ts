import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { IUserActivity } from '../../collections/interfaces/user-activity.interface';
import OmnichainService from '../../omnichain/services/omnichain.service';
import { UserActivityTypeEnum } from '../../collections/enums/user-activity-type.enum';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { LzTransactionStatusEnum } from '../../collections/enums/lz-transaction-status.enum';
import UserService from './user.service';
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import { Message } from '@layerzerolabs/scan-client';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';

@Injectable()
export default class UserActivityService {
	public readonly userActivity$: {
		[UserActivityTypeEnum.CREATE]: BehaviorSubject<Array<IUserActivity<ICollectionDetails>>>,
		[UserActivityTypeEnum.MINT]: BehaviorSubject<Array<IUserActivity<IMintData>>>,
		[UserActivityTypeEnum.CLAIM_EARNED]: BehaviorSubject<Array<IUserActivity<IClaimEarnedTx>>>,
		[UserActivityTypeEnum.REFUND]: BehaviorSubject<Array<IUserActivity<IClaimRefundTx>>>,
	} = {
		[UserActivityTypeEnum.CREATE]: new BehaviorSubject<Array<IUserActivity<ICollectionDetails>>>([]),
		[UserActivityTypeEnum.MINT]: new BehaviorSubject<Array<IUserActivity<IMintData>>>([]),
		[UserActivityTypeEnum.CLAIM_EARNED]: new BehaviorSubject<Array<IUserActivity<IClaimEarnedTx>>>([]),
		[UserActivityTypeEnum.REFUND]: new BehaviorSubject<Array<IUserActivity<IClaimRefundTx>>>([]),
	};

	constructor(
		private readonly omnichainService: OmnichainService,
		private readonly userService: UserService,
	) {
	}

	public async syncUserActivities(
		loadingState: {
			[UserActivityTypeEnum.CREATE]: boolean,
			[UserActivityTypeEnum.MINT]: boolean,
			[UserActivityTypeEnum.CLAIM_EARNED]: boolean,
			[UserActivityTypeEnum.REFUND]: boolean,
		},
	): Promise<void> {
		this.clearUserActivity();
		const collectionsDetails: ICollectionDetails[] = await this.userService.getConnectedUserCollectionsCreateData();

		(new Promise(async (resolve) => {
			for (const collectionDetails of collectionsDetails) {
				try {
					if (!collectionDetails.txHash) {
						continue;
					}
					const userActivity: IUserActivity<ICollectionDetails> = await this.getUserActivity<ICollectionDetails>(
						collectionDetails.txHash,
						UserActivityTypeEnum.CREATE,
						collectionDetails,
					);

					if (!userActivity) {
						continue;
					}
					const currentUserActivity: Array<IUserActivity<ICollectionDetails>> = this.userActivity$.CREATE.getValue();

					this.userActivity$.CREATE.next([...currentUserActivity, userActivity]);
				} catch (e) {
					console.error(e);
				}
			}
			resolve(true);
		})).then(() => {
			loadingState[UserActivityTypeEnum.CREATE] = true;
		});

		(new Promise(async (resolve) => {
			const mintData: IMintData[] = await this.userService.getConnectedUserMintsData();

			for (const mint of mintData) {
				try {
					if (!mint.txHash) {
						continue;
					}
					const userActivity: IUserActivity<IMintData> = await this.getUserActivity<IMintData>(
						mint.txHash,
						UserActivityTypeEnum.MINT,
						mint,
					);

					if (!userActivity) {
						continue;
					}
					const currentUserActivity: Array<IUserActivity<IMintData>> = this.userActivity$.MINT.getValue();

					this.userActivity$.MINT.next([...currentUserActivity, userActivity]);
				} catch (e) {
					console.error(e);
				}
			}

			resolve(true);
		})).then(() => {
			loadingState[UserActivityTypeEnum.MINT] = true;
		});

		(new Promise(async (resolve) => {
			const data: IClaimRefundTx[] = await this.userService.getConnectedUserRefundsData();

			for (const refund of data) {
				try {
					if (!refund.txHash) {
						continue;
					}

					const userActivity: IUserActivity<IClaimRefundTx> = await this.getUserActivity<IClaimRefundTx>(
						refund.txHash,
						UserActivityTypeEnum.REFUND,
						refund,
					);

					if (!userActivity) {
						continue;
					}
					const currentUserActivity: Array<IUserActivity<IClaimRefundTx>> = this.userActivity$.REFUND.getValue();

					this.userActivity$.REFUND.next([...currentUserActivity, userActivity]);
				} catch (e) {
					console.error(e);
				}
			}
			resolve(true);
		})).then(() => {
			loadingState[UserActivityTypeEnum.REFUND] = true;
		});

		(new Promise(async (resolve) => {
			const data: IClaimEarnedTx[] = await this.userService.getConnectedUserClaimEarnedData();

			for (const claimedEarned of data) {
				try {
					if (!claimedEarned.txHash) {
						continue;
					}

					const userActivity: IUserActivity<IClaimEarnedTx> = await this.getUserActivity<IClaimEarnedTx>(
						claimedEarned.txHash,
						UserActivityTypeEnum.CLAIM_EARNED,
						claimedEarned,
					);

					if (!userActivity) {
						continue;
					}
					const currentUserActivity: Array<IUserActivity<IClaimEarnedTx>> = this.userActivity$.CLAIM_EARNED.getValue();

					this.userActivity$.CLAIM_EARNED.next([...currentUserActivity, userActivity]);
				} catch (e) {
					console.error(e);
				}
			}
			resolve(true);
		})).then(() => {
			loadingState[UserActivityTypeEnum.CLAIM_EARNED] = true;
		});
	}

	public async getUserActivity<MetadataType = undefined>(
		txHash: string,
		type: UserActivityTypeEnum,
		metadata?: MetadataType,
	): Promise<IUserActivity<MetadataType>> {
		const lzMessage: Message = await this.omnichainService.getLzTransactionStatus(txHash);

		if (lzMessage === undefined) {
			return undefined;
		}

		return {
			type,
			txHash,
			status: this.mapActivityStatusByLzTxStatus(LzTransactionStatusEnum[lzMessage.status]),
			metadata,
			lzMessage,
		};
	}

	public clearUserActivity(): void {
		this.userActivity$.CREATE.next([]);
		this.userActivity$.MINT.next([]);
		this.userActivity$.CLAIM_EARNED.next([]);
		this.userActivity$.REFUND.next([]);
	}

	private mapActivityStatusByLzTxStatus(txStatus: LzTransactionStatusEnum): 'PENDING' | 'SUCCESS' | 'ERROR' {
		switch (txStatus) {
			case LzTransactionStatusEnum.DELIVERED:
				return 'SUCCESS';
			case LzTransactionStatusEnum.INFLIGHT:
				return 'PENDING';
			case LzTransactionStatusEnum.FAILED:
				return 'ERROR';
		}
	}
}
