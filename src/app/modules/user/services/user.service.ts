import { Injectable } from '@angular/core';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import Web3Service from '../../web3/services/web3.service';
import { IUserTransactionsList } from '../interfaces/user-transactions-list.interface';
import { BehaviorSubject } from 'rxjs';
import firebase from 'firebase/compat';
import Unsubscribe = firebase.Unsubscribe;
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import NftsService from '../../nfts/services/nfts.service';
import CollectionsDataService from '../../chain-data/services/collections-data.service';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';

@Injectable()
export default class UserService {
	public readonly userTransactions$: BehaviorSubject<IUserTransactionsList> = new BehaviorSubject<IUserTransactionsList>(undefined);
	private userAddress: string;
	private userTransactionsListener: Unsubscribe;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly nftsService: NftsService,
		private readonly collectionDataService: CollectionsDataService,
	) {
	}

	public async run(): Promise<void> {
		setTimeout(() => {
			this.web3Service.connectedWallet$.subscribe(async (provider) => {
				if (!provider) {
					return;
				}

				const walletInterval: NodeJS.Timer = setInterval(async () => {
					if (this.web3Service.isWalletConnected()) {
						const userAddress: string = await this.web3Service.getUserAddress();

						if (userAddress) {
							clearInterval(walletInterval);
							this.listenToAllUserTransactions();
						}
					}
				}, 250);
			});
		}, 250);
	}

	public async listenToAllUserTransactions(): Promise<void> {
		if (this.userTransactionsListener) {
			return;
		}

		if (!this.userAddress) {
			this.userAddress = await this.web3Service.getUserAddress();

			if (!this.userAddress) {
				return;
			}
		}
		this.userTransactionsListener = await this.firebaseDatabaseService.get<IUserTransactionsList>(
			`/transactions/${this.userAddress}`,
			{},
			async (transactionsList: IUserTransactionsList) => {
				this.userTransactions$.next(transactionsList);
			},
		);
	}

	public async getConnectedUserCollectionsCreateData(): Promise<ICollectionDetails[]> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}

		return (await this.collectionDataService.getUserCollectionsCreateData(userAddress, true)) || [];
	}

	public async getConnectedUserMintsData(): Promise<IMintData[]> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}

		return (await this.nftsService.getUserMintsData(userAddress)) || [];
	}

	public async getConnectedUserRefundsData(): Promise<IClaimRefundTx[]> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}

		return (await this.nftsService.getUserRefundsData(userAddress)) || [];
	}

	public async getConnectedUserClaimEarnedData(): Promise<IClaimEarnedTx[]> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}

		return (await this.nftsService.getUserClaimEarnedData(userAddress)) || [];
	}
}
