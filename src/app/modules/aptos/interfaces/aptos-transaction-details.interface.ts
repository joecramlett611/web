import { AptosResourceTypeEnum } from '../enums/aptos-event-type.enum';

export interface IAptosTransactionDetails<EventDataType = null> {
	expiration_timestamp_secs: string;
	hash: string;
	success: boolean;
	vm_status: 'Executed successfully';
	events: Array<{
		type: AptosResourceTypeEnum;
		data: EventDataType;
	}>;
}
