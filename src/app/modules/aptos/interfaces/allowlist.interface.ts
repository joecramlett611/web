export interface IAllowlist {
	addresses?: string[];
	isAllowlisted?: boolean;
	maxPerAddress: number;
	isEnabled: boolean;
	publicFrom: number;
	publicMaxPerAddress: number;
}
