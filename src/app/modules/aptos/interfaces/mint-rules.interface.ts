import { IAllowlist } from './allowlist.interface';

export interface IMintRules {
	from: number;
	to: number;
	price: number;
	allowlist: IAllowlist;
	maximum_supply: number;
	royalty_points_numerator: number;
	token_uri: string;
}
