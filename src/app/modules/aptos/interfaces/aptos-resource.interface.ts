export interface IAptosResource {
	type: string;
	data: {
		coin?: { // AptosCoin
			value: number;
		},
		signer_addr: string, // CollectionTokenMinter Resource
	};
}
