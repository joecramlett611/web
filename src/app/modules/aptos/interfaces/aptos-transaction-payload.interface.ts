export interface IAptosTransaction<T = null> {
	function: string; // "0x1::coin::transfer",
	type_arguments: string[]; // ["0x1::aptos_coin::AptosCoin"],
	arguments: T; // ["0x96da8990a7230a82250e85d943ca95e2e9319e5558b0f544f2d7a6aad327e46f", 50]
}

