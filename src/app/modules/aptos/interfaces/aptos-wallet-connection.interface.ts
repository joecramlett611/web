export interface IAptosWalletConnection {
	address: string;
	method: 'connected';
	publicKey: string;
	status: number;
}
