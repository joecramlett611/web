import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IAptosWalletConnection } from '../interfaces/aptos-wallet-connection.interface';
import { IAptosTransaction } from '../interfaces/aptos-transaction-payload.interface';
import { AptosResourceTypeEnum } from '../enums/aptos-resource-type.enum';
import { BigNumber } from 'ethers';
import { environment } from '../../../../environments/environment';
import { IAptosResource } from '../interfaces/aptos-resource.interface';
import { IAptosTransactionDetails } from '../interfaces/aptos-transaction-details.interface';
import { AptosClient } from 'aptos';

@Injectable()
export default class AptosPetraWalletService {
	public connection$: BehaviorSubject<IAptosWalletConnection> = new BehaviorSubject<IAptosWalletConnection>(undefined);

	public async connect(): Promise<void> {
		try {
			const isWalletInstalled = window.aptos;

			if (!isWalletInstalled) {
				window.open('https://petra.app/', '_blank');
				throw new Error('Petra wallet is not installed');
			}
			const connection: IAptosWalletConnection = await window.aptos.connect();

			if (!connection) {
				return;
			}

			this.connection$.next(connection);
		} catch (e) {
			console.error(e);
		}
	}

	public async isConnected(): Promise<boolean> {
		return await window.aptos.isConnected() && this.connection$.getValue() !== undefined;
	}

	public disconnect(): void {
		window.aptos.disconnect();
		this.connection$.next(undefined);
	}

	public async sendTransaction<TxArguments = null, TxDetails = unknown>(
		transaction: IAptosTransaction<TxArguments>,
	): Promise<IAptosTransactionDetails<TxDetails>> {
		const pendingTransaction = await window.aptos.signAndSubmitTransaction(transaction);
		// In most cases a dApp will want to wait for the transaction, in these cases you can use the typescript sdk
		const client = new AptosClient(environment.aptosSpecific.api.uri);

		return (await client.waitForTransactionWithResult(pendingTransaction.hash) as IAptosTransactionDetails);
	}

	public async getTransaction(txHash: string): Promise<any> {
		return await window.aptos.getTransaction(txHash);
	}

	public async getAccountResources(account: string): Promise<Array<IAptosResource>> {
		const client = new AptosClient(environment.aptosSpecific.api.uri);
		return await client.getAccountResources(account) as IAptosResource[];
	}

	public async getBalance(address: string): Promise<BigNumber> {
		const resources: IAptosResource[] = await this.getAccountResources(address);
		const aptosCoinResource = resources.find((resource) => resource.type === AptosResourceTypeEnum.APTOS_COIN);

		if (!aptosCoinResource) {
			return BigNumber.from(0);
		}

		return BigNumber.from(aptosCoinResource.data.coin.value);
	}

	public async setWhitelist(
		moduleVersion: number,
		creator: string,
		collectionName: string,
		addresses: string[],
		maxPerAddress: number,
		maxPerAddressPublic: number,
		publicFrom: number,
		isEnabled: boolean,
	): Promise<Pick<IAptosTransactionDetails, 'hash' | 'success'>> {
		const payload = [
			collectionName,
			addresses,
			maxPerAddress,
			maxPerAddressPublic,
			publicFrom,
			isEnabled,
		];
		const txResult: Pick<IAptosTransactionDetails, 'hash' | 'success'> = await this.sendTransaction({
			function: environment.aptosSpecific.functions.setAllowlist.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
			arguments: payload,
			type_arguments: [],
		});

		try {
			if (!txResult.success) {
				throw new Error('setAllowlist() transaction has failed');
			}

			return txResult;
		} catch (e) {
			console.error(e);
			throw e;
		}
	}
}
