import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IAptosWalletConnection } from '../interfaces/aptos-wallet-connection.interface';
import { IAptosTransaction } from '../interfaces/aptos-transaction-payload.interface';
import { AptosResourceTypeEnum } from '../enums/aptos-resource-type.enum';
import { BigNumber } from 'ethers';
import { environment } from '../../../../environments/environment';
import { IAptosResource } from '../interfaces/aptos-resource.interface';
import { IAptosTransactionDetails } from '../interfaces/aptos-transaction-details.interface';
import { AptosClient } from 'aptos';

@Injectable()
export default class AptosPontemWalletService {
	public connection$: BehaviorSubject<IAptosWalletConnection> = new BehaviorSubject<IAptosWalletConnection>(undefined);

	public async connect(): Promise<void> {
		try {
			const isPontemWalletInstalled = window.pontem;

			if (!isPontemWalletInstalled) {
				window.open('https://chrome.google.com/webstore/detail/pontem-aptos-wallet/phkbamefinggmakgklpkljjmgibohnba', '_blank');
				throw new Error('Pontem wallet is not installed');
			}
			const connection: IAptosWalletConnection = await window.pontem.connect();

			if (!connection) {
				return;
			}

			this.connection$.next(connection);
		} catch (e) {
			console.error(e);
		}
	}

	public async isConnected(): Promise<boolean> {
		return await window.pontem.isConnected() && this.connection$.getValue() !== undefined;
	}

	public disconnect(): void {
		window.pontem.disconnect();
		this.connection$.next(undefined);
	}

	public async sendTransaction<T = null>(sender: string, payload: IAptosTransaction<T>): Promise<string> {
		return (new Promise(async (resolve, reject) => {
			try {
				// @ts-ignore
				const tx: {result: {hash: string}} = await window.pontem.signAndSubmit(payload);
				// @ts-ignore
				resolve(tx.result.hash);
				return tx.result.hash;
			} catch (e) {
				console.error(e);
				if (typeof e === 'string' && e.includes('failure to get a peer from the ring-balancer')) {
					setTimeout(async () => {
						return await this.sendTransaction(sender, payload);
					}, 5000);
				} else {
					reject();
					return;
				}
			}
		}));
	}

	public async getTransaction(txHash: string): Promise<any> {
		return (new Promise(async (resolve, reject) => {
			const client = new AptosClient(environment.aptosSpecific.api.uri);
			let attempts: number = 0;

			try {
				const tx = await client.getTransactionByHash(txHash);

				resolve(tx);
				return tx;
			} catch (e) {
				const interval = setInterval(async () => {
					try {
						attempts++;
						const tx = await client.getTransactionByHash(txHash);

						clearInterval(interval);
						resolve(tx);
						return tx;
					} catch (err) {
						if (attempts >= 10) {
							clearInterval(interval);
							reject(err);
							return;
						}
					}
				}, 3500);
			}
		}));
	}

	public async getAccountResources(account: string): Promise<Array<IAptosResource>> {
		return await window.pontem.getAccountResources(account);
	}

	public async getBalance(address: string): Promise<BigNumber> {
		const resources: IAptosResource[] = await this.getAccountResources(address);
		const aptosCoinResource = resources.find((resource) => resource.type === AptosResourceTypeEnum.APTOS_COIN);

		if (!aptosCoinResource) {
			return BigNumber.from(0);
		}

		return BigNumber.from(aptosCoinResource.data.coin.value);
	}

	public async setWhitelist(
		moduleVersion: number,
		creator: string,
		collectionName: string,
		addresses: string[],
		maxPerAddress: number,
		maxPerAddressPublic: number,
		publicFrom: number,
		isEnabled: boolean,
	): Promise<IAptosTransactionDetails> {
		const payload = [
			collectionName,
			addresses,
			maxPerAddress,
			maxPerAddressPublic,
			publicFrom,
			isEnabled,
		];
		const txHash: string = await this.sendTransaction(creator, {
			function: environment.aptosSpecific.functions.setAllowlist.replace(
				'{moduleName}',
				environment.aptosSpecific.versionToModuleName[moduleVersion],
			),
			arguments: payload,
			type_arguments: [],
		});

		try {
			const txResult: IAptosTransactionDetails = await this.getTransaction(txHash);

			if (!txResult.success) {
				throw new Error('setAllowlist() transaction has failed');
			}

			return txResult;
		} catch (e) {
			console.error(e);
			throw e;
		}
	}
}
