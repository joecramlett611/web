import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import AptosMartianWalletService from './services/aptos-martian-wallet.service';
import AptosPetraWalletService from './services/aptos-petra-wallet.service';
import AptosPontemWalletService from './services/aptos-pontem-wallet.service';

@NgModule({
	providers: [
		AptosMartianWalletService,
		AptosPetraWalletService,
		AptosPontemWalletService,
	],
	imports: [
		CommonModule,
	],
})
export class AptosModule {
}
