import { NgModule } from '@angular/core';
import { TokenRoutingModule } from './token-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TokenIntroComponent } from './components/token-intro/token-intro.component';
import TokenSaleService from './services/token-sale.service';
import { TokenBridgeComponent } from './components/token-bridge/token-bridge.component';
import TokenBridgeService from './services/token-bridge.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	providers: [
		TokenSaleService,
		TokenBridgeService,
	],
	declarations: [
		TokenIntroComponent,
		TokenBridgeComponent,
	],
	imports: [
		TokenRoutingModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
	],
	exports: [
		TokenIntroComponent,
		TokenBridgeComponent,
	],
})
export class TokenModule {
}
