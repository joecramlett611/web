import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TokenIntroComponent } from './components/token-intro/token-intro.component';
import { TokenBridgeComponent } from './components/token-bridge/token-bridge.component';

const routes: Routes = [
	{
		path: '',
		component: TokenIntroComponent,
	},
	{
		path: 'bridge',
		component: TokenBridgeComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TokenRoutingModule {
}
