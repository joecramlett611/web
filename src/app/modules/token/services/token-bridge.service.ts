import { Injectable } from '@angular/core';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, BigNumberish, ContractTransaction, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { environment } from '../../../../environments/environment';
import { Contract } from '@ethersproject/contracts';
import NotificationsService from '../../shared/services/notifications.service';

@Injectable()
export default class TokenBridgeService {
	public readonly tokenBridgeABI: string[] = [
		'function send(uint16 dstChainId, address to, uint256 amount) external payable',
		'function estimateSendFee(uint16 _dstChainId, bytes memory _toAddress, uint _amount, bool _useZro, bytes memory _adapterParams) public view returns (uint)',
	];
	private readonly erc20Abi: string[] = [
		'function allowance(address owner, address spender) public view returns (uint256)',
		'function approve(address spender, uint256 amount) public returns (bool)',
		'function balanceOf(address account) view returns (uint256)',
	];
	private bridgingStatusInterval?: NodeJS.Timer;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly notificationsService: NotificationsService,
	) {
	}

	public async bridge(
		amount: number,
		dstChainId: number,
		to: string,
		state: { status: 'pending' | 'approving' | 'approval_error' | 'error' | 'completed' },
		dstChainSymbol: ChainSymbolEnum,
	): Promise<ContractTransaction> {
		if (this.bridgingStatusInterval) {
			try {
				clearInterval(this.bridgingStatusInterval);
			} catch (e) {
				console.error(e);
			}
		}
		if (amount <= 0) {
			throw new Error('Transferred amount has to be positive');
		}
		if (dstChainId <= 0) {
			throw new Error('Invalid destination chain LayerZero id');
		}
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to bridge');
		}
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to bridge');
		}
		const contractAddress: string = environment.tokenBridgeAddressesMap[connectedChain];
		const contract = new ethers.Contract(
			contractAddress,
			this.tokenBridgeABI,
			signer,
		);
		const assetAddress: string = environment.OSEAAddressMAINNET[connectedChain];
		const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, signer);
		const allowance: BigNumber = await this.getAllowance(assetContract, contractAddress);
		const assetAmountInWei: BigNumber = ethers.utils.parseUnits(amount.toString());

		if (allowance.lt(assetAmountInWei)) {
			try {
				state.status = 'approving';
				const approvalTx: ContractTransaction = await this.approveSpent(assetContract, contractAddress);

				await approvalTx.wait();
			} catch (e) {
				console.error(e);
				state.status = 'approval_error';
				throw e;
			}
		}
		state.status = 'pending';
		const receiverAddress: string = to || (await this.web3Service.getUserAddress());
		let estimatedFee: BigNumberish;

		try {
			estimatedFee = await contract.estimateSendFee(
				dstChainId,
				receiverAddress,
				assetAmountInWei,
				false,
				[],
			);
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Cannot estimate bridging fee');
			state.status = 'error';

			return;
		}

		try {
			const tx: ContractTransaction = await contract.send(
				dstChainId,
				receiverAddress,
				assetAmountInWei,
				{value: estimatedFee},
			);

			try {
				const dstBalance: string = await this.getOSEABalanceFormatted(receiverAddress, dstChainSymbol);

				this.bridgingStatusInterval = setInterval(async () => {
					const currentDstBalance: string = await this.getOSEABalanceFormatted(receiverAddress, dstChainSymbol);

					if (Number(currentDstBalance) > Number(dstBalance)) {
						if (this.bridgingStatusInterval) {
							clearInterval(this.bridgingStatusInterval);
						}
						state.status = 'completed';
						this.notificationsService.success('Success', 'Bridging is completed');
					}
				}, 30000);
			} catch (e) {
				console.error(e);
			}

			return tx;
		} catch (e) {
			console.error(e);
			state.status = 'error';
			const errorMessage: string = e?.message || e;
			const errorDataMessage: string = e?.data?.message;

			if (errorDataMessage) {
				if (errorDataMessage.includes('insufficient funds for gas') || errorDataMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[connectedChain];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}
			}

			if (errorMessage) {
				if (errorMessage.includes('MetaMask Tx Signature: User denied transaction signature')) {
					// Canceled on User's action
					return;
				}

				if (errorMessage.includes('insufficient funds for gas') || errorMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[connectedChain];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}

				if (errorMessage.includes('insufficient balance for transfer')) {
					this.notificationsService.error('Insufficient funds', 'Looks like you currently don\'t have required amount to bridge');
				}
			}

			throw e;
		}
	}

	public async getAllowance(contract: Contract, spenderAddress: string): Promise<BigNumber> {
		return await contract.allowance(contract.signer.getAddress(), spenderAddress);
	}

	public async approveSpent(contract, spenderAddress: string): Promise<ContractTransaction> {
		return await contract.approve(spenderAddress, BigNumber.from(2).pow(256).sub(1));
	}

	public async getOSEABalanceFormatted(userAddress: string, chainSymbol: ChainSymbolEnum): Promise<string> {
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(chainSymbol);
		const assetAddress: string = environment.OSEAAddressMAINNET[chainSymbol];
		const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, provider);
		const balanceBN: BigNumber = await assetContract.balanceOf(userAddress);

		return ethers.utils.formatUnits(balanceBN);
	}
}
