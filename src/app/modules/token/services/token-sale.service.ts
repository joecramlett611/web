import { Injectable } from '@angular/core';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, ContractTransaction, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { environment } from '../../../../environments/environment';
import { Contract } from '@ethersproject/contracts';
import NotificationsService from '../../shared/services/notifications.service';

@Injectable()
export default class TokenSaleService {
	public readonly tokenSaleABI: string[] = [
		'function deposit(uint256 amount) external payable',
		'function withdrawAllocation() external',
		'function buyerToAllocation(address buyer) public view returns (uint256)',
		'function rate() public view returns (uint256)',
		'function from() public view returns (uint256)',
		'function to() public view returns (uint256)',
	];
	private readonly erc20Abi: string[] = [
		'function allowance(address owner, address spender) public view returns (uint256)',
		'function approve(address spender, uint256 amount) public returns (bool)',
		'function balanceOf(address account) view returns (uint256)',
	];
	public readonly airdropABI: string[] = [
		'function receiverClaimable(address receiver) public view returns (uint256)',
		'function claim() external',
	];

	constructor(
		private readonly web3Service: Web3Service,
		private readonly notificationsService: NotificationsService,
	) {
	}

	public async deposit(
		amount: number,
		state: { status: 'pending' | 'approving' | 'depositing' | 'success' | 'approval_error' | 'deposit_error' },
	): Promise<ContractTransaction> {
		if (amount <= 0) {
			throw new Error('Deposit amount has to be positive');
		}
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to deposit');
		}
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to deposit');
		}
		const saleContractAddress: string = environment.tokenSaleAddressesMap[connectedChain];
		const contract = new ethers.Contract(
			saleContractAddress,
			this.tokenSaleABI,
			signer,
		);
		const assetAddress: string = environment.USDCAddress[connectedChain];
		const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, signer);
		const allowance: BigNumber = await this.getAllowance(assetContract, saleContractAddress);
		const assetAmountInWei: BigNumber = ethers.utils.parseUnits(amount.toString(), connectedChain === ChainSymbolEnum.bsc ? 18 : 6);

		if (allowance.lt(assetAmountInWei)) {
			try {
				state.status = 'approving';
				const approvalTx: ContractTransaction = await this.approveSpent(assetContract, saleContractAddress);

				await approvalTx.wait();
			} catch (e) {
				console.error(e);
				state.status = 'approval_error';
				throw e;
			}
		}
		state.status = 'depositing';

		try {
			return await contract.deposit(assetAmountInWei);
		} catch (e) {
			console.error(e);
			state.status = 'deposit_error';
			throw e;
		}
	}

	public async withdrawAllocation(): Promise<ContractTransaction> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to withdrawAllocation');
		}
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to withdrawAllocation');
		}
		const contract = new ethers.Contract(
			environment.tokenSaleAddressesMap[connectedChain],
			this.tokenSaleABI,
			signer,
		);

		return await contract.withdrawAllocation();
	}

	public async getAllocatedByUser(): Promise<BigNumber> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to getAllocatedByUser');
		}
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(connectedChain);
		if (!provider) {
			throw new Error('No provider - unable to getAllocatedByUser');
		}
		const contract = new ethers.Contract(
			environment.tokenSaleAddressesMap[connectedChain],
			this.tokenSaleABI,
			provider,
		);
		const userAddress: string = await this.web3Service.getUserAddress();

		return await contract.buyerToAllocation(userAddress);
	}

	public async getRate(): Promise<BigNumber> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to getRate');
		}
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(connectedChain);
		if (!provider) {
			throw new Error('No provider - unable to getRate');
		}
		const contract = new ethers.Contract(
			environment.tokenSaleAddressesMap[connectedChain],
			this.tokenSaleABI,
			provider,
		);

		return await contract.rate();
	}

	public async getStartDate(): Promise<BigNumber> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to getStartDate');
		}
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(connectedChain);
		if (!provider) {
			throw new Error('No provider - unable to getStartDate');
		}
		const contract = new ethers.Contract(
			environment.tokenSaleAddressesMap[connectedChain],
			this.tokenSaleABI,
			provider,
		);

		return await contract.from();
	}

	public async getEndDate(): Promise<BigNumber> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			throw new Error('Connect with wallet to getEndDate');
		}
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(connectedChain);
		if (!provider) {
			throw new Error('No provider - unable to getEndDate');
		}
		const contract = new ethers.Contract(
			environment.tokenSaleAddressesMap[connectedChain],
			this.tokenSaleABI,
			provider,
		);

		return await contract.to();
	}

	public getFormattedAllocation(allocationInUnit): string {
		return ethers.utils.formatUnits(allocationInUnit);
	}

	public async getAllowance(contract: Contract, spenderAddress: string): Promise<BigNumber> {
		return await contract.allowance(contract.signer.getAddress(), spenderAddress);
	}

	public async approveSpent(contract, spenderAddress: string): Promise<ContractTransaction> {
		return await contract.approve(spenderAddress, BigNumber.from(2).pow(256).sub(1));
	}

	public async getUSDCBalanceFormatted(userAddress: string, connectedChainSymbol: ChainSymbolEnum): Promise<string> {
		const provider: BaseProvider = this.web3Service.getMainnetEVMNetworkProvider(connectedChainSymbol);
		const assetAddress: string = environment.USDCAddress[connectedChainSymbol];
		const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, provider);
		const balanceBN: BigNumber = await assetContract.balanceOf(userAddress);

		return ethers.utils.formatUnits(balanceBN, connectedChainSymbol === ChainSymbolEnum.bsc ? 18 : 6);
	}

	public async claimAirdrop(): Promise<void> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			this.notificationsService.error('Connect with wallet to deposit');
			throw new Error('Connect with wallet to deposit');
		}

		if (connectedChain !== ChainSymbolEnum.bsc) {
			this.notificationsService.error('Airdrop is claimable on BNB Chain (BSC)');
			throw new Error('Airdrop is claimable on BNB Chain (BSC)');
		}
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();

		if (!signer) {
			throw new Error('No signer - unable to deposit');
		}
		const airdropDistributorAddress: string = environment.airdropDistributorAddress;
		const contract = new ethers.Contract(
			airdropDistributorAddress,
			this.airdropABI,
			signer,
		);
		const isClaimable: boolean = await this.isAirdropClaimable();

		if (!isClaimable) {
			this.notificationsService.error('Nothing to claim');
			return;
		}

		try {
			await contract.claim();
			this.notificationsService.success('Successfully claimed');
		} catch (e) {
			this.notificationsService.error('Error', 'Could not claim the airdrop due to network issue.');
			console.error(e);
			throw e;
		}
	}

	public async isAirdropClaimable(): Promise<boolean> {
		const signer: JsonRpcSigner = this.web3Service.getEVMProvider()?.getSigner();
		const airdropDistributorAddress: string = environment.airdropDistributorAddress;
		const contract = new ethers.Contract(
			airdropDistributorAddress,
			this.airdropABI,
			signer,
		);
		const claimable: BigNumber = await contract.receiverClaimable((await this.web3Service.getUserAddress()));

		return claimable.gt(0);
	}
}
