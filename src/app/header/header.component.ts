import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Params, Router } from '@angular/router';
import Web3Service from '../modules/web3/services/web3.service';
import { fromEvent, Subscription } from 'rxjs';
import { ICollectionDetails } from '../modules/web3/interfaces/collection-details.interface';
import ProfileService from '../modules/user/services/profile.service';
import { IProfile } from '../modules/user/interfaces/profile.interface';
import FirebaseStorageService from '../modules/firebase/services/firebase-storage.service';
import GlobalStateService from '../modules/shared/global-state.service';
import { GlobalActionTypeEnum } from '../modules/shared/enums/global-action-type.enum';
import { ConnectedWalletDetails } from '../modules/web3/types/connected-wallet-details.type';
// import { SuietWalletConnection } from '../react/components/react-container/react-container.component';
// import { SuiEthosWalletConnection } from '../modules/sui/interfaces/sui-ethos-wallet-connection.interface';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
	public get isHelpCenter(): boolean {
		return this.router.url.includes('/help-center');
	}

	@ViewChild('walletSelectCloseButton') public walletSelectCloseButton: ElementRef<HTMLButtonElement>;
	public isConnectedToWallet: boolean;
	public web3ConnectionSubscription: Subscription;
	public isMobileNavOpened: boolean;
	public isSearchOpen: boolean;
	public collections: ICollectionDetails[];
	public isLoadedCollections: boolean;
	public profileAvatar: string;
	public isAvatarLoaded: boolean;
	public connectedWalletDetails: ConnectedWalletDetails;
	private subscriptions: Subscription;
	private searchCloseSubscription: Subscription;
	private profile: IProfile;

	constructor(
		private readonly router: Router,
		private readonly globalStateService: GlobalStateService,
		private readonly web3Service: Web3Service,
		private readonly profileService: ProfileService,
		private readonly firebaseStorageService: FirebaseStorageService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.collections = [];

		this.globalStateService.actions[GlobalActionTypeEnum.SELECT_WALLET_TYPE].subscribe(() => {
			this.toggleWalletTypeSelect();
		});

		this.web3ConnectionSubscription = this.web3Service.connectedWallet$.subscribe(async () => {
			setTimeout(async () => {
				this.isConnectedToWallet = this.web3Service.isWalletConnected();

				if (this.isConnectedToWallet) {
					this.connectedWalletDetails = await this.web3Service.getConnectedWallet();

					if (!this.profile) {
						this.profileService.getProfile(await this.web3Service.getUserAddress()).then(async (profile: IProfile) => {
							this.profile = profile;

							if (this.profile?.avatar) {
								this.profileAvatar = await this.firebaseStorageService.getFileURL(this.profile.avatar);
							}
						});
					}
				}
			}, 500);
		});

		this.searchCloseSubscription = fromEvent(document, 'click').subscribe((e) => {
			const clickedNode: Element = e.target as Element;

			if (clickedNode.classList.contains('search-icon')) {
				return;
			}

			if (this.isMobileNavOpened) {
				const mobileSearchDropdown: Element = document.querySelector('#mobileSearchContainer');

				if (this.isSearchOpen && !mobileSearchDropdown.contains(e.target as Node)) {
					this.isSearchOpen = false;
				}

				return;
			}
			const searchDropdown: Element = document.querySelector('#searchContainer');

			if (this.isSearchOpen && !searchDropdown.contains(e.target as Node)) {
				this.isSearchOpen = false;
			}
		});
	}

	public ngOnDestroy(): void {
		if (this.web3ConnectionSubscription) {
			this.web3ConnectionSubscription.unsubscribe();
		}
		if (this.searchCloseSubscription) {
			this.searchCloseSubscription.unsubscribe();
		}
		this.subscriptions.unsubscribe();
	}

	public scrollTo(targetId: string): void {
		document.getElementById(targetId).scrollIntoView({behavior: 'smooth', block: 'end'});
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToHelpCenter(): void {
		this.router.navigate(['/help-center']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToDocumentation(): void {
		window.open('https://docs.omnisea.org', '_blank');

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public async goToTerms(): Promise<void> {
		this.router.navigate(['/terms']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public async goToUserProfile(): Promise<void> {
		this.router.navigate(['/user'], {queryParams: {userAddress: await this.web3Service.getUserAddress()}});

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToHome(): void {
		this.router.navigate(['/']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToExplore(): void {
		this.router.navigate(['/collections']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToRankings(): void {
		this.router.navigate(['/collections/rankings']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToONFTGateway(): void {
		this.router.navigate(['/onft/gateway']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToToken(): void {
		this.router.navigate(['/token']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToTokenBridge(): void {
		this.router.navigate(['/token/bridge']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public async toggleWalletTypeSelect(): Promise<void> {
		const triggerBtn: HTMLButtonElement = document.querySelector<HTMLButtonElement>('#walletSelectButton');

		triggerBtn.click();
	}

	public async connectEVMWalletWithModal(): Promise<void> {
		await this.web3Service.connectEVMWalletWithModal();
		this.walletSelectCloseButton?.nativeElement?.click();
	}

	public async connectAptosMartianWallet(): Promise<void> {
		await this.web3Service.connectAptosMartianWallet();
		this.walletSelectCloseButton?.nativeElement?.click();
	}

	public async connectAptosPontemWallet(): Promise<void> {
		await this.web3Service.connectAptosPontemWallet();
		this.walletSelectCloseButton?.nativeElement?.click();
	}

	public async connectAptosPetraWallet(): Promise<void> {
		await this.web3Service.connectAptosPetraWallet();
		this.walletSelectCloseButton?.nativeElement?.click();
	}

	// public async connectSuiSuietWallet(): Promise<void> {
	// 	await this.web3Service.connectSuiSuietWallet();
	// 	this.walletSelectCloseButton?.nativeElement?.click();
	// }
	//
	// public onSuiSuietConnect(suietConnection: SuietWalletConnection) {
	// 	this.web3Service.setSuiSuietWalletConnection(suietConnection);
	// }
	//
	// public onSuiEthosConnect(connection: SuiEthosWalletConnection) {
	// 	this.web3Service.setSuiEthosWalletConnection(connection);
	// }

	public toggleMobileNav(): void {
		this.isMobileNavOpened = !this.isMobileNavOpened;
	}

	public toggleSearch(): void {
		this.isSearchOpen = !this.isSearchOpen;
	}

	public goToCollection(collection: ICollectionDetails): void {
		const queryParams: Params = {
			id: collection.id,
		};

		this.router.navigate(
			['/drop'],
			{queryParams},
		);
		this.isSearchOpen = false;
		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToAccelerator(): void {
		this.router.navigate(['/community/accelerator']);
	}
}
