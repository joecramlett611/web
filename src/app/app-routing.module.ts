import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './demos/demo-two/home.component';
import { createRoutes } from './modules/create/create-routes';
import { collectionsRoutes } from './modules/collections/collections-routes';
import { testnetRoutes } from './modules/testnet/testnet-routes';
import { SpecificModulePreloadStrategy } from './modules/shared/classes/specific-module-preload-strategy';
import { tokenRoutes } from './modules/token/token-routes';
import { helpCenterRoutes } from './modules/help-center/help-center-routes';
import { oNFTGatewayRoutes } from './modules/onft-gateway/onft-gateway-routes';
import { termsRoutes } from './modules/terms/terms-routes';
import { communityRoutes } from './modules/community/community-routes';
import { userProfileRoutes } from './modules/user-profile/user-profile-routes';

const routes: Routes = [
	{path: '', component: HomeComponent},
	{
		path: 'create',
		children: createRoutes,
	},
	{
		path: 'drop',
		children: collectionsRoutes,
	},
	{
		path: 'user',
		children: userProfileRoutes,
	},
	{
		path: 'testnet',
		children: testnetRoutes,
	},
	{
		path: 'token',
		children: tokenRoutes,
	},
	{
		path: 'help-center',
		children: helpCenterRoutes,
	},
	{
		path: 'onft',
		children: oNFTGatewayRoutes,
	},
	{
		path: 'terms',
		children: termsRoutes,
	},
	{
		path: 'community',
		children: communityRoutes,
	},
	{
		path: ':slug',
		children: collectionsRoutes,
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		scrollPositionRestoration: 'enabled',
		initialNavigation: 'enabled',
		preloadingStrategy: SpecificModulePreloadStrategy,
	})],
	exports: [RouterModule],
	providers: [SpecificModulePreloadStrategy],
})
export class AppRoutingModule {
}
