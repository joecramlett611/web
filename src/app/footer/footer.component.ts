import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
	public get isHome(): boolean {
		return window.location.pathname === '/';
	}

	constructor(private readonly router: Router) {
	}

	public goToHome(): void {
		this.router.navigate(['/']);
	}

	public goToTerms(): void {
		this.router.navigate(['/terms']);
	}

	public goToAmbassadors(): void {
		this.router.navigate(['/community/ambassadors']);
	}

	public goToAirdropForm(): void {
		this.router.navigate(['/community/airdrop-verification']);
	}

	public goToToken(): void {
		this.router.navigate(['/token']);
	}

	public goToTokenBridge(): void {
		this.router.navigate(['/token/bridge']);
	}

	public goToONFTGateway(): void {
		this.router.navigate(['/onft/gateway']);
	}

	public goToLaunch(): void {
		this.router.navigate(['/create']);
	}

	public goToHelpCenter(): void {
		this.router.navigate(['/help-center']);
	}

	public goToDocumentation(): void {
		window.open('https://docs.omnisea.org', '_blank');
	}

	public goToAccelerator(): void {
		this.router.navigate(['/community/accelerator']);
	}
}
